﻿namespace TestApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stateLabel = new System.Windows.Forms.Label();
            this.disconnectButton = new System.Windows.Forms.Button();
            this.connectButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.pkcsButton = new System.Windows.Forms.Button();
            this.capiButton = new System.Windows.Forms.Button();
            this.displayCertButton = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // stateLabel
            // 
            this.stateLabel.Location = new System.Drawing.Point(174, 17);
            this.stateLabel.Name = "stateLabel";
            this.stateLabel.Size = new System.Drawing.Size(258, 13);
            this.stateLabel.TabIndex = 0;
            this.stateLabel.Text = "---";
            // 
            // disconnectButton
            // 
            this.disconnectButton.Location = new System.Drawing.Point(93, 12);
            this.disconnectButton.Name = "disconnectButton";
            this.disconnectButton.Size = new System.Drawing.Size(75, 23);
            this.disconnectButton.TabIndex = 1;
            this.disconnectButton.Text = "Disconnect";
            this.disconnectButton.UseVisualStyleBackColor = true;
            this.disconnectButton.Click += new System.EventHandler(this.DisconnectButton_Click);
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(12, 12);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 2;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(305, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 3;
            // 
            // pkcsButton
            // 
            this.pkcsButton.Location = new System.Drawing.Point(865, 12);
            this.pkcsButton.Name = "pkcsButton";
            this.pkcsButton.Size = new System.Drawing.Size(75, 23);
            this.pkcsButton.TabIndex = 4;
            this.pkcsButton.Text = "Set PKCS cert";
            this.pkcsButton.UseVisualStyleBackColor = true;
            this.pkcsButton.Click += new System.EventHandler(this.SetPKCSCert_Click);
            // 
            // capiButton
            // 
            this.capiButton.Location = new System.Drawing.Point(946, 12);
            this.capiButton.Name = "capiButton";
            this.capiButton.Size = new System.Drawing.Size(60, 23);
            this.capiButton.TabIndex = 5;
            this.capiButton.Text = "Set CAPI cert";
            this.capiButton.UseVisualStyleBackColor = true;
            this.capiButton.Click += new System.EventHandler(this.SetCAPICert_Click);
            // 
            // displayCertButton
            // 
            this.displayCertButton.Location = new System.Drawing.Point(1012, 12);
            this.displayCertButton.Name = "displayCertButton";
            this.displayCertButton.Size = new System.Drawing.Size(73, 23);
            this.displayCertButton.TabIndex = 6;
            this.displayCertButton.Text = "Display Cert";
            this.displayCertButton.UseVisualStyleBackColor = true;
            this.displayCertButton.Click += new System.EventHandler(this.DisplayCert_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.AutoSize = false;
            this.trackBar1.Location = new System.Drawing.Point(411, 12);
            this.trackBar1.Maximum = 1000000;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(347, 23);
            this.trackBar1.TabIndex = 4;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(764, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Stop";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1088, 642);
            this.Controls.Add(this.displayCertButton);
            this.Controls.Add(this.capiButton);
            this.Controls.Add(this.pkcsButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.disconnectButton);
            this.Controls.Add(this.stateLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label stateLabel;
        private System.Windows.Forms.Button disconnectButton;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button pkcsButton;
        private System.Windows.Forms.Button capiButton;
        private System.Windows.Forms.Button displayCertButton;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Button button1;
    }
}

