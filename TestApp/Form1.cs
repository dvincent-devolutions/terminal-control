﻿namespace TestApp
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Security;
    using System.Threading;
    using System.Windows.Forms;
    using Devolutions.Protocols;
    using Devolutions.Terminal;
    using Microsoft.Win32.SafeHandles;

    public partial class Form1 : Form
    {
        private readonly IVT100Terminal terminal;

        private readonly Control controlTerminal;

        private bool isPlaying;

        private bool isReconnecting;

        private bool isConnecting;

        private string keyBlob;

        private long duration;

        private FileStream file;

        public Form1()
        {
            DevolutionsTerminalLoader.Absolute("C:\\Dev\\TerminalControl\\x64\\Debug\\DevolutionsTerminalNative.dll");
            DevolutionsProtocolsLoader.Absolute("C:\\Dev\\DevolutionsProtocols\\Windows\\DevolutionsProtocols\\x64\\Debug");

            this.InitializeComponent();

            SyntaxColoring.RegexError error;
            int index;
            bool isGood = DevolutionsTerminalControl.IsRegexGood("(?z)\\bls .+ [é]", out error, out index);
            Debug.Print("result " + isGood + " " + error + " at " + index + "\r\n");

            this.terminal = new DevolutionsTerminalControl();
            //this.terminal = new SshTerminal();

            this.controlTerminal = (Control)this.terminal;
            Point location = this.connectButton.Location;
            location.Y *= 2;
            location.Y += this.connectButton.Height;
            this.controlTerminal.Location = location;
            this.controlTerminal.Size = new Size(this.ClientSize.Width - (2 * location.X), this.ClientSize.Height - location.Y - location.X);
            this.controlTerminal.Anchor |= AnchorStyles.Bottom | AnchorStyles.Right;
            this.controlTerminal.Visible = true;
            this.Controls.Add(this.controlTerminal);
            this.controlTerminal.Focus();
            this.controlTerminal.Invalidate();

            this.terminal.Connected += this.Terminal_Connected;
            this.terminal.Disconnected += this.Terminal_Disconnected;
            this.terminal.NativeClosed += this.Terminal_NativeClosed;
            this.terminal.SshServerAuthentication += this.Terminal_SshServerAuthentication;
            //this.terminal.SshIsServerKnown += this.Terminal_SshIsServerKnown;
            this.terminal.NeedUser += this.Terminal_NeedUser;
            this.terminal.NeedPassword += this.Terminal_NeedPassword;
            this.terminal.NeedKeyPassword += this.Terminal_NeedKeyPassword;
            this.terminal.NeedJumpHostPassword += this.Terminal_NeedJumpHostPassword;
            this.terminal.Log += this.Terminal_Log;
            //this.terminal.PlayStopped += this.TerminalOnPlayStopped;
            //this.terminal.CurrentPlayTime += this.TerminalOnCurrentPlayTime;
            this.terminal.NewRecordData += this.TerminalOnNewRecordData;

            string comment;
            //DevolutionsProtocolsHelper.StringFromPublicKeyFile("C:\\Users\\david\\ssh_host_rsa_key.pub", out this.keyBlob, out comment);
        }

        private SecretString Terminal_NeedJumpHostPassword(object sender)
        {
            this.Invoke((MethodInvoker)delegate { MessageBox.Show("Need jump host password"); });
            return SecureStringFrom("123456");
        }

        private SshIsServerKnownResponse Terminal_SshIsServerKnown(object sender, string host, int port, string key)
        {
            return this.keyBlob.Equals(key) ? SshIsServerKnownResponse.ACCEPTED : SshIsServerKnownResponse.DENIED;
            //return SshIsServerKnownResponse.NOT_KNOWN;
        }

        private void TerminalOnNewRecordData(object sender, EventArgs eventArgs)
        {
            ThreadPool.QueueUserWorkItem(this.RecordData);
        }

        private void TerminalOnCurrentPlayTime(object sender, long time)
        {
            if (time < 0)
            {
                return;
            }
            this.BeginInvoke((MethodInvoker)delegate
                {
                    long pos = time * 1000000 / this.duration;
                    this.trackBar1.Value = (int)pos;
                });
        }

        private void TerminalOnPlayStopped(object sender, EventArgs eventArgs)
        {
            this.BeginInvoke((MethodInvoker)delegate
                {
                    this.isPlaying = false;
                    this.button1.Text = "Start";
                });
        }

        private void ConnectTerminal()
        {
            if (this.terminal.State == ConnectionState.Connected)
            {
                this.isReconnecting = true;
                this.terminal.CopyTerminalOutput();
                this.terminal.CloseNativeContext();
                return;
            }
            else if (this.terminal.State == ConnectionState.Disconnected)
            {
                this.isConnecting = true;
                this.terminal.CloseNativeContext();
                return;
            }
            this.stateLabel.Text = "Connecting";
            this.terminal.DoubleClickDelimiters = string.Empty;

            this.terminal.Verbose = false;
            //this.terminal.RemoteCommand = "ls -al";
            //this.terminal.LogFileName = "C:\\Users\\dvincent\\PuttyLog.txt";
            //this.terminal.LogContentType = LogContentType.Ascii;
            //this.terminal.LinuxFunctionKeys = KeyType.Xterm;
            this.terminal.MouseIsXterm = MouseType.Windows;
            this.terminal.DisableControlWheel = false;
            this.terminal.ResetScrollOnDisplay = false;
            //this.terminal.InteractiveAuthOnTerminal = true;
            //this.terminal.PingInterval = 5;
            //this.terminal.PingText = "!";
            this.terminal.GssapiAuthentication = false;

            int testCase = 1;
            switch (testCase)
            {
                case 0:
                    this.terminal.Host = "192.168.7.60";
                    this.terminal.Port = 2220;
                    this.terminal.User = "david";
                    this.terminal.Password = SecureStringFrom("123456");

                    this.terminal.GssapiAuthentication = false;

                    //this.terminal.PublicKeyFilePath = "C:\\Users\\dvincent\\Encrypted.ppk";
                    //this.terminal.PublicKeyPassword = SecureStringFrom("qwerty");
                    //this.terminal.ProxyMethod = ProxyType.Socks5;
                    //this.terminal.ProxyHost = "127.0.0.1";
                    //this.terminal.ProxyPort = 1080;
                    //this.terminal.ProxyUserName = "test";
                    //this.terminal.ProxyPassword = SecureStringFrom("123456");
                    //this.terminal.ProxyExcludeList = "";
                    break;

                case 1:
                    this.terminal.Host = "192.168.7.62";
                    this.terminal.Port = 2222;
                    this.terminal.User = "test";
                    this.terminal.Password = SecureStringFrom("123456");
                    //this.terminal.IsRecording = true;
                    //this.terminal.PublicKeyFilePath = "C:\\Users\\dvincent\\Encrypted.ppk";
                    //this.terminal.PublicKeyPassword = SecureStringFrom("qwerty");

                    Tunnel[] tunnels = new Tunnel[1];
                    tunnels[0] = new Tunnel("localhost", 15000, "192.168.7.60", 23);
                    this.terminal.LocalTunnels = tunnels;
                    break;

                case 2:
                    this.terminal.Host = "192.168.7.71";
                    this.terminal.Port = 22;
                    this.terminal.User = "david";
                    this.terminal.Password = SecureStringFrom("123456");
                    break;

                case 3:
                    // Telnet.
                    this.terminal.Host = "192.168.7.60";
                    this.terminal.Port = 23;
                    this.terminal.User = "david";
                    this.terminal.Password = SecureStringFrom("123456");
                    this.terminal.AddressFamily = AddressFamily.Ipv4;
                    this.terminal.Protocol = NativeSshProtocol.Telnet;
                    this.terminal.ExpectedPasswordPrompt = "Password:";
                    //this.terminal.ExpectedUserPrompt = "login:";
                    this.terminal.PingInterval = 0;
                    break;

                case 4:
                    // Telnet.
                    this.terminal.Host = "192.168.7.47";
                    this.terminal.Port = 23;
                    this.terminal.User = "downhill\\downtest";
                    this.terminal.Password = SecureStringFrom("123456");
                    this.terminal.AddressFamily = AddressFamily.Ipv4;
                    this.terminal.Protocol = NativeSshProtocol.Telnet;
                    //this.terminal.ExpectedPasswordPrompt = "password:";
                    //this.terminal.ExpectedUserPrompt = "login:";
                    this.terminal.PingInterval = 5;
                    break;

                case 5:
                    // Serial.
                    this.terminal.Protocol = NativeSshProtocol.Serial;
                    ((DevolutionsTerminalControl)this.terminal).SerialLine = "COM1";
                    this.terminal.Host = string.Empty;
                    break;

                case 6:
                    this.terminal.Host = "baker-w10";
                    this.terminal.Port = 22;
                    this.terminal.User = "baker";
                    this.terminal.Password = SecureStringFrom("123456");
                    break;

                case 7:
                    this.terminal.Host = "hub-w10";
                    this.terminal.Port = 2222;
                    this.terminal.User = "test";
                    this.terminal.Password = SecureStringFrom("123456");
                    break;

                case 8:
                    this.terminal.Jumps = new SshJump[1];
                    this.terminal.Jumps[0] = new SshJump
                    {
                        Host = "192.168.7.62",
                        Port = 2222,
                        User = "test",
                        Password = SecureStringFrom("123456"),
                    };

                    this.terminal.Host = "devolutions54";
                    this.terminal.Port = 22;
                    this.terminal.User = "david";
                    this.terminal.Password = SecureStringFrom("123456");
                    break;

                case 9:
                    this.terminal.Host = "192.168.1.2";
                    this.terminal.Port = 22;
                    this.terminal.User = "david";
                    this.terminal.Password = SecureStringFrom("vlaS/fjP1YG0");
                    //this.terminal.PublicKeyFilePath = "C:\\Users\\dvincent\\Encrypted.ppk";
                    //this.terminal.PublicKeyPassword = SecureStringFrom("qwerty");
                    break;
					
                case 10:
                    // Player.
                    ((DevolutionsTerminalControl)this.terminal).PlayerMaxIdle = 100;
                    this.terminal.Host = "player";
                    this.terminal.Protocol = NativeSshProtocol.Player;
                    break;
					
				case 11:
                    this.terminal.Host = "10.10.0.19";
                    this.terminal.Port = 60019;
                    this.terminal.User = "david";
                    //this.terminal.Password = SecureStringFrom("123456");

                    this.terminal.Jumps = new SshJump[1];
                    this.terminal.Jumps[0] = new SshJump
                    {
                        Host = "vsrv-openvpn.lab.loc",
                        Port = 60016,
                        User = "david",
                        Password = SecureStringFrom("123456"),
                    };

                    break;
            }

            //DevolutionsTerminalControl.Tunnel[] tunnels = new DevolutionsTerminalControl.Tunnel[1];
            //tunnels[0] = new DevolutionsTerminalControl.Tunnel("localhost", 15000, "localhost", 16000);
            //this.terminal.LocalTunnels = tunnels;

            //tunnels[0] = new DevolutionsTerminalControl.Tunnel("localhost", 2222, "192.168.7.62", 14000);
            //this.terminal.DynamicTunnels = tunnels;

            this.terminal.SyntaxColoringAddItem(new SyntaxColoring.Item("(?i)do", 8, 4, true, false, false, true));
            this.terminal.SyntaxColoringAddItem(new SyntaxColoring.Item("test", 9, 5, false, false, false, false));
            //this.terminal.SyntaxColoringDeleteAll();

            this.terminal.Connect();
        }

        private string Terminal_NeedUser(object sender)
        {
            this.Invoke((MethodInvoker)delegate { MessageBox.Show("Need user"); });
            return "david";
        }

        public static SecretString SecureStringFrom(string password)
        {
            char[] passwordArray = password.ToCharArray();
            SecretString securePassword = new SecretString();
            for (int i = 0; i < password.Length; i++)
            {
                securePassword.AppendChar(passwordArray[i]);
            }

            return securePassword;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if (this.terminal != null)
            {
                this.terminal.CloseNativeContext();
            }
        }

        private void Terminal_Log(object sender, LogType type, string message)
        {
            Debug.WriteLine(message);
        }

        private SecretString Terminal_NeedKeyPassword(object sender)
        {
            this.Invoke((MethodInvoker)delegate { MessageBox.Show("Need password for key"); });
            return SecureStringFrom("12345");
        }

        private int tryCount = 0;
        private SecretString Terminal_NeedPassword(object sender)
        {
            this.Invoke((MethodInvoker)delegate { MessageBox.Show("Need password"); });
            if (this.tryCount == 0)
            {
                this.tryCount++;
                return SecureStringFrom("123456");
            }
            return SecureStringFrom("123456");
            //return null;
        }

        private SshServerAuthenticationResponse Terminal_SshServerAuthentication(object sender, SshServerAuthenticationCode code, string fingerprint)
        {
            string message = "New fingerprint is: " + fingerprint;
            string caption = "Server not known";
            if (code == SshServerAuthenticationCode.SSH_SERVER_AUTHENTICATION_KEY_CHANGED)
            {
                caption = "SERVER KEY DIFFERS!";
            }

            DialogResult result = System.Windows.Forms.DialogResult.Cancel;
            this.Invoke((MethodInvoker)delegate { result = MessageBox.Show(message, caption, MessageBoxButtons.YesNoCancel); });
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                return SshServerAuthenticationResponse.AuthorizeAndRemember;
            }
            else if (result == System.Windows.Forms.DialogResult.No)
            {
                return SshServerAuthenticationResponse.AuthorizeOneTime;
            }

            return SshServerAuthenticationResponse.CancelConnection;
        }

        private void Terminal_NativeClosed(object sender, EventArgs e)
        {
            if (this.isReconnecting)
            {
                this.isReconnecting = false;
                this.BeginInvoke((MethodInvoker)delegate
                    {
                        this.stateLabel.Text = "Reconnecting";
                        this.terminal.Connect();
                    });
            }
            else if (this.isConnecting)
            {
                this.isConnecting = false;
                this.BeginInvoke((MethodInvoker)delegate
                    {
                        this.stateLabel.Text = "Connecting";
                        this.terminal.Connect();
                    });
            }
        }

        private void Terminal_Disconnected(object sender, EventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                this.stateLabel.Text = "Disconnected";
                //this.Controls.Remove(this.terminal);
                if (this.file != null)
                {
                    this.file.Close();
                    this.file = null;
                }
            });
            if (this.terminal.TerminalError != TerminalError.None)
            {
                string message = "Error = " + this.terminal.TerminalError.ToString();
                //this.Invoke((MethodInvoker)delegate
                //{
                //    MessageBox.Show(message, "Disconnection");
                //    //this.Controls.Remove(this.terminal);
                //    //this.terminal = null;
                //});
            }
            //this.terminal = null;
            //Thread.Sleep(2000);
            //this.BeginInvoke((MethodInvoker)delegate
            //{
            //    this.ConnectTerminal();
            //    this.stateLabel.Text = "Reconnecting";
            //});
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            // Change the view dimensions.
            base.OnSizeChanged(e);
            if (this.terminal == null)
            {
                return;
            }

            this.Text = "Size " + this.terminal.ActualTerminalSize;
        }

        private void Terminal_Connected(object sender, EventArgs e)
        {
            Debug.Print("Debug print: connected\r\n");
            this.BeginInvoke((MethodInvoker)delegate { this.stateLabel.Text = "Connected";
                this.controlTerminal.Focus();
                this.Text = "Size " + this.terminal.ActualTerminalSize;
                if (this.terminal.Protocol == NativeSshProtocol.Player)
                {
                    FileStream fileStream = new FileStream("Y:\\SessionRecordingKeep.bin", FileMode.Open);
                    byte[] data = new byte[fileStream.Length];
                    fileStream.Read(data, 0, data.Length);
                    ((DevolutionsTerminalControl)this.terminal).PlayerWrite(data);
                    this.duration = ((DevolutionsTerminalControl)this.terminal).PlayerGetDuration();
                    ((DevolutionsTerminalControl)this.terminal).PlayerStart();
                    this.isPlaying = true;
                }
            });

            //CommandSequence sequence = new CommandSequence();
            //sequence.AddCommand("sudo ifconfig\r", 2000);
            //sequence.AddCommand("123456\r", "password for root:", 5000, true);
            //this.terminal.Send(sequence);

            //this.terminal.Send("ena\r", 500);
            //this.terminal.Send("(ASA2018nd1000)\r", 500);
        }

        private void DisconnectButton_Click(object sender, EventArgs e)
        {
            this.terminal.Disconnect();
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            this.ConnectTerminal();
        }

        private void SetCAPICert_Click(object sender, EventArgs e)
        {
            this.terminal.TrySmartCardCertificateAuthentication = true;
            this.terminal.SetSmartCardCertificate(SetSmartCardCertificateMethod.CAPI);
        }

        private void SetPKCSCert_Click(object sender, EventArgs e)
        {
            this.terminal.TrySmartCardCertificateAuthentication = true;
            this.terminal.SetSmartCardCertificate(SetSmartCardCertificateMethod.PKCS);
        }

        private void DisplayCert_Click(object sender, EventArgs e)
        {
            this.terminal.DisplaySmartCardCertificate();
        }

        private void RecordData(object o)
        {
            if (this.file == null)
            {
                this.file = new FileStream("C:\\Users\\dvincent\\SessionRecording.bin", FileMode.Create);
            }
            byte[] bytes = ((DevolutionsTerminalControl)this.terminal).PlayerRead();
            this.file.Write(bytes, 0, bytes.Length);
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            long time = this.trackBar1.Value * this.duration / 1000000;
            ((DevolutionsTerminalControl)this.terminal).PlayerMoveTo(time);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.isPlaying)
            {
                this.button1.Text = "Start";
                this.isPlaying = false;
                ((DevolutionsTerminalControl)this.terminal).PlayerStop();
            }
            else
            {
                this.button1.Text = "Stop";
                this.isPlaying = true;
                ((DevolutionsTerminalControl)this.terminal).PlayerStart();
            }
        }
    }
}
