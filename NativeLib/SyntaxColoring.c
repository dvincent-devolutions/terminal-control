
#include "SyntaxColoring.h"
#include "terminal.h"
#include <ctype.h>
#define PCRE2_CODE_UNIT_WIDTH 8
#define PCRE2_STATIC
#include "pcre2.h"

typedef struct
{
	pcre2_code* compiledRegex;
	int color;
	int8_t isUnderlined;
	int8_t isActive;
} SyntaxColoringItem;


extern int8_t gUtf8Codes[];
#define UTF8_SIZE(firstUtf8Char) (gUtf8Codes[(uint8_t)(firstUtf8Char)])


void SyntaxColoringSetCharAttributes(termchar* c, SyntaxColoringItem* item)
{
	c->syntaxColoring = item->color;
	if (item->isUnderlined)
	{
		c->syntaxColoring |= ATTR_UNDER;
	}
	c->syntaxColoring |= c->attr & ~(ATTR_UNDER | ATTR_FGMASK | ATTR_BGMASK);
}


int Utf8FromCodePoint(uint8_t* p, uint32_t codePoint)
{
	if (codePoint > 0x10FFFF)
	{
		return 0;
	}
	else if (codePoint >= 0x10000)
	{
		p[0] = 0xF0 | (codePoint >> 18);
		p[1] = 0x80 | ((codePoint >> 12) & 0x3F);
		p[2] = 0x80 | ((codePoint >> 6) & 0x3F);
		p[3] = 0x80 | (codePoint & 0x3F);
		return 4;
	}
	else if (codePoint >= 0x800)
	{
		p[0] = 0xE0 | (codePoint >> 12);
		p[1] = 0x80 | ((codePoint >> 6) & 0x3F);
		p[2] = 0x80 | (codePoint & 0x3F);
		return 3;
	}
	else if (codePoint >= 0x80)
	{
		p[0] = 0xC0 | (codePoint >> 6);
		p[1] = 0x80 | (codePoint & 0x3F);
		return 2;
	}
	else
	{
		p[0] = (char)codePoint;
		return 1;
	}
}


void SyntaxColoringExecute(PuttyContext* context, termline* cline)
{
	if (context->syntaxColoringItemCount == 0)
	{
		return;
	}
	char* line = NULL;
	pcre2_match_data* matchData = NULL;

	line = malloc(cline->size * 4);
	if (line == NULL)
	{
		goto EXIT;
	}

	int utf8Index = 0;
	for (int i = 0; i < cline->size; i++)
	{
		unsigned long tchar = cline->chars[i].chr;
		switch (tchar & CSET_MASK)
		{
		case CSET_ASCII:
			tchar = tchar & 0xFF;
			break;
		case CSET_LINEDRW:
			tchar = context->term->ucsdata->unitab_xterm[tchar & 0xFF];
			break;
		case CSET_SCOACS:
			tchar = context->term->ucsdata->unitab_scoacs[tchar & 0xFF];
			break;
		}
		if (tchar == UCSWIDE)
		{
			continue;
		}
		int utf8Size = Utf8FromCodePoint(line + utf8Index, tchar);
		utf8Index += utf8Size;
		cline->chars[i].syntaxColoring = 0;
	}
	int lineLength = utf8Index;

	int size = cline->size;
	SyntaxColoringItem* items = context->syntaxColoringItems;
	int offset = 0;
	for (int i = 0; i < context->syntaxColoringItemCount; i++)
	{
		SyntaxColoringItem* item = items + i;
		if (item->isActive)
		{
			matchData = pcre2_match_data_create_from_pattern(item->compiledRegex, NULL);
			if (matchData == NULL)
			{
				goto EXIT;
			}
			int startIndex = 0;
			utf8Index = 0;
			int index = 0;
			while (1)
			{
				int result = pcre2_match(item->compiledRegex, line, lineLength, startIndex, PCRE2_NOTEMPTY, matchData, NULL);
				if (result == PCRE2_ERROR_NOMATCH)
				{
					break;
				}
				if (result < 0)
				{
					// Fatal error.
					item->isActive = 0;
					break;
				}

				size_t* ovector = pcre2_get_ovector_pointer(matchData);
				int matchStart = (int)ovector[0];
				int matchEnd = (int)ovector[1];
				while (utf8Index < matchStart)
				{
					utf8Index += UTF8_SIZE(line[utf8Index]);
					index++;
					if (cline->chars[index].chr == UCSWIDE)
					{
						index++;
					}
				}
				while (utf8Index < matchEnd)
				{
					SyntaxColoringSetCharAttributes(&(cline->chars[index]), item);
					utf8Index += UTF8_SIZE(line[utf8Index]);
					index++;
					if (cline->chars[index].chr == UCSWIDE)
					{
						index++;
					}
				}
				startIndex = matchEnd;

				if (startIndex >= lineLength)
				{
					break;
				}
			}
			pcre2_match_data_free(matchData);
			matchData = NULL;
		}
	}

EXIT:
	pcre2_match_data_free(matchData);
	free(line);
}


void SyntaxColoringRefresh(PuttyContext* context)
{
	if (context->syntaxColoringIsDisabled)
	{
		return;
	}
	for (int i = 0; i < context->lineCount; i++)
	{
		SyntaxColoringExecute(context, context->clines[i]);
	}
	context->lineCount = 0;
}


void SyntaxColoringAddLine(PuttyContext* context, termline* newLine)
{
	if (context->syntaxColoringIsDisabled)
	{
		return;
	}
	for (int i = 0; i < context->lineCount; i++)
	{
		if (context->clines[i] == newLine)
		{
			return;
		}
	}
	if (context->lineCount >= context->lineBufferCount)
	{
		termline** newClines = realloc(context->clines, (context->lineBufferCount + 1) * sizeof(termline*));
		if (newClines == NULL)
		{
			return;
		}
		context->lineBufferCount++;
		context->clines = newClines;
	}
	context->clines[context->lineCount] = newLine;
	context->lineCount++;
}


DEVOLUTIONS_API int32_t SyntaxColoringIsRegexGood(char* regex, int32_t* error, int32_t* index)
{
	int pcreError;
	size_t pcreErrorIndex;
	pcre2_code* compiledRegex = pcre2_compile(regex, PCRE2_ZERO_TERMINATED, PCRE2_UTF, &pcreError, &pcreErrorIndex, NULL);
	if (compiledRegex != NULL)
	{
		*error = 0;
		*index = 0;
		pcre2_code_free(compiledRegex);
		return 1;
	}

	*error = pcreError;

	// Convert utf8 index to utf16 index.
	int utf16Index = 0;
	int utf8Index = 0;
	while (utf8Index < (int)pcreErrorIndex)
	{
		int utf8Size = UTF8_SIZE(regex[utf8Index]);
		if (utf8Size > 3)
		{
			utf16Index++;
		}
		utf16Index++;
		utf8Index += utf8Size;
	}

	*index = utf16Index;
	return 0;
}


DEVOLUTIONS_API int32_t SyntaxColoringAdd(PuttyContext* context, char* keyword, int32_t foreColor, int32_t backColor, char isCompleteWord, char isCaseSensitive, char isUnderlined, char isRegex)
{
	SyntaxColoringItem* items = realloc(context->syntaxColoringItems, (context->syntaxColoringItemCount + 1) * sizeof(SyntaxColoringItem));
	if (items == NULL)
	{
		return -1;
	}
	SyntaxColoringItem* newItem = items + context->syntaxColoringItemCount;
	context->syntaxColoringItemCount++;
	context->syntaxColoringItems = items;

	if (isRegex)
	{
		int pcreError;
		size_t pcreErrorIndex;
		newItem->compiledRegex = pcre2_compile(keyword, PCRE2_ZERO_TERMINATED, PCRE2_UTF, &pcreError, &pcreErrorIndex, NULL);
	}
	else
	{
		int keywordLength = (int)strlen(keyword);
		char* regex = malloc(keywordLength * 2 + 9);
		regex[0] = 0;
		if (isCaseSensitive == 0)
		{
			strcat(regex, "(?i)");
		}
		if (isCompleteWord)
		{
			strcat(regex, "\\b");
		}
		int utf8Index = 0;
		while (utf8Index < keywordLength)
		{
			int utf8Size = UTF8_SIZE(keyword[utf8Index]);
			char c = keyword[utf8Index];
			if (utf8Size == 1 && (c == '[' || c == '\\' || c == '^' || c == '$' || c == '.' || c == '|' || c == '?' || c == '*' || c == '+' || c == '(' || c == ')' || c == '/'))
			{
				strcat(regex, "\\");
			}
			int regexLength = (int)strlen(regex);
			memcpy(regex + regexLength, keyword + utf8Index, utf8Size);
			regex[regexLength + utf8Size] = 0;
			utf8Index += utf8Size;
		}
		if (isCompleteWord)
		{
			strcat(regex, "\\b");
		}
		int pcreError;
		size_t pcreErrorIndex;
		newItem->compiledRegex = pcre2_compile(regex, PCRE2_ZERO_TERMINATED, PCRE2_UTF, &pcreError, &pcreErrorIndex, NULL);
		free(regex);
	}

	if (newItem->compiledRegex == NULL)
	{
		context->syntaxColoringItemCount--;
		return -1;
	}

	newItem->isActive = 1;
	newItem->color = foreColor | (backColor << 9);
	newItem->isUnderlined = isUnderlined;

	return 0;
}


DEVOLUTIONS_API void SyntaxColoringDisable(PuttyContext* context, int index)
{
	assert(index < context->syntaxColoringItemCount);

	SyntaxColoringItem* items = context->syntaxColoringItems;
	items[index].isActive = 0;
}


DEVOLUTIONS_API void SyntaxColoringEnable(PuttyContext* context, int index)
{
	assert(index < context->syntaxColoringItemCount);

	SyntaxColoringItem* items = context->syntaxColoringItems;
	items[index].isActive = 1;
}


DEVOLUTIONS_API void SyntaxColoringApplyChanges(PuttyContext* context)
{
	// Unimplemented for now.
	assert(0);
}


DEVOLUTIONS_API void SyntaxColoringReleaseMemory(PuttyContext* context)
{
	if (context == NULL)
	{
		return;
	}

	SyntaxColoringItem* items = context->syntaxColoringItems;
	for (int i = 0; i < context->syntaxColoringItemCount; i++)
	{
		pcre2_code_free(items[i].compiledRegex);
	}
	free(context->syntaxColoringItems);
	context->syntaxColoringItems = NULL;
	context->syntaxColoringItemCount = 0;
	free(context->clines);
	context->clines = NULL;
	context->lineCount = 0;
	context->lineBufferCount = 0;
}


