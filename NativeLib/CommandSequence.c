
#include "TerminalMain.h"

enum StepTypes
{
	IDLE,
	DELAY,
	PROMPT,
	END,
};

#define ETX 0xC0

#define SUCCESS 0

int8_t gUtf8Codes[] =
{
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-2, -2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
};

#define UTF8_SIZE(firstUtf8Char) (gUtf8Codes[(uint8_t)(firstUtf8Char)])

#define IS_NUMERIC(c) (c >= '0' && c <= '9')


uint64_t getNumeric(char* p, int digitCount)
{
	assert(p != NULL);

	uint64_t value = 0;
	int i;
	for (i = 0; i < digitCount; i++)
	{
		if (IS_NUMERIC(p[i]))
		{
			value *= 10;
			value += p[i] - '0';
		}
		else
		{
			break;
		}
	}

	return value;
}


int nextWhite(char* p)
{
	int i = 0;
	while (1)
	{
		char c = *p++;
		if (c == ' ' || c == '\t' || c == '\r' || c == '\n')
		{
			return i;
		}
		i++;
	}
}


int nextDelimiter(char* p, char delimiter, int length)
{
	int i = 0;
	while (*p++ != delimiter && i < length)
	{
		i++;
	}
	return i;
}


void CommandSequenceSetupStep(CommandSequence* context)
{
	context->isReady = 0;
	context->lastStep = GetTickCount();
	char* p = context->step;
	if (*p == 'd')
	{
		// Command with predelay: "d 'delay' 'command'[ETX]".
		p += 2;
		context->delay = (int32_t)getNumeric(p, 8);
		p += nextWhite(p);
		p++;
		context->command = p;
		context->commandLength = nextDelimiter(p, ETX, (int)strlen(p));
		context->stepType = DELAY;
	}
	else if (*p == 'p')
	{
		// Format for command with expected prompt: "p 'timeout' a|- 'prompt'[ETX]'command'[ETX]".
		p += 2;
		context->timeout = (int32_t)getNumeric(p, 8);
		p += nextWhite(p);
		p++;
		context->isAbortOnTimeout = (*p == 'a' ? 1 : 0);
		p += 2;
		context->expectedPrompt = p;
		context->expectedPromptLength = nextDelimiter(p, ETX, (int)strlen(p));
		p += context->expectedPromptLength + 1;
		context->command = p;
		context->commandLength = nextDelimiter(p, ETX, (int)strlen(p));
		context->stepType = PROMPT;
	}
	else
	{
		context->stepType = END;
	}
}


void CommandSequenceStart(CommandSequence* context, char* sequence)
{
	context->sequence = _strdup(sequence);
	if (context->sequence == NULL)
	{
		context->result = FAIL_OUT_OF_MEMORY;
		return;
	}
	context->step = context->sequence;
	CommandSequenceSetupStep(context);
}


void CommandSequenceVerifyPrompt(CommandSequence* context, uint8_t* utf8Char)
{
	if (context->stepType != PROMPT)
	{
		return;
	}
	
	int charSize = UTF8_SIZE(*utf8Char);
	if (memcmp(context->expectedPrompt + context->expectedPromptIndex, utf8Char, charSize) == 0)
	{
		context->expectedPromptIndex += charSize;
		if (context->expectedPromptIndex == context->expectedPromptLength)
		{
			context->isReady = 1;
			return;
		}
	}
	else
	{
		context->expectedPromptIndex = 0;
	}
	
	uint32_t tick = GetTickCount();
	if (tick - context->lastStep > context->timeout)
	{
		if (context->isAbortOnTimeout)
		{
			context->stepType = END;
			context->result = FAIL_TIMEOUT;
			return;
		}
		CommandSequenceNextStep(context);
	}
}


void CommandSequenceVerifyDelay(CommandSequence* context)
{
	if (context->stepType != DELAY)
	{
		return;
	}
	
	uint32_t tick = GetTickCount();
	if (tick - context->lastStep > context->delay)
	{
		context->isReady = 1;
	}
}


char* CommandSequenceGetCommand(CommandSequence* context)
{
	if (context->stepType == IDLE || context->stepType == END)
	{
		return "";
	}
	context->isReady = 0;
	context->command[context->commandLength] = 0;
	return context->command;
}


int CommandSequenceNextStep(CommandSequence* context)
{
	context->step = context->command + context->commandLength + 1;
	CommandSequenceSetupStep(context);
	return context->stepType == END ? END_OF_FILE : SUCCESS;
}


void CommandSequenceClear(CommandSequence* context)
{
	free(context->sequence);
	memset(context, 0, sizeof(CommandSequence));
}


