
#include "ssh.h"


typedef struct tagKeyText
{
	char* text;
	int index;
	int size;
} KeyText;


void keyInit(KeyText* key, char* keyText);
int keyGetType(KeyText* key);
unsigned char* keyLoadPublic(KeyText* key, char** algorithm, int* pub_blob_len, char** commentptr, const char** errorstr);
int keyIsEncrypted(KeyText* key, char** commentptr);
struct ssh2_userkey* keyLoadPrivate(KeyText* key, char* passphrase, const char** errorstr);
