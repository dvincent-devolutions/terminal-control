#include <assert.h>
#include "SmartCard.h"

typedef INT(CALLBACK* FUNCTION_1)(CHAR*);
typedef LPBYTE(CALLBACK* FUNCTION_2)(struct ssh2_userkey*, LPCBYTE, INT, INT*, HWND);
typedef struct ssh2_userkey*(CALLBACK* FUNCTION_3)(LPCSTR);
typedef (CALLBACK* FUNCTION_4)(LPCSTR);
typedef CHAR*(CALLBACK* FUNCTION_5)(CHAR*, HWND);
typedef (CALLBACK* FUNCTION_6)(VOID*);
typedef LPSTR(CALLBACK* FUNCTION_7)(LPCSTR);
typedef (CALLBACK* FUNCTION_8)(LPCSTR, HWND);

HINSTANCE SMART_CARD_LIB;
FUNCTION_1 CERT_IS_CERTPATH;
FUNCTION_2 CERT_SIGN;
FUNCTION_3 CERT_LOAD_KEY;
FUNCTION_4 CERT_CONVERT_LEGACY;
FUNCTION_5 CERT_PROMPT;
FUNCTION_6 CERT_FREE;
FUNCTION_7 CERT_KEY_STRING;
FUNCTION_8 CERT_DISPLAY_CERT;

void initializeSmartCardLibInstance()
{
	if (SMART_CARD_LIB == NULL)
	{
		LPCSTR currentModule = "DevolutionsTerminalNative.dll";
		int currentModuleLength = (int)strlen(currentModule);
		HMODULE moduleHandle = GetModuleHandle(currentModule);
		int callingModulePathMaxLength = 400;
		LPSTR modulePath = calloc(callingModulePathMaxLength, sizeof(char));
		if (modulePath == NULL)
		{
			assert(0);
			return;
		}

		int pathLength = GetModuleFileName(moduleHandle, modulePath, callingModulePathMaxLength);
		memset(modulePath + (pathLength - currentModuleLength), '\0', currentModuleLength);
		modulePath = strcat(modulePath, "DevolutionsTerminalSmartCard.dll");
		SMART_CARD_LIB = LoadLibrary(modulePath);
		free(modulePath);
		modulePath = NULL;
		if (SMART_CARD_LIB == NULL)
		{
			assert(0);
		}
	}
}


int smartCardCertIsCertpath(char* cert)
{
	initializeSmartCardLibInstance();
	if (CERT_IS_CERTPATH == NULL)
	{
		CERT_IS_CERTPATH = (FUNCTION_1)GetProcAddress(SMART_CARD_LIB, "smartCardBridgeCertIsCertpath");
		if (CERT_IS_CERTPATH == NULL)
		{
			assert(0);
			return 0;
		}
	}
	
	return CERT_IS_CERTPATH(cert);
}


LPBYTE smartCardCertSign(struct ssh2_userkey * userkey, LPCBYTE pDataToSign, int iDataToSignLen, int * iWrappedSigLen, HWND hWnd)
{
	initializeSmartCardLibInstance();
	if (CERT_SIGN == NULL)
	{
		CERT_SIGN = (FUNCTION_2)GetProcAddress(SMART_CARD_LIB, "smartCardBridgeCertSign");
		if (CERT_SIGN == NULL)
		{
			assert(0);
			return NULL;
		}
	}

	LPBYTE sigblob = CERT_SIGN(userkey, pDataToSign, iDataToSignLen, iWrappedSigLen, hWnd);
	return sigblob;
}


struct ssh2_userkey* smartCardCertLoadKey(LPCSTR certId)
{
	initializeSmartCardLibInstance();
	if (CERT_LOAD_KEY == NULL)
	{
		CERT_LOAD_KEY = (FUNCTION_3)GetProcAddress(SMART_CARD_LIB, "smartCardBridgeCertLoadKey");
		if (CERT_LOAD_KEY == NULL)
		{
			assert(0);
			return NULL;
		}
	}

	return CERT_LOAD_KEY(certId);
}


void smartCardCertConvertLegacy(LPSTR certId)
{
	initializeSmartCardLibInstance();
	if (CERT_CONVERT_LEGACY == NULL)
	{
		CERT_CONVERT_LEGACY = (FUNCTION_4)GetProcAddress(SMART_CARD_LIB, "smartCardBridgeCertConvertLegacy");
		if (CERT_CONVERT_LEGACY == NULL)
		{
			assert(0);
			return;
		}
	}

	CERT_CONVERT_LEGACY(certId);
}

char* smartCardCertPrompt(char* type, HWND hwnd)
{
	initializeSmartCardLibInstance();
	if (CERT_PROMPT == NULL)
	{
		CERT_PROMPT = (FUNCTION_5)GetProcAddress(SMART_CARD_LIB, "smartCardBridgeCertPrompt");
		if (CERT_PROMPT == NULL)
		{
			assert(0);
			return NULL;
		}
	}

	return CERT_PROMPT(type, hwnd);
}

void smartCardFree(void* p)
{
	if (p != NULL)
	{
		initializeSmartCardLibInstance();
		if (CERT_FREE == NULL)
		{
			CERT_FREE = (FUNCTION_6)GetProcAddress(SMART_CARD_LIB, "smartCardBridgeFree");
			if (CERT_FREE == NULL)
			{
				assert(0);
				return;
			}
		}

		CERT_FREE(p);
	}
}

LPSTR smartCardCertKeyString(LPCSTR certId)
{
	initializeSmartCardLibInstance();
	if (CERT_KEY_STRING == NULL)
	{
		CERT_KEY_STRING = (FUNCTION_7)GetProcAddress(SMART_CARD_LIB, "smartCardBridgeCertKeyString");
		if (CERT_KEY_STRING == NULL)
		{
			assert(0);
			return NULL;
		}
	}

	return CERT_KEY_STRING(certId);
}

void smartCardCertDisplayCert(LPCSTR certId, HWND hwnd)
{
	initializeSmartCardLibInstance();
	if (CERT_DISPLAY_CERT == NULL)
	{
		CERT_DISPLAY_CERT = (FUNCTION_8)GetProcAddress(SMART_CARD_LIB, "smartCardBridgeCertDisplayCert");
		if (CERT_DISPLAY_CERT == NULL)
		{
			assert(0);
			return;
		}
	}

	CERT_DISPLAY_CERT(certId, hwnd);
}

void smartCardFreeLibrary()
{
	if (SMART_CARD_LIB != NULL)
	{
		FreeLibrary(SMART_CARD_LIB);
		SMART_CARD_LIB = NULL;
	}
}

DEVOLUTIONS_API char* SmartCardGetCAPICertId(void* hwnd)
{
	char* certId = smartCardCertPrompt("CAPI:", (HWND)hwnd);   // certId needs to be freed at some point using smartCardFree.
	return certId;
}

DEVOLUTIONS_API char* SmartCardGetPKSCCertId(void* hwnd)
{
	char* certId = smartCardCertPrompt("PKCS:", (HWND)hwnd);   // certId needs to be freed at some point using smartCardFree.
	return certId;
}

DEVOLUTIONS_API char* SmartCardGetPublicKey(LPCSTR certId)
{
	if (certId != NULL)
	{
		char* publicKey = smartCardCertKeyString(certId);
		return publicKey;   // publicKey needs to be freed at some point using smartCardFree.
	}

	return NULL;
}

DEVOLUTIONS_API void SmartCardDisplayCert(LPCSTR certId, void* hwnd)
{
	if (certId != NULL)
	{
		smartCardCertDisplayCert(certId, (HWND)hwnd);
	}
}

DEVOLUTIONS_API void SmartCardFree(void* p)
{
	if (p != NULL)
	{
		smartCardFree(p);
	}
}
