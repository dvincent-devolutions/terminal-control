

#ifndef __SyntaxColoring_h__
#define __SyntaxColoring_h__

#include "TerminalMain.h"

void SyntaxColoringRefresh(PuttyContext* context);
void SyntaxColoringAddLine(PuttyContext* context, termline* newLine);
DEVOLUTIONS_API void SyntaxColoringReleaseMemory(PuttyContext* context);

#endif


