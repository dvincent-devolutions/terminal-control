
#ifndef CommandSequence_h
#define CommandSequence_h

#include <windows.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <assert.h>

typedef struct
{
	char* sequence;
	char* step;
	char* command;
	char* expectedPrompt;
	int expectedPromptLength;
	int expectedPromptIndex;
	int commandLength;
	int stepType;
	int result;
	uint32_t lastStep;
	uint32_t delay;
	uint32_t timeout;
	uint8_t isAbortOnTimeout;
	uint8_t isReady;
} CommandSequence;


void CommandSequenceStart(CommandSequence* commandSequence, char* sequence);
void CommandSequenceVerifyPrompt(CommandSequence* commandSequence, uint8_t* utf8Char);
void CommandSequenceVerifyDelay(CommandSequence* commandSequence);
char* CommandSequenceGetCommand(CommandSequence* commandSequence);
int CommandSequenceNextStep(CommandSequence* commandSequence);
void CommandSequenceClear(CommandSequence* commandSequence);


#endif

