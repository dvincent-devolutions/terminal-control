

#ifndef __TerminalMain_h__
#define __TerminalMain_h__

#include "Putty.h"
#include "terminal.h"
#include "CommandSequence.h"
#include <ws2tcpip.h>
#include <inttypes.h>

#define DEVOLUTIONS_API __declspec(dllexport)

enum
{
	SUCCESS = 0,
	FAIL = -1,
};

enum PuttyErrors
{
	PUTTY_ERROR_NONE,
	PUTTY_ERROR_FATAL,
	PUTTY_ERROR_OUT_OF_MEMORY,
	PUTTY_ERROR_SOCKET,
	PUTTY_ERROR_SOCKET_LIBRARY,
	PUTTY_ERROR_SSH,
	PUTTY_ERROR_TELNET,
	PUTTY_ERROR_SERIAL,
};

enum FailCodes
{
	FAIL_OUT_OF_MEMORY = -2,
	FAIL_PERMISSION_DENIED = -3,
	FAIL_OUT_OF_STORAGE_SPACE = -4,
	FAIL_FILE_DOES_EXIST = -5,
	FAIL_INTERRUPTED = -6,
	FAIL_INVALID_ARG = -7,
	FAIL_REACHED_LIMIT = -8,
	FAIL_NAME_TOO_LONG = -9,
	FAIL_PATH_DOES_NOT_EXIST = -10,
	FAIL_READ_ONLY = -11,
	FAIL_OVERFLOW = -12,
	END_OF_FILE = -13,
	FAIL_TEMPORARY = -14,
	FAIL_HOST_NOT_FOUND = -15,
	FAIL_ADDRESS_USED = -16,
	FAIL_ADDRESS_NOT_AVAILABLE = -17,
	FAIL_CONNECTION_REFUSED = -18,
	FAIL_HOST_UNREACHABLE = -19,
	FAIL_NETWORK_DOWN = -20,
	FAIL_TIMEOUT = -21,
	FAIL_CONNECTION_RESET = -22,
	FAIL_NOT_CONNECTED = -23,
	FAIL_LISTEN_DENIED = -24,
	FAIL_ABORTED = -25,
	FAIL_NOT_IMPLEMENTED = -26,
	FAIL_ALREADY_CONNECTED = -27,
	FAIL_DATA_CORRUPTED = -28,
	FAIL_FILE_CORRUPTED = -29,
	FAIL_FILE_TYPE_UNKNOWN = -30,
	FAIL_CERTIFICATE_UNTRUSTED = -31,
	FAIL_NEED_PASSWORD = -32,
	FAIL_BUFFER_TOO_SMALL = -33,
};

#define WM_AGENT_CALLBACK (WM_APP + 4)
#define WM_NETEVENT  (WM_APP + 5)
#define WM_SEND_STRING (WM_USER + 10)
#define WM_COPY_OUTPUT (WM_USER + 11)
#define WM_DISCONNECT (WM_USER + 12)
#define WM_DEFERED_FOCUS (WM_USER + 13)
#define WM_UPDATE_CONFIG (WM_USER + 14)
#define WM_PLAYER_MOVE_TO (WM_USER + 15)
#define WM_PLAYER_START (WM_USER + 16)
#define WM_PLAYER_STOP (WM_USER + 17)
#define WM_PLAYER_LOCK_BUFFER (WM_USER + 18)
#define WM_PLAYER_UNLOCK_BUFFER (WM_USER + 19)
#define WM_END_THREAD (WM_USER + 20)
#define WM_IS_CONNECTED (WM_USER + 21)

#define DEFINE_CONTEXT_POINTER() PuttyContext* gg = (PuttyContext*)TlsGetValue(g_tlsIndex)

#define LOG_TYPE_MESSAGE 0
#define LOG_TYPE_ERROR 1
#define LOG_TYPE_VERBOSE 2
#define LOG_TYPE_WARNING 3

#define NCFGCOLOURS 22
#define NEXTCOLOURS 240
#define NALLCOLOURS (NCFGCOLOURS + NEXTCOLOURS)

#define FONT_MAXNO      0x40

#define HASHINPUT 64                   /* 64 bytes SHA input */
#define HASHSIZE 20                    /* 160 bits SHA output */
#define POOLSIZE 1200                  /* size of random pool */

#define IDM_CLRSB     0x0060
#define IDM_RESET     0x0070

struct RandPool
{
	unsigned char pool[POOLSIZE];
	int poolpos;

	unsigned char incoming[HASHSIZE];

	unsigned char incomingb[HASHINPUT];
	int incomingpos;

	int stir_pending;
};

#define NPRIORITIES 2

struct cmdline_saved_param_set
{
	struct cmdline_saved_param *params;
	int nsaved, savesize;
};

extern DWORD g_tlsIndex;

enum
{
	SSH_SERVER_AUTH_DIFFERENT_KEY = 1,
	SSH_SERVER_AUTH_NO_KEY = 3,

	SSH_SERVER_AUTH_ONE_TIME = 5,
	SSH_SERVER_AUTH_REMEMBER = 6,
	SSH_SERVER_AUTH_CANCEL = 4,
};

typedef struct
{
	void* signal;
	void* needValue;
	void* needString;
	void* log;
	void* releaseString;
	void* setError;
	void* sshServerAuthentication;
	void* sshIsServerKnown;
	void* currentPlayTime;
} PuttyCallbacks;

typedef UINT64 NeedValueCallback(UINT16 valueId);
typedef char* NeedStringCallback(UINT16 stringId);
typedef void ReleaseStringCallback(char* string);
typedef void SignalCallback(int32_t signal);
typedef void LogCallback(UINT16 type, const char* text);
typedef void SetError(UINT16 error);
typedef UINT16 SshServerAuthentication(UINT16 code, char* fingerprint);
typedef UINT16 SshIsServerKnown(char* key);
typedef void SessionTimeCallback(int64_t time);

typedef struct
{
	// Putty globals.
	Backend* back;
	void* backhandle;
	int bold_colours;
	int bold_font_mode;
	int busy_status;
	int caret_x;
	int caret_y;
	HBITMAP caretbm;
	struct callback* cbhead;
	struct callback* cbtail;
	char* chm_path;
	wchar_t* clipboard_contents;
	size_t clipboard_length;
	//char* cmdline_password;
	char* cmdline_session_name;
	int cmdline_tooltype;
	COLORREF colours[NALLCOLOURS];
	int compose_state;
	Conf* conf;
	struct controlbox* ctrlbox;
	struct winctrls ctrls_base;
	struct winctrls ctrls_panel;
	int cursor_type;
	int dbltime;
	int default_port;
	int default_protocol;
	RGBTRIPLE defpal[NALLCOLOURS];
	int descent;
	struct dlgparam dp;
	tree234* errorstrings;
	tree234* errstrings;
	char** events;
	int extra_height;
	int extra_width;
	int flags;
	int flashing;
	int font_dualwidth;
	int font_height;
	int font_varpitch;
	int font_width;
	int fontflag[FONT_MAXNO];
	HFONT fonts[FONT_MAXNO];
	void* frontend;
	tree234* handles_by_evtomain;
	int help_has_contents;
	char* help_path;
	HINSTANCE hinst;
	HWND hwnd;
	char* icon_name;
	int kbd_codepage;
	int lastact;
	Mouse_Button lastbtn;
	int lasttime;
	void *ldisc;
	LOGFONT lfont;
	int loaded_session;
	INTERFACE_INFO local_interfaces[16];
	HWND logbox;
	void* logctx;
	LPLOGPALETTE logpal;
	int n_local_interfaces;   /* 0=not yet, -1=failed, >0=number */
	int n_specials;
	int negsize;
	PSID networksid;
	int nevents;
	long next_flash;
	long next_noise_collection;
	toplevel_callback_notify_fn_t notify_frontend;
	DWORD now;
	int offset_height;
	int offset_width;
	OSVERSIONINFO osVersion;
	HPALETTE pal;
	struct RandPool pool;
	HMENU popup_menus[2];
	char putty_path[2048];
	int random_active;
	int reconfiguring;
	int requested_help;
	HMENU savedsess_menu;
	struct cmdline_saved_param_set saves[NPRIORITIES];
	int send_raw_mouse;
	int session_closed;
	struct sesslist sesslist;
	HMODULE shell32_module;
	tree234* sktree;
	struct telnet_special* specials;
	HMENU specials_menu;
	Terminal* term;
	tree234* timer_contexts;
	tree234* timers;
	long timing_next_time;
	COLORREF tip_bg;
	ATOM tip_class;
	int tip_enabled;
	HFONT tip_font;
	COLORREF tip_text;
	HWND tip_wnd;
	int tried_shgetfolderpath;
	struct unicode_data ucsdata;
	int und_mode;
	PSID usersid;
	int vtmode;
	int wheel_accumulator;
	HMODULE wincrypt_module;
	char* window_name;
	HMODULE winsock_module;
	HMODULE winsock2_module;
	UINT wm_mousewheel;
	PSID worldsid;
	WSADATA wsadata;
	HMODULE wship6_module;

	// Putty function statics.
	int cmdline_get_passwd_input__tried_once;
	struct telnet_special ssh_get_specials__ssh_specials[20];
	int got_crypt__attempted;
	int got_crypt__successful;
	HMODULE got_crypt__crypt;
	UINT winctrl_handle_command__draglistmsg;
	CHOOSECOLOR winctrl_handle_command__cc;
	DWORD winctrl_handle_command__custom[16];
	int force_normal__recurse;
	int update_mouse_pointer__forced_visible;
	int request_resize__first_time;
	RECT request_resize__ss;
	RECT reset_window__ss;
	int show_mouseptr__cursor_visible;
	int WndProc__ignore_clip;
	int WndProc__need_backend_resize;
	int WndProc__fullscr_on_max;
	int WndProc__processed_resize;
	UINT WndProc__last_mousemove;
	WPARAM WndProc__wp;
	LPARAM WndProc__lp;
	int* do_text_internal__lpDx;
	int do_text_internal__lpDx_len;
	wchar_t* do_text_internal__uni_buf;
	int do_text_internal__uni_len;
	char* do_text_internal__directbuf;
	int do_text_internal__directlen;
	WCHAR* do_text_internal__wbuf;
	int do_text_internal__wlen;
	int TranslateKey__alt_sum;
	wchar_t TranslateKey__keys_unicode[3];
	int TranslateKey__compose_char;
	WPARAM TranslateKey__compose_keycode;
	WORD TranslateKey__keys[3];
	BYTE TranslateKey__keysb[3];
	long do_beep__lastbeep;
	HMODULE sk_handle_peer_info__kernel32_module;
	int get_username__tried_usernameex;
	char* load_system32_dll__sysdir;
	int got_advapi__attempted;
	int got_advapi__successful;
	HMODULE got_advapi__advapi;
	char cp_name__buf[32];
	wchar_t pending_surrogate;

	// Devolutions specifics.
	NeedStringCallback* needStringCallback;
	ReleaseStringCallback* releaseStringCallback;
	NeedValueCallback* needValueCallback;
	LogCallback* logCallback;
	SignalCallback* signalCallback;
	SetError* setError;
	SshServerAuthentication* sshServerAuthentication;
	SshIsServerKnown* sshIsServerKnown;
	SessionTimeCallback* currentPlayTime;
	SessionTimeCallback* recordEndTime;

	char hasFocus;
	//char isQuit;
	char isDisconnectionSignaled;
	HANDLE heap;
	HWND parent;
	HANDLE thread;
	DWORD threadId;
	int xOffset;
	WCHAR* outputCopy;
	HANDLE copyCompleteEvent;
	void* player;
	UINT pingInterval;
	UINT_PTR pingTimer;
	CommandSequence sequence;

	// SyntaxColoring specific.
	int lineCount;
	int lineBufferCount;
	char syntaxColoringIsDisabled;
	int syntaxColoringItemCount;
	void* syntaxColoringItems;
	termline** clines;
} PuttyContext;

typedef struct
{
	uint32_t timer;
	uint16_t type;
	uint16_t size;
} RecordChunkHeader;

enum RecordChunkTypes
{
	RECORD_CHUNK_TERMINAL_OUTPUT = 0,
	RECORD_CHUNK_USER_INPUT = 1,
	RECORD_CHUNK_SIZE_CHANGE = 2,
	RECORD_CHUNK_LOG = 3,
};

enum SignalTypes
{
	SIGNAL_TYPE_CONNECTED = 1,
	SIGNAL_TYPE_DISCONNECTED = 2,
	SIGNAL_TYPE_DENIED = 3,
	SIGNAL_TYPE_BEEP = 4,
	SIGNAL_TYPE_NEW_RECORD_DATA = 5,
	SIGNAL_TYPE_CLOSED = 6,
	SIGNAL_TYPE_SEQUENCE_SUCCESS = 7,
	SIGNAL_TYPE_SEQUENCE_FAIL = 8,
	SIGNAL_TYPE_SEQUENCE_TIMEOUT = 9,
};

#define SIGNAL_NEED_VALUE(c, id) (*(c->needValueCallback))(id)
#define SIGNAL_NEED_STRING(c, id) (*(c->needStringCallback))(id)
#define SIGNAL_RELEASE_STRING(c, string) (*(c->releaseStringCallback))(string)
#define SIGNAL_LOG(c, type, text) (*(c->logCallback))(type, text)
#define SIGNAL_CONNECTED(c) (*(c->signalCallback))(SIGNAL_TYPE_CONNECTED)
#define SIGNAL_DENIED(c) (*(c->signalCallback))(SIGNAL_TYPE_DENIED)
#define SIGNAL_DISCONNECTED(c) if (c->isDisconnectionSignaled == 0) { (*(c->signalCallback))(SIGNAL_TYPE_DISCONNECTED); c->isDisconnectionSignaled = 1; }
#define SIGNAL_CLOSED(c) (*(c->signalCallback))(SIGNAL_TYPE_CLOSED)
#define SIGNAL_ERROR(c, error) (*(c->setError))(error)
#define SIGNAL_BEEP(c) (*(c->signalCallback))(SIGNAL_TYPE_BEEP)
#define SIGNAL_SSH_SERVER_AUTHENTICATION(c, code, fingerprint) (*(c->sshServerAuthentication))(code, fingerprint)
#define SIGNAL_SSH_IS_SERVER_KNOWN(c, key) (*(c->sshIsServerKnown))(key)
#define SIGNAL_NEW_RECORD_DATA(c) (*(c->signalCallback))(SIGNAL_TYPE_NEW_RECORD_DATA)
#define SIGNAL_CURRENT_SESSION_TIME(c, time) (*(c->currentPlayTime))(time)
#define SIGNAL_SEQUENCE_SUCCESS(c) (*(c->signalCallback))(SIGNAL_TYPE_SEQUENCE_SUCCESS)
#define SIGNAL_SEQUENCE_FAIL(c) (*(c->signalCallback))(SIGNAL_TYPE_SEQUENCE_FAIL)
#define SIGNAL_SEQUENCE_TIMEOUT(c) (*(c->signalCallback))(SIGNAL_TYPE_SEQUENCE_TIMEOUT)

char* SignalNeedPasswordString(PuttyContext* context, int stringId);

void terminalCopyAll(Terminal *);


#endif

