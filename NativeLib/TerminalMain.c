

#include "TerminalMain.h"
#include "TerminalConfig.h"
#include "SyntaxColoring.h"

int PuttyMain(HINSTANCE inst, HINSTANCE prev, LPSTR cmdline, int show);
SOCKET SshGetTunnelPort(PuttyContext* context);


DWORD g_tlsIndex = TLS_OUT_OF_INDEXES;
DWORD g_tlsIndexRefCount = 0;


DWORD WINAPI PuttyThread(LPVOID parameters)
{
	MSG msg;
	PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);   // This forces the creation of the message queue early on.
	PuttyContext* context = parameters;

	TlsSetValue(g_tlsIndex, parameters);
	PuttyMain(NULL, NULL,  "-load \"Default Settings\"", SW_SHOW);

	return 0;
}


int AllocTlsStorage()
{
	if (g_tlsIndexRefCount == 0)
	{
		g_tlsIndex = TlsAlloc();
		if (g_tlsIndex == TLS_OUT_OF_INDEXES)
		{
			return FAIL;
		}
	}
	g_tlsIndexRefCount++;

	return SUCCESS;
}


void FreeTlsStorage()
{
	if (g_tlsIndex == TLS_OUT_OF_INDEXES || g_tlsIndexRefCount <= 0)
	{
		g_tlsIndexRefCount = 0;
		return;
	}

	g_tlsIndexRefCount--;
	if (g_tlsIndexRefCount == 0)
	{
		TlsFree(g_tlsIndex);
		g_tlsIndex = TLS_OUT_OF_INDEXES;
	}
}


char* SignalNeedPasswordString(PuttyContext* context, int stringId)
{
	wchar_t* utf16Ppassword = (wchar_t*)SIGNAL_NEED_STRING(context, stringId);
	if (utf16Ppassword == NULL)
	{
		return NULL;
	}

	int utf16Length = (int)wcslen(utf16Ppassword);
	int utf8Length = WideCharToMultiByte(CP_UTF8, 0, utf16Ppassword, utf16Length, NULL, 0, NULL, NULL);
	char* utf8Password = malloc(utf8Length + 1);
	if (utf8Password == NULL)
	{
		memset(utf16Ppassword, 0, utf16Length * 2);   // Clear the sensitive data.
		SIGNAL_RELEASE_STRING(context, (char*)utf16Ppassword);
		return NULL;
	}
	WideCharToMultiByte(CP_UTF8, 0, utf16Ppassword, utf16Length, utf8Password, utf8Length, NULL, NULL);
	utf8Password[utf8Length] = 0;
	memset(utf16Ppassword, 0, utf16Length * 2);   // Clear the sensitive data.
	SIGNAL_RELEASE_STRING(context, (char*)utf16Ppassword);

	return utf8Password;
}


DEVOLUTIONS_API INT16 dtConnect(PuttyContext* context)
{
	context->thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)PuttyThread, context, 0, &context->threadId);
	if (context->thread == NULL)
	{
		return FAIL;
	}

	return SUCCESS;
}


DEVOLUTIONS_API INT16 dtGetPort(PuttyContext* context)
{
	return (INT16)SshGetTunnelPort(context);
}


DEVOLUTIONS_API INT64 dtActualTerminalSize(PuttyContext* context)
{
	return ((INT64)context->term->cols) << 32 | context->term->rows;
}


DEVOLUTIONS_API void dtStartPing(PuttyContext* context)
{
	context->pingTimer = SetTimer(context->hwnd, 1, context->pingInterval, NULL);
}


DEVOLUTIONS_API void dtDisconnect(PuttyContext* context)
{
	PostMessage(context->hwnd, WM_DISCONNECT, 0, 0);
}


DEVOLUTIONS_API void dtEndThread(PuttyContext* context)
{
	if (context->thread == NULL)
	{
		return;
	}

	if (context->hwnd != NULL)
	{
		PostMessage(context->hwnd, WM_CLOSE, 0, 0);
	}
	else
	{
		while (PostThreadMessage(context->threadId, WM_END_THREAD, 0, 0) == 0)
		{
			Sleep(0);
		}
	}
}


DEVOLUTIONS_API void dtSend(PuttyContext* context, char* commandString)
{
	if (commandString == NULL || commandString[0] == 0)
	{
		return;
	}
	SendMessage(context->hwnd, WM_SEND_STRING, (WPARAM)commandString, 0);
}


DEVOLUTIONS_API void dtSendSequence(PuttyContext* context, char* sequence)
{
	CommandSequenceStart(&context->sequence, sequence);
}


DEVOLUTIONS_API void dtResetTerminal(PuttyContext* context)
{
	SendMessage(context->hwnd, WM_SYSCOMMAND, IDM_RESET, 0);
}


DEVOLUTIONS_API void dtClearScrollBack(PuttyContext* context)
{
	SendMessage(context->hwnd, WM_SYSCOMMAND, IDM_CLRSB, 0);
}


DEVOLUTIONS_API void* dtCopyOutput(PuttyContext* context)
{
	PostMessage(context->hwnd, WM_COPY_OUTPUT, 0, 0);
	WaitForSingleObject(context->copyCompleteEvent, 5000);
	return context->outputCopy;
}


DEVOLUTIONS_API void dtRelease(PuttyContext* context)
{
	if (context == NULL)
	{
		return;
	}

	DWORD result = WaitForSingleObject(context->thread, INFINITE);
	CloseHandle(context->thread);

	if (context->copyCompleteEvent != NULL)
	{
		CloseHandle(context->copyCompleteEvent);
	}

	SyntaxColoringReleaseMemory(context);
	free(context->outputCopy);

	free(context);
	FreeTlsStorage();
}


DEVOLUTIONS_API PuttyContext* dtNew(HWND parent, PuttyCallbacks* callbacks)
{
	int result = AllocTlsStorage();
	if (result < 0)
	{
		return NULL;
	}

	PuttyContext* context = calloc(1, sizeof(PuttyContext));
	if (context == NULL)
	{
		return NULL;
	}

	context->copyCompleteEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (context->copyCompleteEvent == NULL)
	{
		dtRelease(context);
		return NULL;
	}

	context->parent = parent;

	context->needStringCallback = callbacks->needString;
	context->releaseStringCallback = callbacks->releaseString;
	context->needValueCallback = callbacks->needValue;
	context->logCallback = callbacks->log;
	context->signalCallback = callbacks->signal;
	context->setError = callbacks->setError;
	context->sshServerAuthentication = callbacks->sshServerAuthentication;
	context->sshIsServerKnown = callbacks->sshIsServerKnown;
	context->currentPlayTime = callbacks->currentPlayTime;

	return context;
}


DEVOLUTIONS_API void dtResize(PuttyContext* context, UINT32 width, UINT32 height)
{
	if (context->hwnd != NULL)
	{
		SetWindowPos(context->hwnd, HWND_TOP, 0, 0, width, height, SWP_NOMOVE | SWP_NOZORDER);
	}
}


DEVOLUTIONS_API void dtSetFocus(PuttyContext* context)
{
	if (context->hwnd != NULL)
	{
		PostMessage(context->hwnd, WM_DEFERED_FOCUS, 0, 0);
	}
}


DEVOLUTIONS_API void dtReconfig(PuttyContext* context)
{
	if (context->hwnd != NULL)
	{
		PostMessage(context->hwnd, WM_UPDATE_CONFIG, 0, 0);
	}
}


