#pragma once

#include "TerminalMain.h"

int smartCardCertIsCertpath(char* cert);

LPBYTE  smartCardCertSign(struct ssh2_userkey * userkey, LPCBYTE pDataToSign, int iDataToSignLen, int * iWrappedSigLen, HWND hWnd);

struct ssh2_userkey* smartCardCertLoadKey(LPCSTR certId);

void smartCardCertConvertLegacy(LPSTR certId);

// Release pointer allocated in smartCard lib
void smartCardFree(void* p);

// Used to get the public key needed by host in his authorized keys file
LPSTR smartCardCertKeyString(LPCSTR certId);

void smartCardCertDisplayCert(LPCSTR certId, HWND hwnd);

// Release dll
void smartCardFreeLibrary();

DEVOLUTIONS_API char* SmartCardGetCAPICertId(HWND hwnd);

DEVOLUTIONS_API char* SmartCardGetPKSCCertId(HWND hwnd);

DEVOLUTIONS_API char* SmartCardGetPublicKey(LPCSTR certId);

DEVOLUTIONS_API void SmartCardDisplayCert(LPCSTR certId, HWND hwnd);

DEVOLUTIONS_API void SmartCardFree(void* p);
