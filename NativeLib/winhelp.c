/*
 * winhelp.c: centralised functions to launch Windows help files,
 * and to decide whether to use .HLP or .CHM help in any given
 * situation.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "TerminalMain.h"

#ifndef NO_HTMLHELP
#include <htmlhelp.h>
#endif /* NO_HTMLHELP */

#ifndef NO_HTMLHELP
DECL_WINDOWS_FUNCTION(static, HWND, HtmlHelpA, (HWND, LPCSTR, UINT, DWORD));
#endif /* NO_HTMLHELP */

void init_help(void)
{
	DEFINE_CONTEXT_POINTER();
        char b[2048], *p, *q, *r;
        FILE *fp;

        GetModuleFileName(NULL, b, sizeof(b) - 1);
        r = b;
        p = strrchr(b, '\\');
        if (p && p >= r)
        {
                r = p + 1;
        }
        q = strrchr(b, ':');
        if (q && q >= r)
        {
                r = q + 1;
        }
        strcpy(r, PUTTY_HELP_FILE);
        if ((fp = fopen(b, "r")) != NULL)
        {
		gg->help_path = dupstr(b);
                fclose(fp);
        }
        else
        {
		gg->help_path = NULL;
        }
        strcpy(r, PUTTY_HELP_CONTENTS);
        if ((fp = fopen(b, "r")) != NULL)
        {
                gg->help_has_contents = TRUE;
                fclose(fp);
        }
        else
        {
		gg->help_has_contents = FALSE;
        }

#ifndef NO_HTMLHELP
        strcpy(r, PUTTY_CHM_FILE);
        if ((fp = fopen(b, "r")) != NULL)
        {
                gg->chm_path = dupstr(b);
                fclose(fp);
        }
        else
        {
		gg->chm_path = NULL;
        }
	if (gg->chm_path)
        {
                HINSTANCE dllHH = load_system32_dll("hhctrl.ocx");
                GET_WINDOWS_FUNCTION(dllHH, HtmlHelpA);
                if (!p_HtmlHelpA)
                {
			gg->chm_path = NULL;
                        if (dllHH)
                        {
                                FreeLibrary(dllHH);
                        }
                }
        }
#endif /* NO_HTMLHELP */
}


void shutdown_help(void)
{
        /* Nothing to do currently.
        * (If we were running HTML Help single-threaded, this is where we'd
        * call HH_UNINITIALIZE.) */
}


int has_help(void)
{
	DEFINE_CONTEXT_POINTER();
        /*
         * FIXME: it would be nice here to disregard help_path on
         * platforms that didn't have WINHLP32. But that's probably
         * unrealistic, since even Vista will have it if the user
         * specifically downloads it.
         */
	return (gg->help_path != NULL
#ifndef NO_HTMLHELP
		|| gg->chm_path
#endif /* NO_HTMLHELP */
                );
}


void launch_help(HWND hwnd, const char *topic)
{
	DEFINE_CONTEXT_POINTER();
        if (topic)
        {
                int colonpos = (int)strcspn(topic, ":");

#ifndef NO_HTMLHELP
		if (gg->chm_path)
                {
                        char *fname;
                        assert(topic[colonpos] != '\0');
			fname = dupprintf("%s::/%s.html>main", gg->chm_path,
                                          topic + colonpos + 1);
                        p_HtmlHelpA(hwnd, fname, HH_DISPLAY_TOPIC, 0);
                        sfree(fname);
                }
                else
#endif /* NO_HTMLHELP */
		if (gg->help_path)
                {
                        char *cmd = dupprintf("JI(`',`%.*s')", colonpos, topic);
			WinHelp(hwnd, gg->help_path, HELP_COMMAND, (DWORD)cmd);
                        sfree(cmd);
                }
        }
        else
        {
#ifndef NO_HTMLHELP
		if (gg->chm_path)
                {
			p_HtmlHelpA(hwnd, gg->chm_path, HH_DISPLAY_TOPIC, 0);
                }
                else
#endif /* NO_HTMLHELP */
		if (gg->help_path)
                {
			WinHelp(hwnd, gg->help_path, gg->help_has_contents ? HELP_FINDER : HELP_CONTENTS, 0);
                }
        }
	gg->requested_help = TRUE;
}


void quit_help(HWND hwnd)
{
	DEFINE_CONTEXT_POINTER();
	if (gg->requested_help)
        {
#ifndef NO_HTMLHELP
		if (gg->chm_path)
                {
                        p_HtmlHelpA(NULL, NULL, HH_CLOSE_ALL, 0);
                }
                else
#endif /* NO_HTMLHELP */
		if (gg->help_path)
                {
			WinHelp(hwnd, gg->help_path, HELP_QUIT, 0);
                }
		gg->requested_help = FALSE;
        }
}

