/*
 * winmisc.c: miscellaneous Windows-specific things
 */

#include <stdio.h>
#include <stdlib.h>
#include "TerminalMain.h"
#define SECURITY_WIN32
#include <security.h>

char *platform_get_x_display(void)
{
        /* We may as well check for DISPLAY in case it's useful. */
        return dupstr(getenv("DISPLAY"));
}


Filename *filename_from_str(const char *str)
{
        Filename *ret = snew(Filename);

        ret->path = dupstr(str);
        return ret;
}


Filename *filename_copy(const Filename *fn)
{
        return filename_from_str(fn->path);
}


const char *filename_to_str(const Filename *fn)
{
        return fn->path;
}


int filename_equal(const Filename *f1, const Filename *f2)
{
        return !strcmp(f1->path, f2->path);
}


int filename_is_null(const Filename *fn)
{
        return !*fn->path;
}


void filename_free(Filename *fn)
{
        sfree(fn->path);
        sfree(fn);
}


int filename_serialise(const Filename *f, void *vdata)
{
        char *data = (char *)vdata;
        int len = (int)strlen(f->path) + 1; /* include trailing NUL */

        if (data)
        {
                strcpy(data, f->path);
        }
        return len;
}


Filename *filename_deserialise(void *vdata, int maxsize, int *used)
{
        char *data = (char *)vdata;
        char *end;

        end = memchr(data, '\0', maxsize);
        if (!end)
        {
                return NULL;
        }
        end++;
        *used = (int)(end - data);
        return filename_from_str(data);
}


char filename_char_sanitise(char c)
{
        if (strchr("<>:\"/\\|?*", c))
        {
                return '.';
        }
        return c;
}


#ifndef NO_SECUREZEROMEMORY
/*
 * Windows implementation of smemclr (see misc.c) using SecureZeroMemory.
 */
void smemclr(void *b, size_t n)
{
        if (b && n > 0)
        {
                SecureZeroMemory(b, n);
        }
}


#endif

char *get_username(void)
{
	DEFINE_CONTEXT_POINTER();
        DWORD namelen;
        char *user = NULL;
        int got_username = FALSE;

        DECL_WINDOWS_FUNCTION(static, BOOLEAN, GetUserNameExA,
                              (EXTENDED_NAME_FORMAT, LPSTR, PULONG));

        {
		if (!gg->get_username__tried_usernameex)
                {
                        /* Not available on Win9x, so load dynamically */
                        HMODULE secur32 = load_system32_dll("secur32.dll");
                        GET_WINDOWS_FUNCTION(secur32, GetUserNameExA);
			gg->get_username__tried_usernameex = TRUE;
                }
        }

        if (p_GetUserNameExA)
        {
                /*
                 * If available, use the principal -- this avoids the problem
                 * that the local username is case-insensitive but Kerberos
                 * usernames are case-sensitive.
                 */

                /* Get the length */
                namelen = 0;
                (void) p_GetUserNameExA(NameUserPrincipal, NULL, &namelen);

                user = snewn(namelen, char);
                got_username = p_GetUserNameExA(NameUserPrincipal, user, &namelen);
                if (got_username)
                {
                        char *p = strchr(user, '@');
                        if (p)
                        {
                                *p = 0;
                        }
                }
                else
                {
                        sfree(user);
                }
        }

        if (!got_username)
        {
                /* Fall back to local user name */
                namelen = 0;
                if (GetUserName(NULL, &namelen) == FALSE)
                {
                        /*
                         * Apparently this doesn't work at least on Windows XP SP2.
                         * Thus assume a maximum of 256. It will fail again if it
                         * doesn't fit.
                         */
                        namelen = 256;
                }

                user = snewn(namelen, char);
                got_username = GetUserName(user, &namelen);
                if (!got_username)
                {
                        sfree(user);
                }
        }

        return got_username ? user : NULL;
}


BOOL init_winver(void)
{
        return 1;
}


HMODULE load_system32_dll(const char *libname)
{
        /*
         * Wrapper function to load a DLL out of c:\windows\system32
         * without going through the full DLL search path. (Hence no
         * attack is possible by placing a substitute DLL earlier on that
         * path.)
         */
	DEFINE_CONTEXT_POINTER();
        char *fullpath;
        HMODULE ret;

        if (!gg->load_system32_dll__sysdir)
        {
                int size = 0, len;
                do
                {
                        size = 3 * size / 2 + 512;
			gg->load_system32_dll__sysdir = sresize(gg->load_system32_dll__sysdir, size, char);
			len = GetSystemDirectory(gg->load_system32_dll__sysdir, size);
                } while (len >= size);
        }

	fullpath = dupcat(gg->load_system32_dll__sysdir, "\\", libname, NULL);
        ret = LoadLibrary(fullpath);
        sfree(fullpath);
        return ret;
}


/*
 * A tree234 containing mappings from system error codes to strings.
 */

struct errstring
{
        int error;
        char *text;
};

static int errstring_find(void *av, void *bv)
{
        int *a = (int *)av;
        struct errstring *b = (struct errstring *)bv;

        if (*a < b->error)
        {
                return -1;
        }
        if (*a > b->error)
        {
                return +1;
        }
        return 0;
}


static int errstring_compare(void *av, void *bv)
{
        struct errstring *a = (struct errstring *)av;

        return errstring_find(&a->error, bv);
}


const char *win_strerror(int error)
{
	DEFINE_CONTEXT_POINTER();
        struct errstring *es;

        if (!gg->errstrings)
        {
		gg->errstrings = newtree234(errstring_compare);
        }

	es = find234(gg->errstrings, &error, errstring_find);

        if (!es)
        {
                char msgtext[65536]; /* maximum size for FormatMessage is 64K */

                es = snew(struct errstring);
                es->error = error;
                if (!FormatMessage((FORMAT_MESSAGE_FROM_SYSTEM |
                                    FORMAT_MESSAGE_IGNORE_INSERTS), NULL, error,
                                   MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                                   msgtext, lenof(msgtext) - 1, NULL))
                {
                        sprintf(msgtext,
                                "(unable to format: FormatMessage returned %d)",
                                GetLastError());
                }
                else
                {
                        int len = (int)strlen(msgtext);
                        if (len > 0 && msgtext[len - 1] == '\n')
                        {
                                msgtext[len - 1] = '\0';
                        }
                }
                es->text = dupprintf("Error %d: %s", error, msgtext);
		add234(gg->errstrings, es);
        }

        return es->text;
}


FontSpec *fontspec_new(const char *name,int bold, int height, int charset)
{
        FontSpec *f = snew(FontSpec);

        f->name = dupstr(name);
        f->isbold = bold;
        f->height = height;
        f->charset = charset;
        return f;
}


FontSpec *fontspec_copy(const FontSpec *f)
{
        return fontspec_new(f->name, f->isbold, f->height, f->charset);
}


void fontspec_free(FontSpec *f)
{
        sfree(f->name);
        sfree(f);
}


int fontspec_serialise(FontSpec *f, void *vdata)
{
        char *data = (char *)vdata;
        int len = (int)strlen(f->name) + 1; /* include trailing NUL */

        if (data)
        {
                strcpy(data, f->name);
                PUT_32BIT_MSB_FIRST(data + len, f->isbold);
                PUT_32BIT_MSB_FIRST(data + len + 4, f->height);
                PUT_32BIT_MSB_FIRST(data + len + 8, f->charset);
        }
        return len + 12;               /* also include three 4-byte ints */
}


FontSpec *fontspec_deserialise(void *vdata, int maxsize, int *used)
{
        char *data = (char *)vdata;
        char *end;

        if (maxsize < 13)
        {
                return NULL;
        }
        end = memchr(data, '\0', maxsize - 12);
        if (!end)
        {
                return NULL;
        }
        end++;
        *used = (int)(end - data) + 12;
        return fontspec_new(data,
                            GET_32BIT_MSB_FIRST(end),
                            GET_32BIT_MSB_FIRST(end + 4),
                            GET_32BIT_MSB_FIRST(end + 8));
}

