/*
 * wincapi.c: implementation of wincapi.h.
 */

#include "TerminalMain.h"

#if !defined NO_SECURITY

#define WINCAPI_GLOBAL
#include "wincapi.h"

int got_crypt(void)
{
	DEFINE_CONTEXT_POINTER();

        if (!gg->got_crypt__attempted)
        {
		gg->got_crypt__attempted = TRUE;
		gg->got_crypt__crypt = load_system32_dll("crypt32.dll");
		gg->got_crypt__successful = gg->got_crypt__crypt && GET_WINDOWS_FUNCTION(gg->got_crypt__crypt, CryptProtectMemory);
        }
	return gg->got_crypt__successful;
}


#endif /* !defined NO_SECURITY */

