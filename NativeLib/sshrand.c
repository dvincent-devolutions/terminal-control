/*
 * cryptographic random number generator for PuTTY's ssh client
 */

#include "TerminalMain.h"
#include "ssh.h"
#include <assert.h>

/* Collect environmental noise every 5 minutes */
#define NOISE_REGULAR_INTERVAL (5 * 60 * TICKSPERSEC)

void noise_get_heavy(void (*func)(void *, int));
void noise_get_light(void (*func)(void *, int));

/*
 * `pool' itself is a pool of random data which we actually use: we
 * return bytes from `pool', at position `poolpos', until `poolpos'
 * reaches the end of the pool. At this point we generate more
 * random data, by adding noise, stirring well, and resetting
 * `poolpos' to point to just past the beginning of the pool (not
 * _the_ beginning, since otherwise we'd give away the whole
 * contents of our pool, and attackers would just have to guess the
 * next lot of noise).
 *
 * `incomingb' buffers acquired noise data, until it gets full, at
 * which point the acquired noise is SHA'ed into `incoming' and
 * `incomingb' is cleared. The noise in `incoming' is used as part
 * of the noise for each stirring of the pool, in addition to local
 * time, process listings, and other such stuff.
 */

static void random_stir(void)
{
	DEFINE_CONTEXT_POINTER();
        word32 block[HASHINPUT / sizeof(word32)];
        word32 digest[HASHSIZE / sizeof(word32)];
        int i, j, k;

        /*
         * noise_get_light will call random_add_noise, which may call
         * back to here. Prevent recursive stirs.
         */
        if (gg->pool.stir_pending)
        {
                return;
        }
	gg->pool.stir_pending = TRUE;

        noise_get_light(random_add_noise);

#ifdef RANDOM_DIAGNOSTICS
        {
                int p, q;
                printf("random stir starting\npool:\n");
                for (p = 0; p < POOLSIZE; p += HASHSIZE)
                {
                        printf("   ");
                        for (q = 0; q < HASHSIZE; q += 4)
                        {
                                printf(" %08x", *(word32 *)(pool.pool + p + q));
                        }
                        printf("\n");
                }
                printf("incoming:\n   ");
                for (q = 0; q < HASHSIZE; q += 4)
                {
                        printf(" %08x", *(word32 *)(pool.incoming + q));
                }
                printf("\nincomingb:\n   ");
                for (q = 0; q < HASHINPUT; q += 4)
                {
                        printf(" %08x", *(word32 *)(pool.incomingb + q));
                }
                printf("\n");
                random_diagnostics++;
        }
#endif

	SHATransform((word32 *)gg->pool.incoming, (word32 *)gg->pool.incomingb);
	gg->pool.incomingpos = 0;

        /*
         * Chunks of this code are blatantly endianness-dependent, but
         * as it's all random bits anyway, WHO CARES?
         */
	memcpy(digest, gg->pool.incoming, sizeof(digest));

        /*
         * Make two passes over the pool.
         */
        for (i = 0; i < 2; i++)
        {

                /*
                 * We operate SHA in CFB mode, repeatedly adding the same
                 * block of data to the digest. But we're also fiddling
                 * with the digest-so-far, so this shouldn't be Bad or
                 * anything.
                 */
		memcpy(block, gg->pool.pool, sizeof(block));

                /*
                 * Each pass processes the pool backwards in blocks of
                 * HASHSIZE, just so that in general we get the output of
                 * SHA before the corresponding input, in the hope that
                 * things will be that much less predictable that way
                 * round, when we subsequently return bytes ...
                 */
                for (j = POOLSIZE; (j -= HASHSIZE) >= 0; )
                {
                        /*
                         * XOR the bit of the pool we're processing into the
                         * digest.
                         */

                        for (k = 0; k < sizeof(digest) / sizeof(*digest); k++)
                        {
				digest[k] ^= ((word32 *)(gg->pool.pool + j))[k];
                        }

                        /*
                         * Munge our unrevealed first block of the pool into
                         * it.
                         */
                        SHATransform(digest, block);

                        /*
                         * Stick the result back into the pool.
                         */

                        for (k = 0; k < sizeof(digest) / sizeof(*digest); k++)
                        {
				((word32 *)(gg->pool.pool + j))[k] = digest[k];
                        }
                }

#ifdef RANDOM_DIAGNOSTICS
                if (i == 0)
                {
                        int p, q;
                        printf("random stir midpoint\npool:\n");
                        for (p = 0; p < POOLSIZE; p += HASHSIZE)
                        {
                                printf("   ");
                                for (q = 0; q < HASHSIZE; q += 4)
                                {
                                        printf(" %08x", *(word32 *)(pool.pool + p + q));
                                }
                                printf("\n");
                        }
                        printf("incoming:\n   ");
                        for (q = 0; q < HASHSIZE; q += 4)
                        {
                                printf(" %08x", *(word32 *)(pool.incoming + q));
                        }
                        printf("\nincomingb:\n   ");
                        for (q = 0; q < HASHINPUT; q += 4)
                        {
                                printf(" %08x", *(word32 *)(pool.incomingb + q));
                        }
                        printf("\n");
                }
#endif
        }

        /*
         * Might as well save this value back into `incoming', just so
         * there'll be some extra bizarreness there.
         */
        SHATransform(digest, block);
	memcpy(gg->pool.incoming, digest, sizeof(digest));

	gg->pool.poolpos = sizeof(gg->pool.incoming);

	gg->pool.stir_pending = FALSE;

#ifdef RANDOM_DIAGNOSTICS
        {
                int p, q;
                printf("random stir done\npool:\n");
                for (p = 0; p < POOLSIZE; p += HASHSIZE)
                {
                        printf("   ");
                        for (q = 0; q < HASHSIZE; q += 4)
                        {
                                printf(" %08x", *(word32 *)(pool.pool + p + q));
                        }
                        printf("\n");
                }
                printf("incoming:\n   ");
                for (q = 0; q < HASHSIZE; q += 4)
                {
                        printf(" %08x", *(word32 *)(pool.incoming + q));
                }
                printf("\nincomingb:\n   ");
                for (q = 0; q < HASHINPUT; q += 4)
                {
                        printf(" %08x", *(word32 *)(pool.incomingb + q));
                }
                printf("\n");
                random_diagnostics--;
        }
#endif
}


void random_add_noise(void *noise, int length)
{
	DEFINE_CONTEXT_POINTER();
        unsigned char *p = noise;
        int i;

        if (!gg->random_active)
        {
                return;
        }

        /*
         * This function processes HASHINPUT bytes into only HASHSIZE
         * bytes, so _if_ we were getting incredibly high entropy
         * sources then we would be throwing away valuable stuff.
         */
	while (length >= (HASHINPUT - gg->pool.incomingpos))
        {
		memcpy(gg->pool.incomingb + gg->pool.incomingpos, p, HASHINPUT - gg->pool.incomingpos);
		p += HASHINPUT - gg->pool.incomingpos;
		length -= HASHINPUT - gg->pool.incomingpos;
		SHATransform((word32 *)gg->pool.incoming, (word32 *)gg->pool.incomingb);
                for (i = 0; i < HASHSIZE; i++)
                {
			gg->pool.pool[gg->pool.poolpos++] ^= gg->pool.incomingb[i];
			if (gg->pool.poolpos >= POOLSIZE)
                        {
				gg->pool.poolpos = 0;
                        }
                }
		if (gg->pool.poolpos < HASHSIZE)
                {
                        random_stir();
                }

		gg->pool.incomingpos = 0;
        }

	memcpy(gg->pool.incomingb + gg->pool.incomingpos, p, length);
	gg->pool.incomingpos += length;
}


void random_add_heavynoise(void *noise, int length)
{
	DEFINE_CONTEXT_POINTER();
        unsigned char *p = noise;
        int i;

        while (length >= POOLSIZE)
        {
                for (i = 0; i < POOLSIZE; i++)
                {
			gg->pool.pool[i] ^= *p++;
                }
                random_stir();
                length -= POOLSIZE;
        }

        for (i = 0; i < length; i++)
        {
		gg->pool.pool[i] ^= *p++;
        }
        random_stir();
}


static void random_add_heavynoise_bitbybit(void *noise, int length)
{
	DEFINE_CONTEXT_POINTER();
        unsigned char *p = noise;
        int i;

	while (length >= POOLSIZE - gg->pool.poolpos)
        {
		for (i = 0; i < POOLSIZE - gg->pool.poolpos; i++)
                {
			gg->pool.pool[gg->pool.poolpos + i] ^= *p++;
                }
                random_stir();
		length -= POOLSIZE - gg->pool.poolpos;
		gg->pool.poolpos = 0;
        }

        for (i = 0; i < length; i++)
        {
		gg->pool.pool[i] ^= *p++;
        }
	gg->pool.poolpos = i;
}


static void random_timer(void *ctx, unsigned long now)
{
	DEFINE_CONTEXT_POINTER();

	if (gg->random_active > 0 && now == gg->next_noise_collection)
        {
                noise_regular();
		gg->next_noise_collection =
			schedule_timer(NOISE_REGULAR_INTERVAL, random_timer, &gg->pool);
        }
}


void random_ref(void)
{
	DEFINE_CONTEXT_POINTER();

	if (!gg->random_active)
        {
		memset(&gg->pool, 0, sizeof(gg->pool)); /* just to start with */

                noise_get_heavy(random_add_heavynoise_bitbybit);
                random_stir();

		gg->next_noise_collection =
			schedule_timer(NOISE_REGULAR_INTERVAL, random_timer, &gg->pool);
        }
	gg->random_active++;
}


void random_unref(void)
{
	DEFINE_CONTEXT_POINTER();

	if (gg->random_active <= 0)
	{
		return;
	}

	if (gg->random_active == 1)
        {
                random_save_seed();
		expire_timer_context(&gg->pool);
        }
	gg->random_active--;
}


int random_byte(void)
{
	DEFINE_CONTEXT_POINTER();
	assert(gg->random_active);

	if (gg->pool.poolpos >= POOLSIZE)
        {
                random_stir();
        }

	return gg->pool.pool[gg->pool.poolpos++];
}


void random_get_savedata(void **data, int *len)
{
	DEFINE_CONTEXT_POINTER();
        void *buf = snewn(POOLSIZE / 2, char);

        random_stir();
	memcpy(buf, gg->pool.pool + gg->pool.poolpos, POOLSIZE / 2);
        *len = POOLSIZE / 2;
        *data = buf;
        random_stir();
}

