

#include "TerminalConfig.h"


void TerminalConfigGetInt(PuttyContext* context, int valueId, Conf *conf, int confIndex)
{
	int value = (int)SIGNAL_NEED_VALUE(context, valueId);
	conf_set_int(conf, confIndex, value);
}


void TerminalConfigGetString(PuttyContext* context, int stringId, Conf *conf, int confIndex)
{
	char* string = SIGNAL_NEED_STRING(context, stringId);
	conf_set_str(conf, confIndex, string);
	SIGNAL_RELEASE_STRING(context, string);
}


void TerminalConfigGetPath(PuttyContext* context, int stringId, Conf *conf, int confIndex)
{
	char* string = SIGNAL_NEED_STRING(context, stringId);
	Filename* fileName = filename_from_str(string == NULL ? "" : string);
	conf_set_filename(conf, confIndex, fileName);
	SIGNAL_RELEASE_STRING(context, string);
	filename_free(fileName);
}


void TerminalConfigGetFont(PuttyContext* context, int nameId, int charsetId, int boldId, int heightId, Conf *conf, int confIndex)
{
	char* fontname = SIGNAL_NEED_STRING(context, nameId);
	int isbold = (int)SIGNAL_NEED_VALUE(context, boldId);
	int charset = ANSI_CHARSET;   //SIGNAL_NEED_VALUE(context, charsetId);
	int height = (int)SIGNAL_NEED_VALUE(context, heightId);
	FontSpec* ret = fontspec_new(fontname, isbold, height, charset);
	SIGNAL_RELEASE_STRING(context, fontname);

	conf_set_fontspec(conf, confIndex, ret);
	fontspec_free(ret);
}


