
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "TerminalMain.h"
#include "TerminalConfig.h"

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

#define PROGRESS_INTERVAL 50

typedef struct
{
	const struct plug_function_table *fn;
	// The above field _must_ be first in the structure.

	void* frontend;
	void* ldisc;
	Conf* conf;

	uint8_t* dataBuffer;
	int dataSize;
	uint8_t* lockedBuffer;

	int64_t time;
	int64_t timeDelta;
	int error;
	int state;
	int neededEvent;
	uint32_t waitInterval;
	uint32_t lastTick;
	uint32_t lastTimeProgressTick;
	uint32_t currentTick;
	uint32_t maxIdle;
	int32_t index;
	int32_t pendingColumnCount;
	int32_t pendingRowCount;
	uint16_t callbackInstance;
	uint8_t isLogToTerminal;
	uint8_t isPlaying;
	uint8_t verboseLevel;
} PlayerContext;

static void PlayerSize(void* handle, int width, int height);


void PlayerResetTerminal(PuttyContext* context)
{
	if (context->term != NULL)
	{
		term_pwron(context->term, TRUE);
	}
	if (context->ldisc != NULL)
	{
		ldisc_send(context->ldisc, NULL, 0, 0);
	}
}


void PlayerSignalCurrentTime(PlayerContext* context, uint64_t time)
{
	uint32_t interval = GetTickCount() - context->lastTimeProgressTick;
	if (interval >= PROGRESS_INTERVAL)
	{
		DEFINE_CONTEXT_POINTER();
		SIGNAL_CURRENT_SESSION_TIME(gg, time);
		context->lastTimeProgressTick = GetTickCount();
	}
}


void PlayerParseChunk(PlayerContext* context)
{
	uint16_t dimensions[2];
	RecordChunkHeader header;
	memcpy(&header, context->dataBuffer + context->index, sizeof(RecordChunkHeader));

	context->time += header.timer;
	context->index += sizeof(RecordChunkHeader);
	switch (header.type)
	{
	case RECORD_CHUNK_TERMINAL_OUTPUT:
		from_backend(context->frontend, 0, context->dataBuffer + context->index, header.size);
		break;

	case RECORD_CHUNK_SIZE_CHANGE:
		memcpy(dimensions, context->dataBuffer + context->index, 4);
		// TODO: manage terminal dimensions. // TerminalSetSize(context->terminal, dimensions[0], dimensions[1]);
		break;
	}
	context->index += header.size;
}


void PlayerPlay(PlayerContext* context)
{
	if (context->isPlaying == 0 || context->index >= context->dataSize)
	{
		return;
	}
	context->timeDelta = context->currentTick - context->lastTick;
	uint32_t interval = (context->maxIdle > 0 && context->waitInterval > context->maxIdle ? context->maxIdle : context->waitInterval);
	if (context->currentTick - context->lastTick < interval)
	{
		PlayerSignalCurrentTime(context, context->time + context->timeDelta);
		return;
	}

	PlayerParseChunk(context);
	if (context->index >= context->dataSize)
	{
		DEFINE_CONTEXT_POINTER();
		context->isPlaying = 0;
		SIGNAL_CURRENT_SESSION_TIME(gg, -1);
	}

	PlayerSignalCurrentTime(context, context->time);
	uint32_t timer;
	memcpy(&timer, context->dataBuffer + context->index, sizeof(uint32_t));
	context->lastTick = context->currentTick;
	context->waitInterval = timer;
	context->timeDelta = 0;
}


void PlayerDoMoveTo(int64_t targetTime)
{
	DEFINE_CONTEXT_POINTER();
	PlayerContext* context = gg->player;

	if (targetTime < context->time)
	{
		context->index = 0;
		context->time = 0;
		context->timeDelta = 0;
		PlayerResetTerminal(gg);
	}

	gg->term->isHoldingRefresh = 1;
	while (context->index < context->dataSize)
	{
		uint32_t timer;
		memcpy(&timer, context->dataBuffer + context->index, sizeof(uint32_t));
		if (context->time + timer > targetTime)
		{
			context->waitInterval = timer;
			context->timeDelta = targetTime - context->time;
			break;
		}
		PlayerParseChunk(context);
	}
	gg->term->isHoldingRefresh = 0;
	//PlayerSignalCurrentTime(context, context->time + context->timeDelta);
}


void PlayerMain(void *ctx, unsigned long now)
{
	PlayerContext* context = ctx;

	context->currentTick = now;
	PlayerPlay(context);

	uint32_t waitTime;
	if (context->isPlaying)
	{
		uint32_t interval = GetTickCount() - context->lastTick;
		if (interval < PROGRESS_INTERVAL)
		{
			waitTime = interval;
		}
		else
		{
			waitTime = 50;
		}
		schedule_timer(waitTime, PlayerMain, context);
	}
}


void PlayerDoStart()
{
	DEFINE_CONTEXT_POINTER();
	PlayerContext* context = gg->player;

	context->maxIdle = (uint32_t)SIGNAL_NEED_VALUE(gg, ID_VALUE_PLAYER_MAX_IDLE);
	context->isPlaying = 1;
	context->lastTick = GetTickCount() - (uint32_t)context->timeDelta;
	memcpy(&context->waitInterval, context->dataBuffer + context->index, sizeof(uint32_t));
	schedule_timer(0, PlayerMain, context);
}


void PlayerDoStop()
{
	DEFINE_CONTEXT_POINTER();
	PlayerContext* context = gg->player;

	context->isPlaying = 0;
}


void PlayerDoLockBuffer(uint32_t size)
{
	DEFINE_CONTEXT_POINTER();
	PlayerContext* context = gg->player;

	// MutexLock(&context->bufferMutex);
	uint32_t newSize = context->dataSize + size;
	uint8_t* newBuffer = srealloc(context->dataBuffer, newSize);
	if (newBuffer != NULL)
	{
		context->dataBuffer = newBuffer;
		newBuffer += context->dataSize;
		context->dataSize = newSize;
	}

	context->lockedBuffer = newBuffer;
}


// Public API for managed.


DEVOLUTIONS_API void PlayerMoveTo(PuttyContext* context, uint64_t time)
{
	if (context->hwnd != NULL)
	{
		PostMessage(context->hwnd, WM_PLAYER_MOVE_TO, (WPARAM)(time & 0xFFFFFFFF), (LPARAM)((time >> 32) & 0xFFFFFFFF));
	}
}


DEVOLUTIONS_API void PlayerStart(PuttyContext* context)
{
	if (context->hwnd != NULL)
	{
		PostMessage(context->hwnd, WM_PLAYER_START, 0, 0);
	}
}


DEVOLUTIONS_API void PlayerStop(PuttyContext* context)
{
	if (context->hwnd != NULL)
	{
		PostMessage(context->hwnd, WM_PLAYER_STOP, 0, 0);
	}
}


DEVOLUTIONS_API void PlayerReset(PuttyContext* context)
{
	if (context->hwnd != NULL)
	{
		SendMessage(context->hwnd, WM_PLAYER_STOP, 0, 0);
	}
	if (context->player != NULL)
	{
		PlayerContext* player = context->player;
		sfree(player->dataBuffer);
		player->dataBuffer = NULL;
		player->dataSize = 0;
	}
	PlayerResetTerminal(context);
}


DEVOLUTIONS_API uint8_t* PlayerLockBuffer(PuttyContext* context, uint32_t size)
{
	if (context->hwnd != NULL)
	{
		SendMessage(context->hwnd, WM_PLAYER_LOCK_BUFFER, 0, size);
	}

	PlayerContext* player = context->player;
	return player->lockedBuffer;
}


DEVOLUTIONS_API void PlayerUnlockBuffer(PuttyContext* ctx)
{
	//MutexUnlock(&context->bufferMutex);
}


DEVOLUTIONS_API uint64_t PlayerCalculateTime(PuttyContext* ctx)
{
	PlayerContext* player = ctx->player;

	int index = 0;
	uint64_t time = 0;
	RecordChunkHeader header;

	while (index < player->dataSize)
	{
		memcpy(&header, player->dataBuffer + index, sizeof(RecordChunkHeader));
		time += header.timer;
		index += header.size + sizeof(RecordChunkHeader);
	}

	return time;
}


// Putty standard "plugin" API.

static void PlayerLog(Plug plug, int type, SockAddr addr, int port, const char* error_msg, int error_code)
{
	// Stub.
}


static int PlayerClosing(Plug plug, const char* error_msg, int error_code, int calling_back)
{
	// Stub.
	return 0;
}


static int PlayerReceive(Plug plug, int urgent, char* data, int len)
{
	// Stub.
	return 1;
}


static void PlayerSent(Plug plug, int bufsize)
{
	// Stub.
}


static const struct plug_function_table PlayerFunctionTable =
{
	PlayerLog,
	PlayerClosing,
	PlayerReceive,
	PlayerSent
};


static const char* PlayerInit(void* frontend_handle, void** backend_handle, Conf* conf, char* host, int port, char** realhost, int nodelay, int keepalive)
{
	DEFINE_CONTEXT_POINTER();
	PlayerContext* context = snew(PlayerContext);

	context->fn = &PlayerFunctionTable;
	context->frontend = frontend_handle;
	context->conf = conf_copy(conf);
	*backend_handle = context;
	*realhost = dupstr("player");
	gg->player = context;

	context->maxIdle = (uint32_t)SIGNAL_NEED_VALUE(gg, ID_VALUE_PLAYER_MAX_IDLE);

	SIGNAL_CONNECTED(gg);

	return NULL;
}


static void PlayerFree(void* handle)
{
	PlayerContext* context = handle;

	conf_free(context->conf);
	sfree(context);
}


static void PlayerReconfig(void* handle, Conf* conf)
{
	PlayerContext* context = handle;

	conf_free(context->conf);
	context->conf = conf_copy(conf);
}


static int PlayerSend(void* handle, char* buf, int len)
{
	return len;   // This should cause the front end to believe that all the data has been sent.
}


static int PlayerSendBuffer(void *handle)
{
	return 0;   // There is never anything to send.
}


static void PlayerSize(void* handle, int width, int height)
{
	// No size to send.
}


static void PlayerSpecial(void* handle, Telnet_Special code)
{
	// Nothing to do here.
}


static const struct telnet_special* PlayerGetSpecials(void* handle)
{
	// Nothing special for the player.
	return NULL;
}


static int PlayerConnected(void* handle)
{
	// The player is always connected.
	return 1;
}


static int PlayerSendOk(void* handle)
{
	// Nothing to do here.
	return 1;
}


static void PlayerUnthrottle(void* handle, int backlog)
{
	// Nothing to do here, for now...
}


static int PlayerLdisc(void* handle, int option)
{
	// Nothing to do here.
	return 0;
}


static void PlayerProvideLdisc(void* handle, void* ldisc)
{
	PlayerContext* context = handle;

	context->ldisc = ldisc;
}


static void PlayerProvideLogCtx(void* handle, void* logctx)
{
	// Nothing to do here, for now...
}


static int PlayerExitCode(void* handle)
{
	// Nothing to do here, for now...
	return 0;
}


static int PlayerCfgInfo(void* handle)
{
	// Nothing to do here.
	return 0;
}


const Backend PlayerBackend =
{
	PlayerInit,
	PlayerFree,
	PlayerReconfig,
	PlayerSend,
	PlayerSendBuffer,
	PlayerSize,
	PlayerSpecial,
	PlayerGetSpecials,
	PlayerConnected,
	PlayerExitCode,
	PlayerSendOk,
	PlayerLdisc,
	PlayerProvideLdisc,
	PlayerProvideLogCtx,
	PlayerUnthrottle,
	PlayerCfgInfo,
	"player",
	PROT_PLAYER,
	0
};

