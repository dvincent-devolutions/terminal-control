
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "Key.h"


void keyReset(KeyText* key)
{
	key->index = 0;
}


int keyRead(char* buffer, int readSize, KeyText* key)
{
	int size = key->size - key->index;
	if (readSize < size)
	{
		size = readSize;
	}
	if (size > 0)
	{
		memcpy(buffer, key->text + key->index, size);
		key->index += size;
	}
	return size;
}


int keyReadChar(KeyText* key)
{
	if (key->index >= key->size)
	{
		return -1;
	}
	int c = key->text[key->index];
	key->index++;
	return c;
}


void keyUnreadChar(KeyText* key)
{
	key->index--;
}


char* keyReadString(KeyText* key, char* buffer, int bufferSize)
{
	int i = 0;
	while (key->index < key->size && i < bufferSize - 1)
	{
		char c = key->text[key->index++];
		buffer[i++] = c;
		if (c == '\n')
		{
			break;
		}
	}

	if (i == 0)
	{
		return NULL;
	}

	buffer[i] = 0;
	return buffer;
}


void keyInit(KeyText* key, char* keyText)
{
	key->size = (int)strlen(keyText);
	key->text = keyText;
	key->index = 0;
}


int keyGetType(KeyText* key)
{
	char buf[32];
	static const char putty2_sig[] = "PuTTY-User-Key-File-";
	static const char sshcom_sig[] = "---- BEGIN SSH2 ENCRYPTED PRIVAT";
	static const char openssh_sig[] = "-----BEGIN ";
	static const char rsa_signature[] = "SSH PRIVATE KEY FILE FORMAT 1.1\n";
	int i;

	keyReset(key);
	i = keyRead(buf, sizeof(buf), key);
	if (i < 0)
	{
		return SSH_KEYTYPE_UNOPENABLE;
	}
	if (i < 32)
	{
		return SSH_KEYTYPE_UNKNOWN;
	}
	if (!memcmp(buf, rsa_signature, sizeof(rsa_signature) - 1))
	{
		return SSH_KEYTYPE_SSH1;
	}
	if (!memcmp(buf, putty2_sig, sizeof(putty2_sig) - 1))
	{
		return SSH_KEYTYPE_SSH2;
	}
	if (!memcmp(buf, openssh_sig, sizeof(openssh_sig) - 1))
	{
		return SSH_KEYTYPE_OPENSSH;
	}
	if (!memcmp(buf, sshcom_sig, sizeof(sshcom_sig) - 1))
	{
		return SSH_KEYTYPE_SSHCOM;
	}
	return SSH_KEYTYPE_UNKNOWN;    /* unrecognised or EOF */
}


static char* keyReadLine(KeyText* key)
{
	char *ret = snewn(512, char);
	int size = 512, len = 0;
	while (keyReadString(key, ret + len, size - len))
	{
		len += (int)strlen(ret + len);
		if (len > 0 && ret[len - 1] == '\n')
		{
			break;		       /* got a newline, we're done */
		}
		size = len + 512;
		ret = sresize(ret, size, char);
	}
	if (len == 0)
	{		       /* first fgets returned NULL */
		sfree(ret);
		return NULL;
	}
	ret[len] = '\0';
	return ret;
}


static int keyReadHeader(KeyText* key, char *header)
{
	int len = 39;
	int c;

	while (1)
	{
		c = keyReadChar(key);
		if (c == '\n' || c == '\r' || c == EOF)
		{
			return 0;      /* failure */
		}
		if (c == ':')
		{
			c = keyReadChar(key);
			if (c != ' ')
			{
				return 0;
			}
			*header = '\0';
			return 1;      /* success! */
		}
		if (len == 0)
		{
			return 0;      /* failure */
		}
		*header++ = c;
		len--;
	}
	return 0;                      /* failure */
}


static char* keyReadBody(KeyText* key)
{
	char *text;
	int len;
	int size;
	int c;

	size = 128;
	text = snewn(size, char);
	len = 0;
	text[len] = '\0';

	while (1)
	{
		c = keyReadChar(key);
		if (c == '\r' || c == '\n' || c == EOF)
		{
			if (c != EOF)
			{
				c = keyReadChar(key);
				if (c != '\r' && c != '\n')
				{
					keyUnreadChar(key);
				}
			}
			return text;
		}
		if (len + 1 >= size)
		{
			size += 128;
			text = sresize(text, size, char);
		}
		text[len++] = c;
		text[len] = '\0';
	}
}


static unsigned char* keyReadBlob(KeyText* key, int nlines, int *bloblen)
{
	unsigned char *blob;
	char *line;
	int linelen, len;
	int i, j, k;

	/* We expect at most 64 base64 characters, ie 48 real bytes, per line. */
	blob = snewn(48 * nlines, unsigned char);
	len = 0;
	for (i = 0; i < nlines; i++)
	{
		line = keyReadBody(key);
		if (!line)
		{
			sfree(blob);
			return NULL;
		}
		linelen = (int)strlen(line);
		if (linelen % 4 != 0 || linelen > 64)
		{
			sfree(blob);
			sfree(line);
			return NULL;
		}
		for (j = 0; j < linelen; j += 4)
		{
			k = base64_decode_atom(line + j, blob + len);
			if (!k)
			{
				sfree(line);
				sfree(blob);
				return NULL;
			}
			len += k;
		}
		sfree(line);
	}
	*bloblen = len;
	return blob;
}


unsigned char* keyLoadPublic(KeyText* key, char** algorithm, int* pub_blob_len, char** commentptr, const char** errorstr)
{
	char header[40], *b;
	const struct ssh_signkey *alg;
	unsigned char *public_blob;
	int public_blob_len;
	int i;
	const char *error = NULL;
	char *comment;

	public_blob = NULL;
	keyReset(key);

	/* Read the first header line which contains the key type. */
	if (!keyReadHeader(key, header) || (0 != strcmp(header, "PuTTY-User-Key-File-2") && 0 != strcmp(header, "PuTTY-User-Key-File-1")))
	{
		if (0 == strncmp(header, "PuTTY-User-Key-File-", 20))
		{
			error = "PuTTY key format too new";
		}
		else
		{
			error = "not a PuTTY SSH-2 private key";
		}
		goto error;
	}
	error = "file format error";
	if ((b = keyReadBody(key)) == NULL)
	{
		goto error;
	}
	/* Select key algorithm structure. */
	alg = find_pubkey_alg(b);
	if (!alg)
	{
		sfree(b);
		goto error;
	}
	sfree(b);

	/* Read the Encryption header line. */
	if (!keyReadHeader(key, header) || 0 != strcmp(header, "Encryption"))
	{
		goto error;
	}
	if ((b = keyReadBody(key)) == NULL)
	{
		goto error;
	}
	sfree(b);                      /* we don't care */

	/* Read the Comment header line. */
	if (!keyReadHeader(key, header) || 0 != strcmp(header, "Comment"))
	{
		goto error;
	}
	if ((comment = keyReadBody(key)) == NULL)
	{
		goto error;
	}

	if (commentptr)
	{
		*commentptr = comment;
	}
	else
	{
		sfree(comment);
	}

	/* Read the Public-Lines header line and the public blob. */
	if (!keyReadHeader(key, header) || 0 != strcmp(header, "Public-Lines"))
	{
		goto error;
	}
	if ((b = keyReadBody(key)) == NULL)
	{
		goto error;
	}
	i = atoi(b);
	sfree(b);
	if ((public_blob = keyReadBlob(key, i, &public_blob_len)) == NULL)
	{
		goto error;
	}

	if (pub_blob_len)
	{
		*pub_blob_len = public_blob_len;
	}
	if (algorithm)
	{
		*algorithm = alg->name;
	}
	return public_blob;

	/*
	* Error processing.
	*/
error:
	if (public_blob)
	{
		sfree(public_blob);
	}
	if (errorstr)
	{
		*errorstr = error;
	}
	return NULL;
}


int keyIsEncrypted(KeyText* key, char** commentptr)
{
	char header[40], *b, *comment;
	int ret;

	if (commentptr)
	{
		*commentptr = NULL;
	}

	keyReset(key);

	if (!keyReadHeader(key, header) || (0 != strcmp(header, "PuTTY-User-Key-File-2") && 0 != strcmp(header, "PuTTY-User-Key-File-1")))
	{
		return 0;
	}
	if ((b = keyReadBody(key)) == NULL)
	{
		return 0;
	}
	sfree(b);                      /* we don't care about key type here */

	/* Read the Encryption header line. */
	if (!keyReadHeader(key, header) || 0 != strcmp(header, "Encryption"))
	{
		return 0;
	}
	if ((b = keyReadBody(key)) == NULL)
	{
		return 0;
	}

	/* Read the Comment header line. */
	if (!keyReadHeader(key, header) || 0 != strcmp(header, "Comment"))
	{
		sfree(b);
		return 1;
	}
	if ((comment = keyReadBody(key)) == NULL)
	{
		sfree(b);
		return 1;
	}

	if (commentptr)
	{
		*commentptr = comment;
	}
	else
	{
		sfree(comment);
	}

	if (!strcmp(b, "aes256-cbc"))
	{
		ret = 1;
	}
	else
	{
		ret = 0;
	}
	sfree(b);
	return ret;
}


struct ssh2_userkey* keyLoadPrivate(KeyText* key, char* passphrase, const char** errorstr)
{
	char header[40], *b, *encryption, *comment, *mac;
	const struct ssh_signkey *alg;
	struct ssh2_userkey *ret;
	int cipher, cipherblk;
	unsigned char *public_blob, *private_blob;
	int public_blob_len, private_blob_len;
	int i, is_mac, old_fmt;
	int passlen = passphrase ? (int)strlen(passphrase) : 0;
	const char *error = NULL;

	ret = NULL;                    /* return NULL for most errors */
	encryption = comment = mac = NULL;
	public_blob = private_blob = NULL;

	keyReset(key);

	/* Read the first header line which contains the key type. */
	if (!keyReadHeader(key, header))
	{
		goto error;
	}
	if (0 == strcmp(header, "PuTTY-User-Key-File-2"))
	{
		old_fmt = 0;
	}
	else if (0 == strcmp(header, "PuTTY-User-Key-File-1"))
	{
		/* this is an old key file; warn and then continue */
		old_keyfile_warning();
		old_fmt = 1;
	}
	else if (0 == strncmp(header, "PuTTY-User-Key-File-", 20))
	{
		/* this is a key file FROM THE FUTURE; refuse it, but with a
		* more specific error message than the generic one below */
		error = "PuTTY key format too new";
		goto error;
	}
	else
	{
		error = "not a PuTTY SSH-2 private key";
		goto error;
	}
	error = "file format error";
	if ((b = keyReadBody(key)) == NULL)
	{
		goto error;
	}
	/* Select key algorithm structure. */
	alg = find_pubkey_alg(b);
	if (!alg)
	{
		sfree(b);
		goto error;
	}
	sfree(b);

	/* Read the Encryption header line. */
	if (!keyReadHeader(key, header) || 0 != strcmp(header, "Encryption"))
	{
		goto error;
	}
	if ((encryption = keyReadBody(key)) == NULL)
	{
		goto error;
	}
	if (!strcmp(encryption, "aes256-cbc"))
	{
		cipher = 1;
		cipherblk = 16;
	}
	else if (!strcmp(encryption, "none"))
	{
		cipher = 0;
		cipherblk = 1;
	}
	else
	{
		goto error;
	}

	/* Read the Comment header line. */
	if (!keyReadHeader(key, header) || 0 != strcmp(header, "Comment"))
	{
		goto error;
	}
	if ((comment = keyReadBody(key)) == NULL)
	{
		goto error;
	}

	/* Read the Public-Lines header line and the public blob. */
	if (!keyReadHeader(key, header) || 0 != strcmp(header, "Public-Lines"))
	{
		goto error;
	}
	if ((b = keyReadBody(key)) == NULL)
	{
		goto error;
	}
	i = atoi(b);
	sfree(b);
	if ((public_blob = keyReadBlob(key, i, &public_blob_len)) == NULL)
	{
		goto error;
	}

	/* Read the Private-Lines header line and the Private blob. */
	if (!keyReadHeader(key, header) || 0 != strcmp(header, "Private-Lines"))
	{
		goto error;
	}
	if ((b = keyReadBody(key)) == NULL)
	{
		goto error;
	}
	i = atoi(b);
	sfree(b);
	if ((private_blob = keyReadBlob(key, i, &private_blob_len)) == NULL)
	{
		goto error;
	}

	/* Read the Private-MAC or Private-Hash header line. */
	if (!keyReadHeader(key, header))
	{
		goto error;
	}
	if (0 == strcmp(header, "Private-MAC"))
	{
		if ((mac = keyReadBody(key)) == NULL)
		{
			goto error;
		}
		is_mac = 1;
	}
	else if (0 == strcmp(header, "Private-Hash") && old_fmt)
	{
		if ((mac = keyReadBody(key)) == NULL)
		{
			goto error;
		}
		is_mac = 0;
	}
	else
	{
		goto error;
	}

	/*
	* Decrypt the private blob.
	*/
	if (cipher)
	{
		unsigned char key[40];
		SHA_State s;

		if (!passphrase)
		{
			goto error;
		}
		if (private_blob_len % cipherblk)
		{
			goto error;
		}

		SHA_Init(&s);
		SHA_Bytes(&s, "\0\0\0\0", 4);
		SHA_Bytes(&s, passphrase, passlen);
		SHA_Final(&s, key + 0);
		SHA_Init(&s);
		SHA_Bytes(&s, "\0\0\0\1", 4);
		SHA_Bytes(&s, passphrase, passlen);
		SHA_Final(&s, key + 20);
		aes256_decrypt_pubkey(key, private_blob, private_blob_len);
	}

	/*
	* Verify the MAC.
	*/
	{
		char realmac[41];
		unsigned char binary[20];
		unsigned char *macdata;
		int maclen;
		int free_macdata;

		if (old_fmt)
		{
			/* MAC (or hash) only covers the private blob. */
			macdata = private_blob;
			maclen = private_blob_len;
			free_macdata = 0;
		}
		else
		{
			unsigned char *p;
			int namelen = (int)strlen(alg->name);
			int enclen = (int)strlen(encryption);
			int commlen = (int)strlen(comment);
			maclen = (4 + namelen +
				4 + enclen +
				4 + commlen +
				4 + public_blob_len +
				4 + private_blob_len);
			macdata = snewn(maclen, unsigned char);
			p = macdata;
#define DO_STR(s,len) PUT_32BIT(p,(len)); memcpy(p + 4,(s),(len)); p += 4 + (len)
			DO_STR(alg->name, namelen);
			DO_STR(encryption, enclen);
			DO_STR(comment, commlen);
			DO_STR(public_blob, public_blob_len);
			DO_STR(private_blob, private_blob_len);

			free_macdata = 1;
		}

		if (is_mac)
		{
			SHA_State s;
			unsigned char mackey[20];
			char header[] = "putty-private-key-file-mac-key";

			SHA_Init(&s);
			SHA_Bytes(&s, header, sizeof(header) - 1);
			if (cipher && passphrase)
			{
				SHA_Bytes(&s, passphrase, passlen);
			}
			SHA_Final(&s, mackey);

			hmac_sha1_simple(mackey, 20, macdata, maclen, binary);

			smemclr(mackey, sizeof(mackey));
			smemclr(&s, sizeof(s));
		}
		else
		{
			SHA_Simple(macdata, maclen, binary);
		}

		if (free_macdata)
		{
			smemclr(macdata, maclen);
			sfree(macdata);
		}

		for (i = 0; i < 20; i++)
		{
			sprintf(realmac + 2 * i, "%02x", binary[i]);
		}

		if (strcmp(mac, realmac))
		{
			/* An incorrect MAC is an unconditional Error if the key is
			* unencrypted. Otherwise, it means Wrong Passphrase. */
			if (cipher)
			{
				error = "wrong passphrase";
				ret = (struct ssh2_userkey*)SSH2_WRONG_PASSPHRASE;
			}
			else
			{
				error = "MAC failed";
				ret = NULL;
			}
			goto error;
		}
	}
	sfree(mac);
	mac = NULL;

	/*
	* Create and return the key.
	*/
	ret = snew(struct ssh2_userkey);
	ret->alg = alg;
	ret->comment = comment;
	ret->data = alg->createkey(public_blob, public_blob_len,
		private_blob, private_blob_len);
	if (!ret->data)
	{
		sfree(ret);
		ret = NULL;
		error = "createkey failed";
		goto error;
	}
	sfree(public_blob);
	smemclr(private_blob, private_blob_len);
	sfree(private_blob);
	sfree(encryption);
	if (errorstr)
	{
		*errorstr = NULL;
	}
	return ret;

	/*
	* Error processing.
	*/
error:
	if (comment)
	{
		sfree(comment);
	}
	if (encryption)
	{
		sfree(encryption);
	}
	if (mac)
	{
		sfree(mac);
	}
	if (public_blob)
	{
		sfree(public_blob);
	}
	if (private_blob)
	{
		smemclr(private_blob, private_blob_len);
		sfree(private_blob);
	}
	if (errorstr)
	{
		*errorstr = error;
	}
	return ret;
}


//-------


enum
{
	OSSH_DSA,
	OSSH_RSA
};

enum
{
	OSSH_ENC_3DES,
	OSSH_ENC_AES
};

struct openssh_key
{
	int type;
	int encrypted, encryption;
	char iv[32];
	unsigned char *keyblob;
	int keyblob_len, keyblob_size;
};


#define isbase64(c) (((c) >= 'A' && (c) <= 'Z') || \
	((c) >= 'a' && (c) <= 'z') || \
	((c) >= '0' && (c) <= '9') || \
	(c) == '+' || (c) == '/' || (c) == '=' \
	)

void strip_crlf(char *str)
{
	char *p = str + strlen(str);

	while (p > str && (p[-1] == '\r' || p[-1] == '\n'))
	{
		*--p = '\0';
	}
}


static int ber_read_id_len(void *source, int sourcelen, int *id, int *length, int *flags)
{
	unsigned char *p = (unsigned char *)source;

	if (sourcelen == 0)
	{
		return -1;
	}

	*flags = (*p & 0xE0);
	if ((*p & 0x1F) == 0x1F)
	{
		*id = 0;
		while (*p & 0x80)
		{
			p++, sourcelen--;
			if (sourcelen == 0)
			{
				return -1;
			}
			*id = (*id << 7) | (*p & 0x7F);
		}
		p++, sourcelen--;
	}
	else
	{
		*id = *p & 0x1F;
		p++, sourcelen--;
	}

	if (sourcelen == 0)
	{
		return -1;
	}

	if (*p & 0x80)
	{
		unsigned len;
		int n = *p & 0x7F;
		p++, sourcelen--;
		if (sourcelen < n)
		{
			return -1;
		}
		len = 0;
		while (n--)
		{
			len = (len << 8) | (*p++);
		}
		sourcelen -= n;
		*length = toint(len);
	}
	else
	{
		*length = *p;
		p++, sourcelen--;
	}

	return (int)(p - (unsigned char *)source);
}


static struct openssh_key* load_openssh_key(KeyText* keyText, const char** errmsg_p)
{
	struct openssh_key *ret;
	char *line = NULL;
	char *errmsg, *p;
	int headers_done;
	char base64_bit[4];
	int base64_chars = 0;

	ret = snew(struct openssh_key);
	ret->keyblob = NULL;
	ret->keyblob_len = ret->keyblob_size = 0;
	ret->encrypted = 0;
	memset(ret->iv, 0, sizeof(ret->iv));

	if (!(line = keyReadLine(keyText)))
	{
		errmsg = "unexpected end of file";
		goto error;
	}
	strip_crlf(line);
	if (!strstartswith(line, "-----BEGIN ") || !strendswith(line, "PRIVATE KEY-----"))
	{
		errmsg = "file does not begin with OpenSSH key header";
		goto error;
	}
	if (!strcmp(line, "-----BEGIN RSA PRIVATE KEY-----"))
	{
		ret->type = OSSH_RSA;
	}
	else if (!strcmp(line, "-----BEGIN DSA PRIVATE KEY-----"))
	{
		ret->type = OSSH_DSA;
	}
	else
	{
		errmsg = "unrecognised key type";
		goto error;
	}
	smemclr(line, strlen(line));
	sfree(line);
	line = NULL;

	headers_done = 0;
	while (1)
	{
		if (!(line = keyReadLine(keyText)))
		{
			errmsg = "unexpected end of file";
			goto error;
		}
		strip_crlf(line);
		if (strstartswith(line, "-----END ") &&
			strendswith(line, "PRIVATE KEY-----"))
		{
			sfree(line);
			line = NULL;
			break;                     /* done */
		}
		if ((p = strchr(line, ':')) != NULL)
		{
			if (headers_done)
			{
				errmsg = "header found in body of key data";
				goto error;
			}
			*p++ = '\0';
			while (*p && isspace((unsigned char)*p))
			{
				p++;
			}
			if (!strcmp(line, "Proc-Type"))
			{
				if (p[0] != '4' || p[1] != ',')
				{
					errmsg = "Proc-Type is not 4 (only 4 is supported)";
					goto error;
				}
				p += 2;
				if (!strcmp(p, "ENCRYPTED"))
				{
					ret->encrypted = 1;
				}
			}
			else if (!strcmp(line, "DEK-Info"))
			{
				int i, j, ivlen;

				if (!strncmp(p, "DES-EDE3-CBC,", 13))
				{
					ret->encryption = OSSH_ENC_3DES;
					ivlen = 8;
				}
				else if (!strncmp(p, "AES-128-CBC,", 12))
				{
					ret->encryption = OSSH_ENC_AES;
					ivlen = 16;
				}
				else
				{
					errmsg = "unsupported cipher";
					goto error;
				}
				p = strchr(p, ',') + 1; /* always non-NULL, by above checks */
				for (i = 0; i < ivlen; i++)
				{
					if (1 != sscanf(p, "%2x", &j))
					{
						errmsg = "expected more iv data in DEK-Info";
						goto error;
					}
					ret->iv[i] = j;
					p += 2;
				}
				if (*p)
				{
					errmsg = "more iv data than expected in DEK-Info";
					goto error;
				}
			}
		}
		else
		{
			headers_done = 1;

			p = line;
			while (isbase64(*p))
			{
				base64_bit[base64_chars++] = *p;
				if (base64_chars == 4)
				{
					unsigned char out[3];
					int len;

					base64_chars = 0;

					len = base64_decode_atom(base64_bit, out);

					if (len <= 0)
					{
						errmsg = "invalid base64 encoding";
						goto error;
					}

					if (ret->keyblob_len + len > ret->keyblob_size)
					{
						ret->keyblob_size = ret->keyblob_len + len + 256;
						ret->keyblob = sresize(ret->keyblob, ret->keyblob_size,
							unsigned char);
					}

					memcpy(ret->keyblob + ret->keyblob_len, out, len);
					ret->keyblob_len += len;

					smemclr(out, sizeof(out));
				}

				p++;
			}
		}
		smemclr(line, strlen(line));
		sfree(line);
		line = NULL;
	}

	keyReset(keyText);

	if (ret->keyblob_len == 0 || !ret->keyblob)
	{
		errmsg = "key body not present";
		goto error;
	}

	if (ret->encrypted && ret->keyblob_len % 8 != 0)
	{
		errmsg = "encrypted key blob is not a multiple of cipher block size";
		goto error;
	}

	smemclr(base64_bit, sizeof(base64_bit));
	if (errmsg_p)
	{
		*errmsg_p = NULL;
	}
	return ret;

error:
	if (line)
	{
		smemclr(line, strlen(line));
		sfree(line);
		line = NULL;
	}
	smemclr(base64_bit, sizeof(base64_bit));
	if (ret)
	{
		if (ret->keyblob)
		{
			smemclr(ret->keyblob, ret->keyblob_size);
			sfree(ret->keyblob);
		}
		smemclr(ret, sizeof(*ret));
		sfree(ret);
	}
	if (errmsg_p)
	{
		*errmsg_p = errmsg;
	}
	return NULL;
}


struct ssh2_userkey* keyLoadPrivateOpenSsh(KeyText* keyText, char* passphrase, const char** errmsg_p)
{
	struct openssh_key *key = load_openssh_key(keyText, errmsg_p);
	struct ssh2_userkey *retkey;
	unsigned char *p;
	int ret, id, len, flags;
	int i, num_integers;
	struct ssh2_userkey *retval = NULL;
	char *errmsg;
	unsigned char *blob;
	int blobsize = 0, blobptr, privptr;
	char *modptr = NULL;
	int modlen = 0;

	blob = NULL;

	if (!key)
	{
		return NULL;
	}

	if (key->encrypted)
	{
		/*
		* Derive encryption key from passphrase and iv/salt:
		*
		*  - let block A equal MD5(passphrase || iv)
		*  - let block B equal MD5(A || passphrase || iv)
		*  - block C would be MD5(B || passphrase || iv) and so on
		*  - encryption key is the first N bytes of A || B
		*
		* (Note that only 8 bytes of the iv are used for key
		* derivation, even when the key is encrypted with AES and
		* hence there are 16 bytes available.)
		*/
		struct MD5Context md5c;
		unsigned char keybuf[32];

		MD5Init(&md5c);
		MD5Update(&md5c, (unsigned char *)passphrase, (unsigned int)strlen(passphrase));
		MD5Update(&md5c, (unsigned char *)key->iv, 8);
		MD5Final(keybuf, &md5c);

		MD5Init(&md5c);
		MD5Update(&md5c, keybuf, 16);
		MD5Update(&md5c, (unsigned char *)passphrase, (unsigned int)strlen(passphrase));
		MD5Update(&md5c, (unsigned char *)key->iv, 8);
		MD5Final(keybuf + 16, &md5c);

		/*
		* Now decrypt the key blob.
		*/
		if (key->encryption == OSSH_ENC_3DES)
		{
			des3_decrypt_pubkey_ossh(keybuf, (unsigned char *)key->iv,
				key->keyblob, key->keyblob_len);
		}
		else
		{
			void *ctx;
			//assert(key->encryption == OSSH_ENC_AES);
			ctx = aes_make_context();
			aes128_key(ctx, keybuf);
			aes_iv(ctx, (unsigned char *)key->iv);
			aes_ssh2_decrypt_blk(ctx, key->keyblob, key->keyblob_len);
			aes_free_context(ctx);
		}

		smemclr(&md5c, sizeof(md5c));
		smemclr(keybuf, sizeof(keybuf));
	}

	/*
	* Now we have a decrypted key blob, which contains an ASN.1
	* encoded private key. We must now untangle the ASN.1.
	*
	* We expect the whole key blob to be formatted as a SEQUENCE
	* (0x30 followed by a length code indicating that the rest of
	* the blob is part of the sequence). Within that SEQUENCE we
	* expect to see a bunch of INTEGERs. What those integers mean
	* depends on the key type:
	*
	*  - For RSA, we expect the integers to be 0, n, e, d, p, q,
	*    dmp1, dmq1, iqmp in that order. (The last three are d mod
	*    (p-1), d mod (q-1), inverse of q mod p respectively.)
	*
	*  - For DSA, we expect them to be 0, p, q, g, y, x in that
	*    order.
	*/

	p = key->keyblob;

	/* Expect the SEQUENCE header. Take its absence as a failure to
	* decrypt, if the key was encrypted. */
	ret = ber_read_id_len(p, key->keyblob_len, &id, &len, &flags);
	p += ret;
	if (ret < 0 || id != 16 || len < 0 ||
		key->keyblob + key->keyblob_len - p < len)
	{
		errmsg = "ASN.1 decoding failure";
		retval = (key->encrypted ? (void*)SSH2_WRONG_PASSPHRASE : NULL);
		goto error;
	}

	/* Expect a load of INTEGERs. */
	if (key->type == OSSH_RSA)
	{
		num_integers = 9;
	}
	else if (key->type == OSSH_DSA)
	{
		num_integers = 6;
	}
	else
	{
		num_integers = 0;              /* placate compiler warnings */

	}
	/*
	* Space to create key blob in.
	*/
	blobsize = 256 + key->keyblob_len;
	blob = snewn(blobsize, unsigned char);
	PUT_32BIT(blob, 7);
	if (key->type == OSSH_DSA)
	{
		memcpy(blob + 4, "ssh-dss", 7);
	}
	else if (key->type == OSSH_RSA)
	{
		memcpy(blob + 4, "ssh-rsa", 7);
	}
	blobptr = 4 + 7;
	privptr = -1;

	for (i = 0; i < num_integers; i++)
	{
		ret = ber_read_id_len(p, (int)(key->keyblob + key->keyblob_len - p), &id, &len, &flags);
		p += ret;
		if (ret < 0 || id != 2 || len < 0 ||
			key->keyblob + key->keyblob_len - p < len)
		{
			errmsg = "ASN.1 decoding failure";
			retval = key->encrypted ? (void*)SSH2_WRONG_PASSPHRASE : NULL;
			goto error;
		}

		if (i == 0)
		{
			/*
			* The first integer should be zero always (I think
			* this is some sort of version indication).
			*/
			if (len != 1 || p[0] != 0)
			{
				errmsg = "version number mismatch";
				goto error;
			}
		}
		else if (key->type == OSSH_RSA)
		{
			/*
			* Integers 1 and 2 go into the public blob but in the
			* opposite order; integers 3, 4, 5 and 8 go into the
			* private blob. The other two (6 and 7) are ignored.
			*/
			if (i == 1)
			{
				/* Save the details for after we deal with number 2. */
				modptr = (char *)p;
				modlen = len;
			}
			else if (i != 6 && i != 7)
			{
				PUT_32BIT(blob + blobptr, len);
				memcpy(blob + blobptr + 4, p, len);
				blobptr += 4 + len;
				if (i == 2)
				{
					PUT_32BIT(blob + blobptr, modlen);
					memcpy(blob + blobptr + 4, modptr, modlen);
					blobptr += 4 + modlen;
					privptr = blobptr;
				}
			}
		}
		else if (key->type == OSSH_DSA)
		{
			/*
			* Integers 1-4 go into the public blob; integer 5 goes
			* into the private blob.
			*/
			PUT_32BIT(blob + blobptr, len);
			memcpy(blob + blobptr + 4, p, len);
			blobptr += 4 + len;
			if (i == 4)
			{
				privptr = blobptr;
			}
		}

		/* Skip past the number. */
		p += len;
	}

	/*
	* Now put together the actual key. Simplest way to do this is
	* to assemble our own key blobs and feed them to the createkey
	* functions; this is a bit faffy but it does mean we get all
	* the sanity checks for free.
	*/
	//assert(privptr > 0);               /* should have bombed by now if not */
	retkey = snew(struct ssh2_userkey);
	retkey->alg = (key->type == OSSH_RSA ? &ssh_rsa : &ssh_dss);
	retkey->data = retkey->alg->createkey(blob, privptr,
		blob + privptr, blobptr - privptr);
	if (!retkey->data)
	{
		sfree(retkey);
		errmsg = "unable to create key data structure";
		goto error;
	}

	retkey->comment = dupstr("imported-openssh-key");
	errmsg = NULL;                     /* no error */
	retval = retkey;

error:
	if (blob)
	{
		smemclr(blob, blobsize);
		sfree(blob);
	}
	smemclr(key->keyblob, key->keyblob_size);
	sfree(key->keyblob);
	smemclr(key, sizeof(*key));
	sfree(key);
	if (errmsg_p)
	{
		*errmsg_p = errmsg;
	}
	return retval;
}


