﻿namespace Devolutions.Terminal
{
    using System;
    using System.IO;
    using System.Runtime.InteropServices;

    public class DevolutionsTerminalLoader
    {
        private const string DevolutionsTerminal = "DevolutionsTerminalNative.dll";

        public static bool IsLoaded { get; set; }

        public static void Absolute(string path)
        {
            // This is for the development environment.
            LoadLibrary(path);
            IsLoaded = true;
        }

        public static void LoadLibrary()
        {
            if (IsLoaded)
            {
                return;
            }

            LoadLibrary(Environment.Is64BitProcess ? Path.Combine("x64", DevolutionsTerminal) : Path.Combine("x86", DevolutionsTerminal));
            IsLoaded = true;
        }

        [DllImport("kernel32", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Auto, BestFitMapping = false, ThrowOnUnmappableChar = true, SetLastError = true)]
        private static extern IntPtr LoadLibrary(string fileName);
    }
}