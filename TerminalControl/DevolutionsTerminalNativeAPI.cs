﻿namespace Devolutions.Terminal
{
    using System;
    using System.Runtime.InteropServices;

    [UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
    internal delegate void DelegateCallback();

    [UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
    internal delegate void DelegateSignal(int signal);

    [UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
    internal delegate void DelegateLog(ushort logType, IntPtr utf8Text);

    [UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
    internal delegate long DelegateNeedValue(ushort valueId);

    [UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
    internal delegate IntPtr DelegateNeedString(ushort stringId);

    [UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
    internal delegate void DelegateReleaseString(IntPtr utf8String);

    [UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
    internal delegate void DelegateSetError(ushort error);

    [UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
    internal delegate ushort DelegateSshServerAuthentication(ushort code, IntPtr fingerprint);

    [UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
    internal delegate ushort DelegateSshIsServerKnown(IntPtr key);

    [UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
    internal delegate void DelegateSessionTime(long time);

    internal enum StringId
    {
        HostName = 0,
        UserName = 1,
        Password = 2,
        PublicKeyFilePath = 3,
        TerminalType = 4,
        LineCodePage = 5,
        LogFileName = 6,
        ProxyHost = 7,
        ProxyUserName = 8,
        ProxyPassword = 9,
        ProxyExcludeList = 10,
        ProxyTelnetCommand = 11,
        X11AuthFile = 12,
        X11Display = 13,
        SerialLine = 14,
        Font = 15,
        Tunnels = 16,
        KeyData = 17,
        KeyPassword = 18,
        RemoteCommand = 19,
        DoubleClickDelimiters = 20,
        RealHostName = 21,
        ExpectedPasswordPrompt = 22,
        ExpectedUserPrompt = 23,
        SmartCardCertificateId = 24,
        TerminalCopy = 25,
        PingText = 26,
        Answerback = 27,
    }

    internal enum ValueId
    {
        HasFocus = 0,
        PortNumber = 1,
        Protocol = 2,
        AddressFamily = 3,
        AutoWrapMode = 4,
        LogType = 5,
        LocalEcho = 6,
        MouseIsXterm = 7,
        NoDBackspace = 8,
        CRImpliesLF = 9,
        LFImpliesCR = 10,
        ScrollbackLines = 11,
        ApplicationCursorKeys = 12,
        NoApplicationKeys = 13,
        ApplicationKeypad = 14,
        BackspaceIsDelete = 15,
        RXVTHomeEnd = 16,
        LinuxFunctionKeys = 17,
        TryAgent = 18,
        AgentFwd = 19,
        TcpKeepAlive = 20,
        PingInterval = 21,
        SshNoShell = 22,
        CloseOnExit = 23,
        ProxyDns = 24,
        ProxyPort = 25,
        ProxyMethod = 26,
        ProxyLocalHost = 27,
        X11AuthType = 28,
        X11Forward = 29,
        SerialDataBits = 30,
        SerialFlowControl = 31,
        SerialParity = 32,
        SerialSpeed = 33,
        SerialStopHalfbits = 34,
        TerminalFontHeight = 35,
        Colors = 36,   // Next is 102.
        LogOverwrite = 102,
        Verbose = 103,
        CursorType = 104,
        CursorBlink = 105,
        GssapiAuth = 106,
        RealPortNumber = 107,
        FontIsBold = 108,
        FontQuality = 109,
        ResetScrollOnDisplay = 110,
        PlayerMaxIdle = 111,
        IsRecording = 112,
        TrySmartCardCertificateAuthentication = 113,
        GssForward = 114,
        IsInteractiveAuthOnTerminal = 115,
        IsControlWheelDisabled = 116,
    }

    internal enum SignalId
    {
        Connected = 1,
        Disconnected = 2,
        Denied = 3,
        Beep = 4,
        NewRecordData = 5,
        Closed = 6,
        SequenceSuccess = 7,
        SequenceFail = 8,
        SequenceTimeout = 9,
    }

    internal class DevolutionsTerminalNativeAPI
    {
        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtActualTerminalSize", CallingConvention = CallingConvention.Cdecl)]
        public static extern long ActualTerminalSize(IntPtr paintContext);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtConnect", CallingConvention = CallingConvention.Cdecl)]
        public static extern short Connect(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtCopyOutput", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CopyOutput(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtNew", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CreateContext(IntPtr parent, IntPtr callbacks);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtDisconnect", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Disconnect(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtEndThread", CallingConvention = CallingConvention.Cdecl)]
        public static extern void EndThread(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtGetPort", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetTunnelPort(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtRelease", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Release(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtResize", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Resize(IntPtr context, int width, int height);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtSend", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Send(IntPtr context, IntPtr commandString);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtSendSequence", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SendSequence(IntPtr context, IntPtr sequence);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtSetFocus", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SetFocus(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtReconfig", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Reconfig(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtResetTerminal", CallingConvention = CallingConvention.Cdecl)]
        public static extern void ResetTerminal(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtClearScrollBack", CallingConvention = CallingConvention.Cdecl)]
        public static extern void ClearScrollBack(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "PlayerStart", CallingConvention = CallingConvention.Cdecl)]
        public static extern void PlayerStart(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "PlayerStop", CallingConvention = CallingConvention.Cdecl)]
        public static extern void PlayerStop(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "PlayerMoveTo", CallingConvention = CallingConvention.Cdecl)]
        public static extern void PlayerMoveTo(IntPtr context, long time);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "PlayerReset", CallingConvention = CallingConvention.Cdecl)]
        public static extern void PlayerReset(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "PlayerLockBuffer", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr PlayerLockBuffer(IntPtr context, int size);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "PlayerUnlockBuffer", CallingConvention = CallingConvention.Cdecl)]
        public static extern void PlayerUnlockBuffer(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "PlayerCalculateTime", CallingConvention = CallingConvention.Cdecl)]
        public static extern long PlayerGetDuration(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "SyntaxColoringAdd", CallingConvention = CallingConvention.Cdecl)]
        public static extern short SyntaxColoringAdd(IntPtr context, IntPtr keyword, int foreColorIndex, int backColorIndex, byte isCompleteWord, byte isCaseSensitive, byte isUnderlined, byte isRegex);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "SyntaxColoringIsRegexGood", CallingConvention = CallingConvention.Cdecl)]
        public static extern int SyntaxColoringIsRegexGood(IntPtr regex, out int error, out int index);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "SyntaxColoringReleaseMemory", CallingConvention = CallingConvention.Cdecl)]
        public static extern short SyntaxColoringReleaseMemory(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "SyntaxColoringApplyChanges", CallingConvention = CallingConvention.Cdecl)]
        public static extern short SyntaxColoringApplyChanges(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "SmartCardGetCAPICertId", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SmartCardGetCAPICertId(IntPtr hwnd);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "SmartCardGetPKSCCertId", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SmartCardGetPKSCCertId(IntPtr hwnd);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "SmartCardGetPublicKey", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SmartCardGetPublicKey(string certId);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "SmartCardDisplayCert", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SmartCardDisplayCert(string certId, IntPtr hwnd);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "SmartCardFree", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr SmartCardFree(IntPtr certId);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "dtStartPing", CallingConvention = CallingConvention.Cdecl)]
        public static extern void StartPing(IntPtr context);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "TerminalLockRecordData", CallingConvention = CallingConvention.Cdecl)]
        public static extern long TerminalLockBuffer(IntPtr context, out IntPtr buffer, out int size);

        [DllImport("DevolutionsTerminalNative", EntryPoint = "TerminalUnlockRecordData", CallingConvention = CallingConvention.Cdecl)]
        public static extern void TerminalUnlockBuffer(IntPtr context);

        // Native structure marshalling struct: do NOT change member order.
        [StructLayout(LayoutKind.Sequential)]
        public struct Callbacks
        {
            public IntPtr Signal;
            public IntPtr NeedValue;
            public IntPtr NeedString;
            public IntPtr Log;
            public IntPtr ReleaseString;
            public IntPtr SetError;
            public IntPtr SshServerAuthentication;
            public IntPtr SshIsServerKnown;
            public IntPtr CurrentPlayTime;
        }
    }
}
