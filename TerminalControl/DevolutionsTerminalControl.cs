﻿namespace Devolutions.Terminal
{
    using System;
    using System.Collections;
    using System.Collections.Concurrent;
    using System.Diagnostics.CodeAnalysis;
    using System.Drawing;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Threading;
    using System.Windows.Forms;

    using Devolutions.Protocols;

    [SuppressMessage("ReSharper", "PrivateFieldCanBeConvertedToLocalVariable", Justification = "no clue why")]
    public sealed class DevolutionsTerminalControl : Control, IVT100Terminal
    {
        private readonly DevolutionsTerminalNativeAPI.Callbacks callbacks;

        private readonly ConcurrentQueue<Command> commandQueue;

        private readonly System.Timers.Timer commandTimer;

        private readonly DelegateSessionTime currentPlayTimeDelegate;

        private readonly DevolutionsTerminalNativeAPI.Callbacks jumpCallbacks;

        private readonly DelegateNeedString jumpNeedStringDelegate;

        private readonly DelegateNeedValue jumpNeedValueDelegate;

        private readonly DelegateSignal jumpSignalDelegate;

        private readonly DelegateSshIsServerKnown jumpSshIsServerKnownDelegate;

        private readonly DelegateLog logDelegate;

        private readonly DelegateNeedString needStringDelegate;

        private readonly DelegateNeedValue needValueDelegate;

        private readonly DelegateReleaseString releaseStringDelegate;

        private readonly DelegateSetError setErrorDelegate;

        private readonly DelegateSignal signalDelegate;

        private readonly DelegateSshIsServerKnown sshIsServerKnownDelegate;

        private readonly DelegateSshServerAuthentication sshServerAuthenticationDelegate;

        private readonly ArrayList syntaxColoringItems;

        private readonly Queue sequenceQueue;

        private SecretString cachedJumpHostPassword;

        private SecretString cachedJumpHostPublicKeyPassword;

        private SecretString cachedKeyPassword;

        private SecretString cachedPassword;

        private SecretString cachedProxyPassword;

        private string cachedUser;

        private IntPtr context;

        private bool contextIsActive;

        private Tunnel[] dynamicTunnels;

        private bool focusState;

        private IntPtr hwnd;

        private string jumpCachedUser;

        private IntPtr jumpContext;

        private bool jumpContextIsActive;

        private int jumpPort;

        private Tunnel[] localTunnels;

        private Tunnel[] remoteTunnels;

        private string terminalCopy;

        public DevolutionsTerminalControl()
        {
            this.signalDelegate = this.OnSignal;
            this.logDelegate = this.OnLog;
            this.needStringDelegate = this.OnNeedString;
            this.releaseStringDelegate = this.OnReleaseString;
            this.needValueDelegate = this.OnNeedValue;
            this.setErrorDelegate = this.OnError;
            this.sshServerAuthenticationDelegate = this.OnSshServerAuthentication;
            this.sshIsServerKnownDelegate = this.OnSshIsServerKnown;
            this.currentPlayTimeDelegate = this.OnCurrentPlayTime;

            this.jumpSignalDelegate = this.OnJumpSignal;
            this.jumpNeedStringDelegate = this.OnJumpNeedString;
            this.jumpNeedValueDelegate = this.OnJumpNeedValue;
            this.jumpSshIsServerKnownDelegate = this.OnJumpSshIsServerKnown;

            this.callbacks = new DevolutionsTerminalNativeAPI.Callbacks();
            this.callbacks.Signal = Marshal.GetFunctionPointerForDelegate(this.signalDelegate);
            this.callbacks.Log = Marshal.GetFunctionPointerForDelegate(this.logDelegate);
            this.callbacks.NeedString = Marshal.GetFunctionPointerForDelegate(this.needStringDelegate);
            this.callbacks.NeedValue = Marshal.GetFunctionPointerForDelegate(this.needValueDelegate);
            this.callbacks.ReleaseString = Marshal.GetFunctionPointerForDelegate(this.releaseStringDelegate);
            this.callbacks.SetError = Marshal.GetFunctionPointerForDelegate(this.setErrorDelegate);
            this.callbacks.SshServerAuthentication = Marshal.GetFunctionPointerForDelegate(this.sshServerAuthenticationDelegate);
            this.callbacks.SshIsServerKnown = Marshal.GetFunctionPointerForDelegate(this.sshIsServerKnownDelegate);
            this.callbacks.CurrentPlayTime = Marshal.GetFunctionPointerForDelegate(this.currentPlayTimeDelegate);

            this.jumpCallbacks = new DevolutionsTerminalNativeAPI.Callbacks();
            this.jumpCallbacks.Signal = Marshal.GetFunctionPointerForDelegate(this.jumpSignalDelegate);
            this.jumpCallbacks.Log = Marshal.GetFunctionPointerForDelegate(this.logDelegate);
            this.jumpCallbacks.NeedString = Marshal.GetFunctionPointerForDelegate(this.jumpNeedStringDelegate);
            this.jumpCallbacks.NeedValue = Marshal.GetFunctionPointerForDelegate(this.jumpNeedValueDelegate);
            this.jumpCallbacks.ReleaseString = Marshal.GetFunctionPointerForDelegate(this.releaseStringDelegate);
            this.jumpCallbacks.SetError = Marshal.GetFunctionPointerForDelegate(this.setErrorDelegate);
            this.jumpCallbacks.SshServerAuthentication = Marshal.GetFunctionPointerForDelegate(this.sshServerAuthenticationDelegate);
            this.jumpCallbacks.SshIsServerKnown = Marshal.GetFunctionPointerForDelegate(this.jumpSshIsServerKnownDelegate);
            this.jumpCallbacks.CurrentPlayTime = Marshal.GetFunctionPointerForDelegate(this.currentPlayTimeDelegate);

            this.sequenceQueue = new Queue();
            this.commandQueue = new ConcurrentQueue<Command>();
            this.commandTimer = new System.Timers.Timer();
            this.commandTimer.Elapsed += this.commandTimer_Elapsed;

            this.State = ConnectionState.Closed;
            this.Protocol = NativeSshProtocol.Ssh;
            this.TerminalType = "xterm";
            this.AutoWrapMode = true;
            this.LocalEcho = TriState.Auto;
            this.ScrollbackLines = 2000;
            this.ResetScrollOnDisplay = true;
            this.TryAgent = true;
            this.ProxyPort = 80;
            this.ProxyConnectCommand = "connect %host %port\\n";
            this.X11AuthType = X11AuthType.Mit;
            this.SerialDataBits = 8;
            this.SerialSpeed = 9600;
            this.SerialStopHalfbits = 2;
            this.syntaxColoringItems = new ArrayList();
            this.TerminalFontName = "Courier New";
            this.TerminalFontHeight = 12;
            this.TerminalColors = new[]
                {
                    Color.FromArgb(187, 187, 187),
                    Color.FromArgb(255, 255, 255),
                    Color.FromArgb(0, 0, 0),
                    Color.FromArgb(85, 85, 85),
                    Color.FromArgb(0, 0, 0),
                    Color.FromArgb(0, 255, 0),
                    Color.FromArgb(0, 0, 0),
                    Color.FromArgb(85, 85, 85),
                    Color.FromArgb(187, 0, 0),
                    Color.FromArgb(255, 85, 85),
                    Color.FromArgb(0, 187, 0),
                    Color.FromArgb(85, 255, 85),
                    Color.FromArgb(187, 187, 0),
                    Color.FromArgb(255, 255, 85),
                    Color.FromArgb(0, 0, 187),
                    Color.FromArgb(85, 85, 255),
                    Color.FromArgb(187, 0, 187),
                    Color.FromArgb(255, 85, 255),
                    Color.FromArgb(0, 187, 187),
                    Color.FromArgb(85, 255, 255),
                    Color.FromArgb(187, 187, 187),
                    Color.FromArgb(255, 255, 255)
                };

            this.GssapiAuthentication = true;
            this.RemoteCommand = string.Empty;
            this.focusState = this.Focused;
            this.TrySmartCardCertificateAuthentication = false;
            this.Answerback = "PuTTY";
        }

        public event EventHandler Activity
        {
            add { }
            remove { }
        }

        public event EventHandler Beep;

        public event EventHandler Connected;

        // Consumer of this event must return as fast as possible, eg: invoke assynchronously on main thread to update any visuals.
        public event SessionTimeEventHandler CurrentPlayTime;

        public event EventHandler Disconnected;

        public event LogEventHandler Log;

        public event LogOverwriteEventHandler LogOverwrite;

        public event EventHandler NativeClosed;

        public event NeedSecretStringHandler NeedJumpHostKeyPassword;

        public event NeedSecretStringHandler NeedJumpHostPassword;

        public event NeedStringHandler NeedJumpHostUser;

        public event NeedSecretStringHandler NeedKeyPassword;

        public event NeedSecretStringHandler NeedPassword;

        public event NeedSecretStringHandler NeedProxyPassword;

        public event NeedStringHandler NeedUser;

        public event TextChangeHandler IconName
        {
            add { }
            remove { }
        }

        public event TextChangeHandler WindowTitle
        {
            add { }
            remove { }
        }

        public event MultilineWarningHandler MultilineWarning
        {
            add { }
            remove { }
        }

        public event SshMonitoringHandler SshMonitoring
        {
            add { }
            remove { }
        }

        // Consumer of this event must schedule to read recorded data with PlayerRead() and return as fast as possible.
        public event EventHandler NewRecordData;

#pragma warning disable 67

        // This event is implemented to fulfill the interface, but is not used right now.
        public event PlaySizeChangeHandler PlaySizeChanged;

#pragma warning restore 67

        // Consumer of this event should invoke assynchronously on main thread to update any visuals.
        public event EventHandler PlayStopped;

        // Consumer of this event should assynchronously close recorded data.
        public event SessionTimeEventHandler RecordEndTime;

        public event EventHandler SequenceComplete;

#pragma warning disable 67

        // Theese events are implemented to fulfill the interface, but are not used right now.
        public event SshInteractiveHandler SshInteractive;

        public event SshIsServerKnownHandler SshIsServerKnown;

        public event SizeChangeHandler SizeChange;

#pragma warning restore 67

        public event SshServerAuthenticationHandler SshServerAuthentication;

        public Size ActualTerminalSize
        {
            get
            {
                if (this.context == IntPtr.Zero)
                {
                    return new Size();
                }

                long result = DevolutionsTerminalNativeAPI.ActualTerminalSize(this.context);
                Size size = new Size((int)(result >> 32), (int)(result & 0xFFFFFFFF));
                return size;
            }
        }

        public AddressFamily AddressFamily { get; set; }

        public bool AgentFwd { get; set; }

        public string Answerback { get; set; }

        public bool ApplicationCursorKeys { get; set; }

        public bool ApplicationKeypad { get; set; }

        public bool AutoWrapMode { get; set; }

        public bool BackspaceIsDelete { get; set; }

        public bool CRImpliesLF { get; set; }

        public Color CursorBackground
        {
            get => this.TerminalColors[5];

            set => this.TerminalColors[5] = value;
        }

        public bool CursorBlink { get; set; }

        public Color CursorForeground
        {
            get => this.TerminalColors[4];

            set => this.TerminalColors[4] = value;
        }

        public CursorType CursorType { get; set; }

        public Color DefaultBackground
        {
            get => this.TerminalColors[2];

            set => this.TerminalColors[2] = value;
        }

        public Color DefaultForeground
        {
            get => this.TerminalColors[0];

            set => this.TerminalColors[0] = value;
        }

        public bool DisableControlWheel { get; set; }

        public CompletionCode DisconnectionReason { get; private set; }

        public string DoubleClickDelimiters { get; set; }

        public Tunnel[] DynamicTunnels
        {
            get => this.dynamicTunnels;

            set
            {
                if (value == null)
                {
                    this.dynamicTunnels = null;
                }
                else
                {
                    this.dynamicTunnels = new Tunnel[value.Length];
                    Array.Copy(value, this.dynamicTunnels, value.Length);
                }
            }
        }

        public string ExpectedPasswordPrompt { get; set; }

        public string ExpectedUserPrompt { get; set; }

        public bool GssapiAuthentication { get; set; }

        public bool GssapiDelegation { get; set; }

        public string Host { get; set; }

        public bool IgnoreSubstituteCharacter { get; set; }

        public bool InteractiveAuthOnTerminal { get; set; }

        public NetworkFamilyHint InternetProtocol { get; set; }

        public bool IsRecording { get; set; }

        public string JumpHostName { get; set; }

        public SecretString JumpHostPassword { get; set; }

        public int JumpHostPort { get; set; }

        public string JumpHostPublicKeyData { get; set; }

        public string JumpHostPublicKeyFilePath { get; set; }

        public SecretString JumpHostPublicKeyPassword { get; set; }

        public string JumpHostUser { get; set; }

        public SshJump[] Jumps { get; set; }

        public bool LFImpliesCR { get; set; }

        public string LineCodePage { get; set; }

        public KeyType LinuxFunctionKeys { get; set; }

        public TriState LocalEcho { get; set; }

        public Tunnel[] LocalTunnels
        {
            get => this.localTunnels;

            set
            {
                if (value == null)
                {
                    this.localTunnels = null;
                }
                else
                {
                    this.localTunnels = new Tunnel[value.Length];
                    Array.Copy(value, this.localTunnels, value.Length);
                }
            }
        }

        public LogContentType LogContentType { get; set; }

        public string LogFileName { get; set; }

        public bool LogToTerminal { get; set; }

        public int MinimumDiffieHellmanSize { get; set; }

        public MouseType MouseIsXterm { get; set; }

        public bool NoApplicationKeys { get; set; }

        public bool NoDBackspace { get; set; }

        public SecretString Password { get; set; }

        public int PasteDelay { get; set; }

        public SecretString Pin { get; set; }

        public int PingInterval { get; set; }

        public string PingText { get; set; }

        public string PlaybackPath { get; set; }

        public int PlayerMaxIdle { get; set; }

        public long PlayerTotalTime { get; }

        public int Port { get; set; }

        public bool PreserveFormatingOnCopy { get; set; }

        public NativeSshProtocol Protocol { get; set; }

        public string ProxyConnectCommand { get; set; } // ProxyTelnetCommand

        [Obsolete("Use ProxyResolveHint instead (TriState.Auto = ProxyResolveHint.FORCE_REMOTE, TriState.ForceOn = ProxyResolveHint.FORCE_REMOTE, TriState.ForceOff = ProxyResolveHint.FORCE_LOCAL)", false)]
        public int ProxyDnsLookup
        {
            get
            {
                switch (this.ProxyResolveHint)
                {
                    case ProxyResolveHint.PROXY_RESOLVE_FORCE_LOCAL:
                        return (int)TriState.ForceOff;

                    default:
                        return (int)TriState.ForceOn;
                }
            }

            set
            {
                switch (value)
                {
                    case (int)TriState.ForceOff:
                        this.ProxyResolveHint = ProxyResolveHint.PROXY_RESOLVE_FORCE_LOCAL;
                        break;

                    default:
                        this.ProxyResolveHint = ProxyResolveHint.PROXY_RESOLVE_FORCE_REMOTE;
                        break;
                }
            }
        }

        public ProxyResolveHint ProxyResolveHint { get; set; }

        public string ProxyExclusion { get; set; } // ProxyExcludeList

        public string ProxyHost { get; set; }

        public bool ProxyIncludeLocal { get; set; } // ProxyLocalHost

        public SecretString ProxyPassword { get; set; }

        public int ProxyPort { get; set; }

        public int ProxyType { get; set; } // ProxyMethod

        public string ProxyUser { get; set; } // ProxyUserName

        public string PublicKeyData { get; set; }

        public string PublicKeyFilePath { get; set; }

        public SecretString PublicKeyPassword { get; set; }

        public string RecordPath { get; set; }

        public long RecordTime { get; private set; }

        public string RemoteCommand { get; set; }

        public bool RemoteMonitoring { get; set; }

        public int RemoteMonitoringInterval { get; set; }

        public Tunnel[] RemoteTunnels
        {
            get => this.remoteTunnels;

            set
            {
                if (value == null)
                {
                    this.remoteTunnels = null;
                }
                else
                {
                    this.remoteTunnels = new Tunnel[value.Length];
                    Array.Copy(value, this.remoteTunnels, value.Length);
                }
            }
        }

        public bool ResetScrollOnDisplay { get; set; }

        public bool RXVTHomeEnd { get; set; }

        public int ScrollbackLines { get; set; }

        public bool SendScrollWheel { get; set; }

        public CompletionCode SequenceResult { get; private set; }

        public int SerialDataBits { get; set; }

        public SerialFlowControl SerialFlowControl { get; set; }

        public string SerialLine { get; set; }

        public SerialParity SerialParity { get; set; }

        public int SerialSpeed { get; set; }

        public int SerialStopHalfbits { get; set; }

        public bool skipEnvironmentVariableSetup { get; set; }

        public string SmartCardCertificateId { get; set; }

        public string SmartCardCertificatePublicKey { get; set; }

        public string SmartCardCertificateThumbprint { get; set; }

        public bool SshNoShell { get; set; }

        public ConnectionState State { get; private set; }

        public bool TcpKeepAlive { get; set; }

        public Color[] TerminalColors { get; private set; }

        public TerminalError TerminalError { get; private set; }

        public int TerminalFontHeight { get; set; }

        public bool TerminalFontIsBold { get; set; }

        public string TerminalFontName { get; set; }

        public FontQuality TerminalFontQuality { get; set; }

        public string TerminalType { get; set; }

        public bool TryAgent { get; set; }

        public bool TrySmartCardCertificateAuthentication { get; set; }

        public string User { get; set; }

        public bool Verbose { get; set; }

        public int VerboseLevel { get; set; } // TODO

        public string X11AuthFile { get; set; }

        public X11AuthType X11AuthType { get; set; }

        public string X11Display { get; set; }

        public bool X11Forward { get; set; }

        public bool SkipEnvironmentVariableSetup { get; set; } // Stub must be executable for inter-operability

        public static bool IsRegexGood(string regex, out SyntaxColoring.RegexError error, out int index)
        {
            IntPtr utf8Regex = NativeUtf8FromString(regex);
            if (utf8Regex == IntPtr.Zero)
            {
                throw new OutOfMemoryException();
            }

            int nativeError;
            int result = DevolutionsTerminalNativeAPI.SyntaxColoringIsRegexGood(utf8Regex, out nativeError, out index);
            error = (SyntaxColoring.RegexError)nativeError;
            Marshal.FreeHGlobal(utf8Regex);
            return result != 0;
        }

        public void ClearScrollBack()
        {
            if (this.context != IntPtr.Zero)
            {
                DevolutionsTerminalNativeAPI.ClearScrollBack(this.context);
            }
        }

        public void ClearSmartCardCertificateInformation()
        {
            this.SmartCardCertificateId = string.Empty;
            this.SmartCardCertificatePublicKey = string.Empty;
            this.SmartCardCertificateThumbprint = string.Empty;
        }

        public void CloseNativeContext()
        {
            if (this.context != IntPtr.Zero)
            {
                DevolutionsTerminalNativeAPI.EndThread(this.context);
            }

            if (this.jumpContext != IntPtr.Zero)
            {
                DevolutionsTerminalNativeAPI.EndThread(this.jumpContext);
            }
        }

        public bool Connect()
        {
            if (this.Jumps != null && this.Jumps[0] != null)
            {
                this.JumpHostName = this.Jumps[0].Host;
                this.JumpHostPublicKeyPassword = this.Jumps[0].KeyFilePassword;
                this.JumpHostPublicKeyData = this.Jumps[0].KeyData;
                this.JumpHostPublicKeyFilePath = this.Jumps[0].KeyFilePath;
                this.JumpHostPassword = this.Jumps[0].Password;
                this.JumpHostPort = this.Jumps[0].Port;
                this.JumpHostUser = this.Jumps[0].User;
            }

            this.RecordTime = 0;
            if (this.State != ConnectionState.Disconnected && this.State != ConnectionState.Closed)
            {
                return false;
            }

            if (this.context != IntPtr.Zero)
            {
                return false;
            }

            this.TerminalError = TerminalError.None;

            IntPtr callbacksBuffer = Marshal.AllocHGlobal(Marshal.SizeOf(this.callbacks));
            Marshal.StructureToPtr(this.callbacks, callbacksBuffer, false);
            this.hwnd = this.Handle;
            this.context = DevolutionsTerminalNativeAPI.CreateContext(this.hwnd, callbacksBuffer);
            Marshal.FreeHGlobal(callbacksBuffer);

            if (string.IsNullOrEmpty(this.JumpHostName))
            {
                if (this.context != IntPtr.Zero)
                {
                    this.ApplySyntaxColoring();
                    short result = DevolutionsTerminalNativeAPI.Connect(this.context);
                    if (result == 0)
                    {
                        this.contextIsActive = true;
                        this.State = ConnectionState.Connecting;
                    }
                }
            }
            else
            {
                if (this.jumpContext != IntPtr.Zero)
                {
                    return false;
                }

                this.TerminalError = TerminalError.None;

                callbacksBuffer = Marshal.AllocHGlobal(Marshal.SizeOf(this.jumpCallbacks));
                Marshal.StructureToPtr(this.jumpCallbacks, callbacksBuffer, false);
                this.jumpContext = DevolutionsTerminalNativeAPI.CreateContext(IntPtr.Zero, callbacksBuffer);
                Marshal.FreeHGlobal(callbacksBuffer);

                if (this.jumpContext != IntPtr.Zero)
                {
                    short result = DevolutionsTerminalNativeAPI.Connect(this.jumpContext);
                    if (result == 0)
                    {
                        this.jumpContextIsActive = true;
                        this.State = ConnectionState.Connecting;
                    }
                }
            }

            return true;
        }

        public string CopyTerminalOutput()
        {
            if (this.context == IntPtr.Zero)
            {
                if (this.terminalCopy != null)
                {
                    return this.terminalCopy;
                }

                return string.Empty;
            }

            IntPtr rawOutput = DevolutionsTerminalNativeAPI.CopyOutput(this.context);
            this.terminalCopy = Marshal.PtrToStringUni(rawOutput);
            return this.terminalCopy;
        }

        public void Disconnect()
        {
            if (this.context != IntPtr.Zero && this.contextIsActive)
            {
                DevolutionsTerminalNativeAPI.Disconnect(this.context);
            }

            if (this.jumpContext != IntPtr.Zero && this.jumpContextIsActive)
            {
                DevolutionsTerminalNativeAPI.Disconnect(this.jumpContext);
            }
        }

        public void DisplaySmartCardCertificate()
        {
            if (this.SmartCardCertificateId != null && !this.SmartCardCertificateId.Equals(string.Empty))
            {
                DevolutionsTerminalNativeAPI.SmartCardDisplayCert(this.SmartCardCertificateId, this.Handle);
            }
        }

        public void JoinGroup(int group)
        {
        }

        public void LeaveGroup()
        {
        }

        public void LocalizeContextMenu(GetLocalizedStringDelegate getLocalized)
        {
            // No localization for Putty (the contextual menu is generated in native).
        }

        public long PlayerGetDuration()
        {
            if (this.context != IntPtr.Zero)
            {
                return DevolutionsTerminalNativeAPI.PlayerGetDuration(this.context);
            }

            return 0;
        }

        public void PlayerMoveTo(long time)
        {
            if (this.context != IntPtr.Zero)
            {
                DevolutionsTerminalNativeAPI.PlayerMoveTo(this.context, time);
            }
        }

        public byte[] PlayerRead()
        {
            byte[] bytes = null;
            IntPtr p;
            int size;
            this.RecordTime = DevolutionsTerminalNativeAPI.TerminalLockBuffer(this.context, out p, out size);
            if (p != IntPtr.Zero && size > 0)
            {
                bytes = new byte[size];
                Marshal.Copy(p, bytes, 0, size);
            }

            DevolutionsTerminalNativeAPI.TerminalUnlockBuffer(this.context);
            return bytes;
        }

        public void PlayerReset()
        {
            if (this.context != IntPtr.Zero)
            {
                DevolutionsTerminalNativeAPI.PlayerReset(this.context);
            }
        }

        public void PlayerStart()
        {
            if (this.context != IntPtr.Zero)
            {
                DevolutionsTerminalNativeAPI.PlayerStart(this.context);
            }
        }

        public void PlayerStop()
        {
            if (this.context != IntPtr.Zero)
            {
                DevolutionsTerminalNativeAPI.PlayerStop(this.context);
            }
        }

        public void PlayerWrite(byte[] data)
        {
            IntPtr p = DevolutionsTerminalNativeAPI.PlayerLockBuffer(this.context, data.Length);
            if (p == IntPtr.Zero)
            {
                return;
            }

            Marshal.Copy(data, 0, p, data.Length);
            DevolutionsTerminalNativeAPI.PlayerUnlockBuffer(this.context);
        }

        public void Reconfig()
        {
            if (this.context != IntPtr.Zero)
            {
                DevolutionsTerminalNativeAPI.Reconfig(this.context);
            }
        }

        public byte[] RecordGet()
        {
            return null;
        }

        public void RecordStart(string recordPath)
        {
        }

        public void RecordStop()
        {
        }

        public void ResetTerminal()
        {
            if (this.context != IntPtr.Zero)
            {
                DevolutionsTerminalNativeAPI.ResetTerminal(this.context);
            }
        }

        public TextSearchResponse Search(string text, TextSearchDirection direction, TextSearchType searchType, bool isCaseSensitive)
        {
            return TextSearchResponse.NO_MATCH;
        }

        public void SearchClear()
        {
        }

        public void Send(string commandString, int delay = 0)
        {
            if (this.context != IntPtr.Zero)
            {
                if (delay == 0 && this.commandQueue.IsEmpty)
                {
                    IntPtr nativeString = NativeUtf8FromString(commandString);
                    DevolutionsTerminalNativeAPI.Send(this.context, nativeString);

                    // The native code will release the native string.
                }
                else
                {
                    Command command = new Command();
                    command.String = commandString;
                    command.MsDelay = delay;
                    this.commandQueue.Enqueue(command);

                    if (this.commandTimer.Enabled == false)
                    {
                        this.commandTimer.Interval = delay;
                        this.commandTimer.Start();
                    }
                }
            }
        }

        public void Send(CommandSequence sequence)
        {
            this.sequenceQueue.Enqueue(sequence);
            if (this.sequenceQueue.Count == 1)
            {
                IntPtr utf8Sequence = sequence.GetSequence();
                DevolutionsTerminalNativeAPI.SendSequence(this.context, utf8Sequence);
                Marshal.FreeHGlobal(utf8Sequence);
            }
        }

        public void SendBreak()
        {
        }

        public void SendSignal(Signal signal)
        {
        }

        public void SetSmartCardCertificate(SetSmartCardCertificateMethod certificateType)
        {
            this.SetSmartCardCertificateId(certificateType);
            this.SetSmartCardCertificatePublicKey();
            this.SetSmartCardCertificateThumbprint();
        }

        public void SyntaxColoringAddItem(SyntaxColoring.Item item)
        {
            foreach (SyntaxColoring.Item existingItem in this.syntaxColoringItems)
            {
                if (string.Compare(item.Keyword, existingItem.Keyword, StringComparison.Ordinal) == 0)
                {
                    // An item with the same keyword already is contained in the syntax coloring item list:
                    return;
                }
            }

            if (this.context != IntPtr.Zero)
            {
                IntPtr utf8Keyword = NativeUtf8FromString(item.Keyword);
                if (utf8Keyword == IntPtr.Zero)
                {
                    throw new OutOfMemoryException();
                }

                DevolutionsTerminalNativeAPI.SyntaxColoringAdd(
                    this.context,
                    utf8Keyword,
                    item.ForegroundColor,
                    item.BackgroundColor,
                    (byte)(item.IsCompleteWord ? 1 : 0),
                    (byte)(item.IsCaseSensitive ? 1 : 0),
                    (byte)(item.IsUnderlined ? 1 : 0),
                    (byte)(item.IsRegex ? 1 : 0));
                Marshal.FreeHGlobal(utf8Keyword);
            }

            item.IsEnabled = true;
            this.syntaxColoringItems.Add(item);
        }

        public void SyntaxColoringApplyChanges()
        {
            if (this.context != IntPtr.Zero)
            {
                DevolutionsTerminalNativeAPI.SyntaxColoringApplyChanges(this.context);
            }
        }

        public void SyntaxColoringDeleteAll()
        {
            throw new NotImplementedException();
        }

        public void SyntaxColoringDeleteItem(SyntaxColoring.Item item)
        {
            throw new NotImplementedException();
        }

        public void SyntaxColoringDisableAll()
        {
            throw new NotImplementedException();
        }

        public void SyntaxColoringDisableItem(SyntaxColoring.Item item)
        {
            throw new NotImplementedException();
        }

        public void SyntaxColoringEnableAll()
        {
            throw new NotImplementedException();
        }

        public void SyntaxColoringEnableItem(SyntaxColoring.Item item)
        {
            throw new NotImplementedException();
        }

        public SyntaxColoring.Item[] SyntaxColoringGetAllItems()
        {
            return (SyntaxColoring.Item[])this.syntaxColoringItems.ToArray();
        }

        protected override void OnClientSizeChanged(EventArgs e)
        {
            base.OnClientSizeChanged(e);
            if (this.context != IntPtr.Zero)
            {
                DevolutionsTerminalNativeAPI.Resize(this.context, this.ClientRectangle.Width, this.ClientRectangle.Height);
            }
        }

        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            this.focusState = true;
            if (this.context != IntPtr.Zero)
            {
                DevolutionsTerminalNativeAPI.SetFocus(this.context);
            }
        }

        protected override void OnHandleDestroyed(EventArgs e)
        {
            if (this.context != IntPtr.Zero)
            {
                DevolutionsTerminalNativeAPI.EndThread(this.context);
                IntPtr p = this.context;
                ThreadPool.QueueUserWorkItem(o => DevolutionsTerminalNativeAPI.Release(p));
            }

            if (this.jumpContext != IntPtr.Zero)
            {
                DevolutionsTerminalNativeAPI.EndThread(this.jumpContext);
                IntPtr p = this.jumpContext;
                ThreadPool.QueueUserWorkItem(o => DevolutionsTerminalNativeAPI.Release(p));
            }

            base.OnHandleDestroyed(e);
        }

        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            this.focusState = false;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.FillRectangle(Brushes.Black, this.ClientRectangle);
        }

        private static IntPtr NativeUtf8FromString(string managedString)
        {
            if (managedString == null)
            {
                return IntPtr.Zero;
            }

            int len = Encoding.UTF8.GetByteCount(managedString);
            byte[] buffer = new byte[len + 1];
            Encoding.UTF8.GetBytes(managedString, 0, managedString.Length, buffer, 0);
            IntPtr nativeUtf8 = Marshal.AllocHGlobal(buffer.Length);
            Marshal.Copy(buffer, 0, nativeUtf8, buffer.Length);
            return nativeUtf8;
        }

        private void ApplySyntaxColoring()
        {
            foreach (SyntaxColoring.Item item in this.syntaxColoringItems)
            {
                if (item.IsEnabled)
                {
                    IntPtr utf8Keyword = NativeUtf8FromString(item.Keyword);
                    if (utf8Keyword == IntPtr.Zero)
                    {
                        throw new OutOfMemoryException();
                    }

                    DevolutionsTerminalNativeAPI.SyntaxColoringAdd(
                        this.context,
                        utf8Keyword,
                        item.ForegroundColor,
                        item.BackgroundColor,
                        (byte)(item.IsCompleteWord ? 1 : 0),
                        (byte)(item.IsCaseSensitive ? 1 : 0),
                        (byte)(item.IsUnderlined ? 1 : 0),
                        (byte)(item.IsRegex ? 1 : 0));
                    Marshal.FreeHGlobal(utf8Keyword);
                }
            }
        }

        private void commandTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.BeginInvoke((Action)this.ProcessTimer);
        }

        private string MakeTunnelString()
        {
            string tunnelString = string.Empty;
            if (this.localTunnels != null)
            {
                foreach (Tunnel tunnel in this.localTunnels)
                {
                    tunnelString += "L";
                    tunnelString += string.IsNullOrEmpty(tunnel.SourceHost) ? "localhost" : tunnel.SourceHost;
                    tunnelString += ":" + tunnel.SourcePort.ToString() + "\t";
                    tunnelString += string.IsNullOrEmpty(tunnel.DestinationHost) ? "localhost" : tunnel.DestinationHost;
                    tunnelString += ":" + tunnel.DestinationPort.ToString() + "\n";
                }
            }

            if (this.remoteTunnels != null)
            {
                foreach (Tunnel tunnel in this.remoteTunnels)
                {
                    tunnelString += "R";
                    tunnelString += string.IsNullOrEmpty(tunnel.SourceHost) ? "localhost" : tunnel.SourceHost;
                    tunnelString += ":" + tunnel.SourcePort.ToString() + "\t";
                    tunnelString += string.IsNullOrEmpty(tunnel.DestinationHost) ? "localhost" : tunnel.DestinationHost;
                    tunnelString += ":" + tunnel.DestinationPort.ToString() + "\n";
                }
            }

            if (this.dynamicTunnels != null)
            {
                foreach (Tunnel tunnel in this.dynamicTunnels)
                {
                    tunnelString += "L";
                    tunnelString += string.IsNullOrEmpty(tunnel.SourceHost) ? "localhost" : tunnel.SourceHost;
                    tunnelString += ":" + tunnel.SourcePort.ToString() + "\tD\n";
                }
            }

            return tunnelString;
        }

        private void OnCurrentPlayTime(long time)
        {
            if (time < 0)
            {
                if (this.Protocol == NativeSshProtocol.Player)
                {
                    if (this.PlayStopped != null)
                    {
                        this.PlayStopped(this, null);
                    }
                }
                else
                {
                    if (this.RecordEndTime != null)
                    {
                        this.RecordEndTime(this, time);
                    }
                }
            }
            else
            {
                if (this.CurrentPlayTime != null)
                {
                    this.CurrentPlayTime(this, time);
                }
            }
        }

        private void OnError(ushort error)
        {
            this.TerminalError = (TerminalError)error;
            switch (this.TerminalError)
            {
                case TerminalError.None:
                    this.DisconnectionReason = CompletionCode.SUCCESS;
                    break;

                case TerminalError.Fatal:
                    this.DisconnectionReason = CompletionCode.FAIL;
                    break;

                case TerminalError.OutOfMemory:
                    this.DisconnectionReason = CompletionCode.FAIL_OUT_OF_MEMORY;
                    break;

                case TerminalError.Socket:
                    this.DisconnectionReason = CompletionCode.FAIL_NOT_CONNECTED;
                    break;

                case TerminalError.SocketLibrary:
                    this.DisconnectionReason = CompletionCode.FAIL_NETWORK_DOWN;
                    break;

                case TerminalError.Ssh:
                    this.DisconnectionReason = CompletionCode.FAIL_PROTOCOL_ERROR;
                    break;

                case TerminalError.Telnet:
                    this.DisconnectionReason = CompletionCode.FAIL_PROTOCOL_ERROR;
                    break;

                case TerminalError.Serial:
                    this.DisconnectionReason = CompletionCode.FAIL_PROTOCOL_ERROR;
                    break;

                default:
                    this.DisconnectionReason = CompletionCode.FAIL;
                    break;
            }
        }

        private IntPtr OnJumpNeedString(ushort stringId)
        {
            try
            {
                switch ((StringId)stringId)
                {
                    case StringId.HostName:
                    case StringId.RealHostName:
                        return NativeUtf8FromString(this.JumpHostName);

                    case StringId.UserName:
                        {
                            string user = this.JumpHostUser;
                            if (user == null)
                            {
                                user = this.jumpCachedUser;
                            }

                            if (user == null)
                            {
                                if (this.NeedJumpHostUser != null)
                                {
                                    user = this.NeedJumpHostUser(this);
                                }
                            }

                            if (user == null)
                            {
                                return IntPtr.Zero;
                            }

                            this.jumpCachedUser = user;
                            return NativeUtf8FromString(user);
                        }

                    case StringId.Password:
                        {
                            SecretString password = this.JumpHostPassword;
                            if (password == null)
                            {
                                password = this.cachedJumpHostPassword;
                            }

                            if (password == null)
                            {
                                if (this.NeedJumpHostPassword != null)
                                {
                                    password = this.NeedJumpHostPassword(this);
                                    this.cachedJumpHostPassword = password;
                                }
                            }

                            if (password == null)
                            {
                                return IntPtr.Zero;
                            }

                            return password.Get();
                        }

                    case StringId.ProxyPassword:
                        {
                            return IntPtr.Zero;
                        }

                    case StringId.KeyPassword:
                        {
                            SecretString password = this.JumpHostPublicKeyPassword;
                            if (password == null)
                            {
                                password = this.cachedJumpHostPublicKeyPassword;
                            }

                            if (password == null)
                            {
                                if (this.NeedJumpHostKeyPassword != null)
                                {
                                    password = this.NeedJumpHostKeyPassword(this);
                                    this.cachedJumpHostPublicKeyPassword = password;
                                }
                            }

                            if (password == null)
                            {
                                return IntPtr.Zero;
                            }

                            return password.Get();
                        }

                    case StringId.PublicKeyFilePath:
                        return NativeUtf8FromString(this.JumpHostPublicKeyFilePath);

                    case StringId.Tunnels:
                        string tunnelString = "4Llocalhost:0\t" + this.Host + ":" + this.Port + "\n";
                        return NativeUtf8FromString(tunnelString);

                    case StringId.KeyData:
                        return NativeUtf8FromString(this.JumpHostPublicKeyData);
                }
            }
            catch (Exception)
            {
                // Just avoid the exception to crash the native code.
            }

            return this.OnNeedString(stringId);
        }

        private long OnJumpNeedValue(ushort valueId)
        {
            switch ((ValueId)valueId)
            {
                case ValueId.HasFocus:
                    return 0;

                case ValueId.PortNumber:
                case ValueId.RealPortNumber:
                    return this.JumpHostPort;

                case ValueId.AddressFamily:
                    return (long)AddressFamily.Ipv4;

                case ValueId.SshNoShell:
                    return 1;

                case ValueId.ProxyMethod:
                    return (long)Devolutions.Protocols.ProxyType.None;

                case ValueId.X11Forward:
                    return 0;
            }

            return this.OnNeedValue(valueId);
        }

        private void OnJumpSignal(int signal)
        {
            switch ((SignalId)signal)
            {
                case SignalId.Connected:
                    try
                    {
                        this.jumpPort = DevolutionsTerminalNativeAPI.GetTunnelPort(this.jumpContext);

                        if (this.context != IntPtr.Zero)
                        {
                            this.ApplySyntaxColoring();
                            short result = DevolutionsTerminalNativeAPI.Connect(this.context);
                            if (result == 0)
                            {
                                this.contextIsActive = true;
                            }
                        }

                        if (this.Log != null)
                        {
                            this.Log(this, LogType.LOG_TYPE_MESSAGE, "Connected to proxy successfuly");
                        }
                    }
                    catch (Exception)
                    {
                        // Just catch the exception to avoid a crash.
                    }

                    break;

                case SignalId.Disconnected:
                    this.BeginInvoke(
                        (Action)(() =>
                            {
                                this.jumpContextIsActive = false;
                                if (this.context != IntPtr.Zero && this.contextIsActive)
                                {
                                    DevolutionsTerminalNativeAPI.Disconnect(this.context);
                                }
                                else
                                {
                                    this.BeginInvoke((Action)(() => this.ReleaseContext(this)));
                                }
                            }));
                    break;

                case SignalId.Beep:
                    // There is nothing to do (and it should not even get called, but for security...)
                    break;

                case SignalId.Denied:
                    // Nothing to do for now.
                    break;

                case SignalId.NewRecordData:
                    // There is nothing to do (and it should not even get called, but for security...)
                    break;

                case SignalId.Closed:
                    this.jumpContext = IntPtr.Zero;
                    this.State = ConnectionState.Closed;
                    if (this.context == IntPtr.Zero && this.NativeClosed != null)
                    {
                        this.NativeClosed(this, null);
                    }

                    break;
            }
        }

        private ushort OnJumpSshIsServerKnown(IntPtr nativeKey)
        {
            if (this.SshIsServerKnown != null)
            {
                try
                {
                    string key = Marshal.PtrToStringAnsi(nativeKey);
                    return (ushort)this.SshIsServerKnown(this, this.JumpHostName, this.JumpHostPort, key);
                }
                catch (Exception)
                {
                    // Just catch the exception to avoid a crash.
                }
            }

            return 0;
        }

        private void OnLog(ushort logType, IntPtr nativeText)
        {
            if (this.Log != null)
            {
                try
                {
                    string message = Marshal.PtrToStringAnsi(nativeText);
                    this.BeginInvoke((MethodInvoker)delegate { this.Log(this, (LogType)logType, message); });
                }
                catch (Exception)
                {
                    // Just catch the exception to avoid a crash.
                }
            }
        }

        private IntPtr OnNeedString(ushort stringId)
        {
            try
            {
                switch ((StringId)stringId)
                {
                    case StringId.HostName:
                        if (this.jumpContext == IntPtr.Zero)
                        {
                            return NativeUtf8FromString(this.Host);
                        }
                        else
                        {
                            return NativeUtf8FromString("localhost");
                        }

                    case StringId.UserName:
                        {
                            string user = this.User;
                            if (user == null)
                            {
                                user = this.cachedUser;
                            }

                            if (user == null)
                            {
                                if (this.InteractiveAuthOnTerminal)
                                {
                                    return IntPtr.Zero;
                                }

                                if (this.NeedUser != null)
                                {
                                    user = this.NeedUser(this);
                                }
                            }

                            if (user == null)
                            {
                                return IntPtr.Zero;
                            }

                            this.cachedUser = user;
                            return NativeUtf8FromString(user);
                        }

                    case StringId.Password:
                        {
                            SecretString password = this.Password;
                            if (password == null)
                            {
                                password = this.cachedPassword;
                            }

                            if (password == null)
                            {
                                if (this.InteractiveAuthOnTerminal)
                                {
                                    return IntPtr.Zero;
                                }

                                if (this.NeedPassword != null)
                                {
                                    password = this.NeedPassword(this);
                                    this.cachedPassword = password;
                                }
                            }

                            return password == null ? IntPtr.Zero : password.Get();
                        }

                    case StringId.ProxyPassword:
                        {
                            SecretString password = this.ProxyPassword;
                            if (password == null)
                            {
                                password = this.cachedProxyPassword;
                            }

                            if (password == null && this.NeedProxyPassword != null)
                            {
                                password = this.NeedProxyPassword(this);
                                this.cachedProxyPassword = password;
                            }

                            return password == null ? IntPtr.Zero : password.Get();
                        }

                    case StringId.KeyPassword:
                        {
                            SecretString password = this.PublicKeyPassword;
                            if (password == null)
                            {
                                password = this.cachedKeyPassword;
                            }

                            if (password == null && this.NeedKeyPassword != null)
                            {
                                password = this.NeedKeyPassword(this);
                                this.cachedKeyPassword = password;
                            }

                            if (password == null)
                            {
                                return IntPtr.Zero;
                            }

                            return password.Get();
                        }

                    case StringId.PublicKeyFilePath:
                        return NativeUtf8FromString(this.PublicKeyFilePath);

                    case StringId.TerminalType:
                        return NativeUtf8FromString(this.TerminalType);

                    case StringId.LineCodePage:
                        return NativeUtf8FromString(this.LineCodePage);

                    case StringId.LogFileName:
                        return NativeUtf8FromString(this.LogFileName);

                    case StringId.ProxyHost:
                        return NativeUtf8FromString(this.ProxyHost);

                    case StringId.ProxyUserName:
                        return NativeUtf8FromString(this.ProxyUser);

                    case StringId.ProxyExcludeList:
                        return NativeUtf8FromString(this.ProxyExclusion);

                    case StringId.X11AuthFile:
                        return NativeUtf8FromString(this.X11AuthFile);

                    case StringId.X11Display:
                        return NativeUtf8FromString(this.X11Display);

                    case StringId.SerialLine:
                        return NativeUtf8FromString(this.SerialLine);

                    case StringId.Font:
                        return NativeUtf8FromString(this.TerminalFontName);

                    case StringId.Tunnels:
                        return NativeUtf8FromString(this.MakeTunnelString());

                    case StringId.RemoteCommand:
                        return NativeUtf8FromString(this.RemoteCommand);

                    case StringId.DoubleClickDelimiters:
                        return NativeUtf8FromString(this.DoubleClickDelimiters);

                    case StringId.KeyData:
                        return NativeUtf8FromString(this.PublicKeyData);

                    case StringId.RealHostName:
                        return NativeUtf8FromString(this.Host);

                    case StringId.ExpectedPasswordPrompt:
                        return NativeUtf8FromString(this.ExpectedPasswordPrompt);

                    case StringId.ExpectedUserPrompt:
                        return NativeUtf8FromString(this.ExpectedUserPrompt);

                    case StringId.SmartCardCertificateId:
                        return NativeUtf8FromString(this.SmartCardCertificateId);

                    case StringId.TerminalCopy:
                        string temp = this.terminalCopy;
                        this.terminalCopy = null;
                        return NativeUtf8FromString(temp);

                    case StringId.PingText:
                        if (string.IsNullOrEmpty(this.PingText))
                        {
                            return IntPtr.Zero;
                        }

                        return NativeUtf8FromString(this.PingText);
                }
            }
            catch (Exception)
            {
                // Just avoid the exception to crash the native code.
            }

            return IntPtr.Zero;
        }

        private long OnNeedValue(ushort valueId)
        {
            switch ((ValueId)valueId)
            {
                case ValueId.HasFocus:
                    return this.focusState ? 1 : 0;

                case ValueId.PortNumber:
                    return this.jumpContext == IntPtr.Zero ? this.Port : this.jumpPort;

                case ValueId.Protocol:
                    return (long)this.Protocol;

                case ValueId.AddressFamily:
                    if (this.jumpContext == IntPtr.Zero)
                    {
                        return (long)this.AddressFamily;
                    }
                    else
                    {
                        return (long)AddressFamily.Ipv4;
                    }

                case ValueId.AutoWrapMode:
                    return this.AutoWrapMode == false ? 0 : 1;

                case ValueId.LogType:
                    return (long)this.LogContentType;

                case ValueId.LocalEcho:
                    return (long)this.LocalEcho;

                case ValueId.MouseIsXterm:
                    return (long)this.MouseIsXterm;

                case ValueId.NoDBackspace:
                    return this.NoDBackspace == false ? 0 : 1;

                case ValueId.CRImpliesLF:
                    return this.CRImpliesLF == false ? 0 : 1;

                case ValueId.LFImpliesCR:
                    return this.LFImpliesCR == false ? 0 : 1;

                case ValueId.ScrollbackLines:
                    return this.ScrollbackLines;

                case ValueId.ApplicationCursorKeys:
                    return this.ApplicationCursorKeys == false ? 0 : 1;

                case ValueId.NoApplicationKeys:
                    return this.NoApplicationKeys == false ? 0 : 1;

                case ValueId.ApplicationKeypad:
                    return this.ApplicationKeypad == false ? 0 : 1;

                case ValueId.BackspaceIsDelete:
                    return this.BackspaceIsDelete == false ? 0 : 1;

                case ValueId.RXVTHomeEnd:
                    return this.RXVTHomeEnd == false ? 0 : 1;

                case ValueId.LinuxFunctionKeys:
                    return (long)this.LinuxFunctionKeys;

                case ValueId.TryAgent:
                    return this.TryAgent == false ? 0 : 1;

                case ValueId.AgentFwd:
                    return this.AgentFwd == false ? 0 : 1;

                case ValueId.TcpKeepAlive:
                    return this.TcpKeepAlive == false ? 0 : 1;

                case ValueId.PingInterval:
                    return this.PingInterval;

                case ValueId.SshNoShell:
                    return this.SshNoShell == false ? 0 : 1;

                case ValueId.CloseOnExit:
                    return (long)TriState.ForceOff;

                case ValueId.ProxyDns:
                    if (this.ProxyType == (int)Devolutions.Protocols.ProxyType.PROXY_TYPE_SOCKS4)
                    {
                        return (long)TriState.ForceOff;
                    }

                    if (this.ProxyResolveHint == ProxyResolveHint.PROXY_RESOLVE_FORCE_LOCAL)
                    {
                        return (long)TriState.ForceOff;
                    }

                    return (long)TriState.ForceOn;

                case ValueId.ProxyPort:
                    return this.ProxyPort;

                case ValueId.ProxyMethod:
                    return this.ProxyType;

                case ValueId.ProxyLocalHost:
                    return this.ProxyIncludeLocal == false ? 0 : 1;

                case ValueId.X11AuthType:
                    return (long)this.X11AuthType;

                case ValueId.X11Forward:
                    return this.X11Forward == false ? 0 : 1;

                case ValueId.SerialDataBits:
                    return this.SerialDataBits;

                case ValueId.SerialFlowControl:
                    return (long)this.SerialFlowControl;

                case ValueId.SerialParity:
                    return (long)this.SerialParity;

                case ValueId.SerialSpeed:
                    return this.SerialSpeed;

                case ValueId.SerialStopHalfbits:
                    return this.SerialStopHalfbits;

                case ValueId.TerminalFontHeight:
                    return this.TerminalFontHeight;

                case ValueId.LogOverwrite:
                    if (this.LogOverwrite != null)
                    {
                        return this.LogOverwrite(this) == LogOverwriteResponse.Overwrite ? 2 : 1;
                    }

                    return (long)LogOverwriteResponse.Append;

                case ValueId.Verbose:
                    return this.Verbose == false ? 0 : 1;

                case ValueId.CursorType:
                    return (long)this.CursorType;

                case ValueId.CursorBlink:
                    return this.CursorBlink == false ? 0 : 1;

                case ValueId.GssapiAuth:
                    return this.GssapiAuthentication == false ? 0 : 1;

                case ValueId.GssForward:
                    return this.GssapiDelegation == false ? 0 : 1;

                case ValueId.RealPortNumber:
                    return this.Port;

                case ValueId.FontIsBold:
                    return this.TerminalFontIsBold == false ? 0 : 1;

                case ValueId.FontQuality:
                    return (long)this.TerminalFontQuality;

                case ValueId.TrySmartCardCertificateAuthentication:
                    return this.TrySmartCardCertificateAuthentication == false ? 0 : 1;

                case ValueId.ResetScrollOnDisplay:
                    return this.ResetScrollOnDisplay == false ? 0 : 1;

                case ValueId.PlayerMaxIdle:
                    return this.PlayerMaxIdle;

                case ValueId.IsRecording:
                    if (this.Protocol == NativeSshProtocol.Player)
                    {
                        return 0;
                    }
                    else
                    {
                        return this.IsRecording == false ? 0 : 1;
                    }

                case ValueId.IsInteractiveAuthOnTerminal:
                    return this.InteractiveAuthOnTerminal == false ? 0 : 1;

                case ValueId.IsControlWheelDisabled:
                    return this.DisableControlWheel == false ? 0 : 1;
            }

            if (valueId >= (ushort)ValueId.Colors && valueId < (ushort)ValueId.Colors + (3 * 22))
            {
                int i = (valueId - (ushort)ValueId.Colors) / 3;
                int c = (valueId - (ushort)ValueId.Colors) % 3;
                if (c == 0)
                {
                    return this.TerminalColors[i].R;
                }
                else if (c == 1)
                {
                    return this.TerminalColors[i].G;
                }

                return this.TerminalColors[i].B;
            }

            return 0;
        }

        private void OnReleaseString(IntPtr utf8String)
        {
            Marshal.FreeHGlobal(utf8String);
        }

        private void OnSequenceComplete(CompletionCode result)
        {
            this.SequenceResult = result;
            if (this.SequenceResult != CompletionCode.SUCCESS)
            {
                this.sequenceQueue.Clear();
            }
            else
            {
                this.sequenceQueue.Dequeue();
            }

            if (this.sequenceQueue.Count <= 0)
            {
                if (this.SequenceComplete != null)
                {
                    this.SequenceComplete(this, null);
                }
            }
            else
            {
                CommandSequence sequence = (CommandSequence)this.sequenceQueue.Peek();
                IntPtr utf8Sequence = sequence.GetSequence();
                DevolutionsTerminalNativeAPI.SendSequence(this.context, utf8Sequence);
                Marshal.FreeHGlobal(utf8Sequence);
            }
        }

        private void OnSignal(int signal)
        {
            switch ((SignalId)signal)
            {
                case SignalId.Connected:
                    this.State = ConnectionState.Connected;
                    if (this.PingInterval > 0 && string.IsNullOrEmpty(this.PingText) == false)
                    {
                        DevolutionsTerminalNativeAPI.StartPing(this.context);
                    }

                    if (this.Connected != null)
                    {
                        try
                        {
                            this.BeginInvoke((MethodInvoker)delegate { this.Connected(this, null); });
                        }
                        catch (Exception)
                        {
                            // Just catch the exception to avoid a crash.
                        }
                    }

                    break;

                case SignalId.Disconnected:
                    try
                    {
                        this.BeginInvoke(
                            (Action)(() =>
                                {
                                    this.contextIsActive = false;
                                    if (this.jumpContext != IntPtr.Zero && this.jumpContextIsActive)
                                    {
                                        DevolutionsTerminalNativeAPI.EndThread(this.jumpContext);
                                    }
                                    else
                                    {
                                        this.BeginInvoke((Action)(() => this.ReleaseContext(this)));
                                    }
                                }));
                    }
                    catch (Exception)
                    {
                        // Just catch the exception to avoid a crash.
                    }

                    break;

                case SignalId.Beep:
                    if (this.Beep != null)
                    {
                        try
                        {
                            this.BeginInvoke((MethodInvoker)delegate { this.Beep(this, null); });
                        }
                        catch (Exception)
                        {
                            // Just catch the exception to avoid a crash.
                        }
                    }

                    break;

                case SignalId.Denied:
                    this.cachedPassword = null;
                    break;

                case SignalId.NewRecordData:
                    if (this.NewRecordData != null)
                    {
                        this.NewRecordData(this, null);
                    }

                    break;

                case SignalId.Closed:
                    this.context = IntPtr.Zero;
                    this.State = ConnectionState.Closed;
                    if (this.jumpContext == IntPtr.Zero && this.NativeClosed != null)
                    {
                        this.NativeClosed(this, null);
                    }

                    break;

                case SignalId.SequenceSuccess:
                    this.OnSequenceComplete(CompletionCode.SUCCESS);
                    break;

                case SignalId.SequenceFail:
                    this.OnSequenceComplete(CompletionCode.FAIL);
                    break;

                case SignalId.SequenceTimeout:
                    this.OnSequenceComplete(CompletionCode.FAIL_TIMEOUT);
                    break;
            }
        }

        private ushort OnSshIsServerKnown(IntPtr nativeKey)
        {
            if (this.SshIsServerKnown != null)
            {
                try
                {
                    string key = Marshal.PtrToStringAnsi(nativeKey);
                    return (ushort)this.SshIsServerKnown(this, this.Host, this.Port, key);
                }
                catch (Exception)
                {
                    // Just catch the exception to avoid a crash.
                }
            }

            return 0;
        }

        private ushort OnSshServerAuthentication(ushort code, IntPtr nativeFingerprint)
        {
            if (this.SshServerAuthentication != null)
            {
                try
                {
                    string fingerprint = Marshal.PtrToStringAnsi(nativeFingerprint);
                    return (ushort)this.SshServerAuthentication(this, (SshServerAuthenticationCode)code, fingerprint);
                }
                catch (Exception)
                {
                    // Just catch the exception to avoid a crash.
                }
            }

            return 0;
        }

        private void ProcessTimer()
        {
            if (this.context == IntPtr.Zero)
            {
                return;
            }

            bool success = this.commandQueue.TryDequeue(out Command command);
            if (success)
            {
                IntPtr nativeString = NativeUtf8FromString(command.String);
                DevolutionsTerminalNativeAPI.Send(this.context, nativeString);

                // The native code will release the native string.
            }

            success = this.commandQueue.TryPeek(out command);
            if (success)
            {
                if (command.MsDelay == 0)
                {
                    this.BeginInvoke((Action)this.ProcessTimer);
                }
                else
                {
                    this.commandTimer.Interval = command.MsDelay;
                    this.commandTimer.Start();
                }
            }
            else
            {
                this.commandTimer.Stop();
            }
        }

        private void ReleaseContext(object info)
        {
            if (this.context != IntPtr.Zero)
            {
                IntPtr rawOutput = DevolutionsTerminalNativeAPI.CopyOutput(this.context);
                this.terminalCopy = Marshal.PtrToStringUni(rawOutput);
            }

            if (this.jumpContext != IntPtr.Zero)
            {
                IntPtr temp = this.jumpContext;
                ThreadPool.QueueUserWorkItem(o => DevolutionsTerminalNativeAPI.Release(temp));
                this.jumpContext = IntPtr.Zero;
            }

            this.cachedUser = null;
            this.cachedPassword = null;
            this.cachedProxyPassword = null;
            this.cachedKeyPassword = null;
            this.contextIsActive = false;
            this.jumpContextIsActive = false;
            this.cachedJumpHostPassword = null;
            this.cachedJumpHostPublicKeyPassword = null;

            this.State = ConnectionState.Disconnected;
            if (this.Disconnected != null)
            {
                this.Disconnected(this, null);
            }
        }

        private void SetSmartCardCertificateId(SetSmartCardCertificateMethod certificateType)
        {
            IntPtr certId;
            switch (certificateType)
            {
                case SetSmartCardCertificateMethod.CAPI:
                    certId = DevolutionsTerminalNativeAPI.SmartCardGetCAPICertId(this.Handle);
                    break;

                case SetSmartCardCertificateMethod.PKCS:
                    certId = DevolutionsTerminalNativeAPI.SmartCardGetPKSCCertId(this.Handle);
                    break;

                default:
                    certId = DevolutionsTerminalNativeAPI.SmartCardGetPKSCCertId(this.Handle);
                    break;
            }

            this.SmartCardCertificateId = Marshal.PtrToStringAnsi(certId);
            DevolutionsTerminalNativeAPI.SmartCardFree(certId);
        }

        private void SetSmartCardCertificatePublicKey()
        {
            if (this.SmartCardCertificateId != null && !this.SmartCardCertificateId.Equals(string.Empty))
            {
                IntPtr publicKey = DevolutionsTerminalNativeAPI.SmartCardGetPublicKey(this.SmartCardCertificateId);
                this.SmartCardCertificatePublicKey = Marshal.PtrToStringAnsi(publicKey);
                DevolutionsTerminalNativeAPI.SmartCardFree(publicKey);
            }
        }

        private void SetSmartCardCertificateThumbprint()
        {
            if (this.SmartCardCertificateId != null && !this.SmartCardCertificateId.Equals(string.Empty))
            {
                int indexOfFirstEqualSign = this.SmartCardCertificateId.IndexOf('=');
                if (indexOfFirstEqualSign > 0)
                {
                    this.SmartCardCertificateThumbprint = this.SmartCardCertificateId.Substring(0, indexOfFirstEqualSign);
                }
                else
                {
                    this.SmartCardCertificateThumbprint = string.Empty;
                }
            }
        }

        private class Command
        {
            internal int MsDelay { get; set; }

            internal string String { get; set; }
        }
    }
}