#pragma once

#include <windows.h>
#include "ssh.h"

int smartCardBridgeCertIsCertpath(char* cert);

LPBYTE  smartCardBridgeCertSign(struct ssh2_userkey * userkey, LPCBYTE pDataToSign, int iDataToSignLen, int * iWrappedSigLen, HWND hWnd);

struct ssh2_userkey* smartCardBridgeCertLoadKey(LPCSTR certId);

void smartCardBridgeCertConvertLegacy(LPSTR certId);

char* smartCardBridgeCertPrompt(char* type, HWND hwnd);

LPSTR smartCardBridgeCertKeyString(LPCSTR certId);

void smartCardBridgeCertDisplayCert(LPCSTR certId, HWND hwnd);

// Bridge to sfree(), used to free memory allocated in this dll
void smartCardBridgeFree(void* p);
