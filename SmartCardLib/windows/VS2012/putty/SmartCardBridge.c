#pragma once

#include "SmartCardBridge.h"
#include "cert_common.h"

int smartCardBridgeCertIsCertpath(char* cert)
{
	return cert_is_certpath(cert);
}

LPBYTE  smartCardBridgeCertSign(struct ssh2_userkey * userkey, LPCBYTE pDataToSign, int iDataToSignLen, int * iWrappedSigLen, HWND hWnd)
{
	return cert_sign(userkey, pDataToSign, iDataToSignLen, iWrappedSigLen, hWnd);
}

struct ssh2_userkey* smartCardBridgeCertLoadKey(LPCSTR certId)
{
	return cert_load_key(certId);
}

void smartCardBridgeCertConvertLegacy(LPSTR certId)
{
	cert_convert_legacy(certId);
}

char* smartCardBridgeCertPrompt(char* type, HWND hwnd)
{
	return cert_prompt(type, hwnd);
}

LPSTR smartCardBridgeCertKeyString(LPCSTR certId)
{
	return cert_key_string(certId);
}

void smartCardBridgeCertDisplayCert(LPCSTR certId, HWND hwnd)
{
	cert_display_cert(certId, hwnd);
}

void smartCardBridgeFree(void* p)
{
	if (p != NULL)
	{
		sfree(p);
		p = NULL;
	}
}